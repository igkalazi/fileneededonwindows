CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{C1476214-EB10-479B-B1C4-61BA06FD6B65}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{97E7EB6B-1412-4A79-8D3E-F458471A684E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{878E8C3D-4CA7-436E-AF8F-289E335335A8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{04BBFC7D-9931-43D3-A2EC-7F0377A765D5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{DF9F5E6C-8B39-4631-8ADD-2E845918623F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{0CE3D452-39A6-4BA0-A7A2-FC4CC3FA835B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{BE09EC2A-A92E-4837-A4BF-136EF1AF8034}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{1BFBDDE0-E37F-4928-8F3A-E85BF1A9DB59}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "{B27A2309-B52C-438D-86C8-3514DF732FA5}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{BB66788C-4C4F-4EB0-B146-9178857DE287}"/>')
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0" , "ValidationSig", "sJahCcOGwHda8hOP7cRu/Cg5gP3UeMqOZwG9FCh3pcEvcZ1oetMb5y7Do68ueLe51zGr5MG9eW2+LsNgOGRC0XX/q5PByuwxwbKpGRvKvQeisFJGOiq7u8C/skNfdxDAbThwj8L9XXn1t2E5gN0XtH7kmRquEtOvPxJ01epWROOTKesLUVgrayxB62wZuRHRz6Qy/9z6k+Zl8XLLJrqsYxggU3MSzZW5cQJ+Xtr5ZHf42GFjx0thiZZeNi+9pgGfZZh2fhtFqePeo8j81TsGOtESWoFLHv4RL78WafH3sobgGrAGt90gycEXDPxbEGT4KXw+Zq3ehGo6AjN/0RrC+qvT0+H0KcGG7LfFwnI9JRMhSPk3wWAt3qtWxlxkphH2TPgTfwBHC8xH5F2FKO0hcUnCnZDdXEYy+ONVVGcvf0CHVJ081l3s6K+rWLZKQZFVvQjTUkBgNAgEBLx+PbbAZ7tRO6rWODRnkA4O3r3Uv+wsYgVDkhPxjSez4BDFAOeH")
INSERT INTO Payloads VALUES	("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "CoreTech", "AdobeColorNA CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-09 00:43:29.180000</Property>
    <Property name="TargetName">AdobeColorNA_Recommended4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{BB66788C-4C4F-4EB0-B146-9178857DE287}</Property>
    <Property name="ProductName">Adobe Color NA Recommended Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>en_US</Language>
    <Language>fr_CA</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorNA CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{C1476214-EB10-479B-B1C4-61BA06FD6B65}</AdobeCode>
    <AdobeCode>{97E7EB6B-1412-4A79-8D3E-F458471A684E}</AdobeCode>
    <AdobeCode>{878E8C3D-4CA7-436E-AF8F-289E335335A8}</AdobeCode>
    <AdobeCode>{04BBFC7D-9931-43D3-A2EC-7F0377A765D5}</AdobeCode>
    <AdobeCode>{DF9F5E6C-8B39-4631-8ADD-2E845918623F}</AdobeCode>
    <AdobeCode>{0CE3D452-39A6-4BA0-A7A2-FC4CC3FA835B}</AdobeCode>
    <AdobeCode>{BE09EC2A-A92E-4837-A4BF-136EF1AF8034}</AdobeCode>
    <AdobeCode>{1BFBDDE0-E37F-4928-8F3A-E85BF1A9DB59}</AdobeCode>
    <AdobeCode>{B27A2309-B52C-438D-86C8-3514DF732FA5}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorNARecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color NA Recommended Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2390004"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2390004</TotalSize>
      <MaxPathComponent>/Color/Settings/Recommended\North America General Purpose.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2390004"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="en_US">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="en_US">Adobe Color NA Recommended Settings CS6</Value>
      <Value lang="fr_CA">Adobe Color NA Recommended Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0", "ChannelID", "AdobeColorNARecommendedSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{BB66788C-4C4F-4EB0-B146-9178857DE287}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorNARecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color NA Recommended Settings CS6</DisplayName>
  </Channel>')
