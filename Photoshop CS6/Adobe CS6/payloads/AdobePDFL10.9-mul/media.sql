CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{756F1781-9FEE-4F2B-AE29-F89302FA5723}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{DA68FB0E-51A2-4582-A1F5-C1DB84F5C22A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{D542DEEF-BD99-4D23-8DCA-4DBBBC1453EB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{50DF8260-E929-4227-8692-420398D346F0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{C4F53041-9E7F-4A93-87A0-49E7A3F658D8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{DBE3BD2E-0F40-4CE3-945A-F8E2D9098BBE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{C521404E-8D5B-4614-A789-9B73CECD9DE6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{405ECF89-5959-40C3-B17A-943114F3081E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{14A05962-430A-4803-8386-45F0E54C777C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "{E1076347-5436-4A04-A56B-9DB198C68D8B}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{557F9FD3-EED8-43D7-AF29-0F19CA832728}"/>')
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0" , "ValidationSig", "lPsusMFTiUZVEMYaMOcPZrZferQBqh2eXLAOaEFokn0dKHcO6DqY688Zmv4MVfeVjKMYTFRuon0Lb/WC+Nq8u1Npu31giTu38UXMN3zIFK2N66Vlw5ph/eA8dmQgFWfEX8IyXaQKqNka9jXZlGNUxqzrixq5g6nICUfZFONoZslRnTgJ/vZiRpJH5fefaXr1bJI/E3LUwIrjvBTtrq9wQLPQtYCeC0N3NC1dWaI09BjN0r9Roj+LiDMqq1Nrv82HcvtiadqJFx/VPDlbS2vGOEZ0syik15PjuxBIVTPlj0ff1jZcnlYsWPRX1HQfa/snOruM9SNnLUgDgnA8n2db8FOhq1UFcwsJ3UipJXTzBa/ia/TsYhLNCOkTS8mnOCQMRx6Ztts+xfzyM3ZY+OOaEVLOOqm2c4g1ET1ANQTAurAz2euoHzO6RuTIKo7DZqp8jNBFsAT5ZOz4PyzYydF8qCULGOm12xWmoiX+HAkWkeazmiLxPPFZif6a0xN/KnAG")
INSERT INTO Payloads VALUES	("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "CoreTech", "AdobePDFL CS6", "10.9", "normal")
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-11-28 17:08:40.801000</Property>
    <Property name="TargetName">AdobePDFL10.9-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{557F9FD3-EED8-43D7-AF29-0F19CA832728}</Property>
    <Property name="ProductName">Adobe PDF Library Files CS6</Property>
    <Property name="ProductVersion">10.9</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\PDFL\10.9</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobePDFL CS6</ProductName>
	<ProductVersion>10.9</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{756F1781-9FEE-4F2B-AE29-F89302FA5723}</AdobeCode>
    <AdobeCode>{DA68FB0E-51A2-4582-A1F5-C1DB84F5C22A}</AdobeCode>
    <AdobeCode>{D542DEEF-BD99-4D23-8DCA-4DBBBC1453EB}</AdobeCode>
    <AdobeCode>{50DF8260-E929-4227-8692-420398D346F0}</AdobeCode>
    <AdobeCode>{C4F53041-9E7F-4A93-87A0-49E7A3F658D8}</AdobeCode>
    <AdobeCode>{DBE3BD2E-0F40-4CE3-945A-F8E2D9098BBE}</AdobeCode>
    <AdobeCode>{C521404E-8D5B-4614-A789-9B73CECD9DE6}</AdobeCode>
    <AdobeCode>{405ECF89-5959-40C3-B17A-943114F3081E}</AdobeCode>
    <AdobeCode>{14A05962-430A-4803-8386-45F0E54C777C}</AdobeCode>
    <AdobeCode>{E1076347-5436-4A04-A56B-9DB198C68D8B}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobePDFLibraryFilesCS6-10.9">
    <DisplayName>Adobe PDF Library Files CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="71046768"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>71046768</TotalSize>
      <MaxPathComponent>/PDFL/10.9/Fonts\AdobeArabic-BoldItalic.otf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="71046768"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">10.9</Value>
      <Value lang="be_BY">10.9</Value>
      <Value lang="bg_BG">10.9</Value>
      <Value lang="ca_ES">10.9</Value>
      <Value lang="cs_CZ">10.9</Value>
      <Value lang="da_DK">10.9</Value>
      <Value lang="de_DE">10.9</Value>
      <Value lang="el_GR">10.9</Value>
      <Value lang="en_GB">10.9</Value>
      <Value lang="en_MX">10.9</Value>
      <Value lang="en_US">10.9</Value>
      <Value lang="en_XC">10.9</Value>
      <Value lang="en_XM">10.9</Value>
      <Value lang="es_ES">10.9</Value>
      <Value lang="es_MX">10.9</Value>
      <Value lang="es_QM">10.9</Value>
      <Value lang="et_EE">10.9</Value>
      <Value lang="fi_FI">10.9</Value>
      <Value lang="fr_CA">10.9</Value>
      <Value lang="fr_FR">10.9</Value>
      <Value lang="fr_MX">10.9</Value>
      <Value lang="fr_XM">10.9</Value>
      <Value lang="he_IL">10.9</Value>
      <Value lang="hi_IN">10.9</Value>
      <Value lang="hr_HR">10.9</Value>
      <Value lang="hu_HU">10.9</Value>
      <Value lang="is_IS">10.9</Value>
      <Value lang="it_IT">10.9</Value>
      <Value lang="ja_JP">10.9</Value>
      <Value lang="ko_KR">10.9</Value>
      <Value lang="lt_LT">10.9</Value>
      <Value lang="lv_LV">10.9</Value>
      <Value lang="mk_MK">10.9</Value>
      <Value lang="nb_NO">10.9</Value>
      <Value lang="nl_NL">10.9</Value>
      <Value lang="nn_NO">10.9</Value>
      <Value lang="no_NO">10.9</Value>
      <Value lang="pl_PL">10.9</Value>
      <Value lang="pt_BR">10.9</Value>
      <Value lang="ro_RO">10.9</Value>
      <Value lang="ru_RU">10.9</Value>
      <Value lang="sh_YU">10.9</Value>
      <Value lang="sk_SK">10.9</Value>
      <Value lang="sl_SI">10.9</Value>
      <Value lang="sq_AL">10.9</Value>
      <Value lang="sv_SE">10.9</Value>
      <Value lang="th_TH">10.9</Value>
      <Value lang="tr_TR">10.9</Value>
      <Value lang="uk_UA">10.9</Value>
      <Value lang="vi_VN">10.9</Value>
      <Value lang="zh_CN">10.9</Value>
      <Value lang="zh_TW">10.9</Value>
      <Value lang="en_AE">10.9</Value>
      <Value lang="en_IL">10.9</Value>
      <Value lang="fr_MA">10.9</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe PDF Library Files CS6</Value>
      <Value lang="be_BY">Adobe PDF Library Files CS6</Value>
      <Value lang="bg_BG">Adobe PDF Library Files CS6</Value>
      <Value lang="ca_ES">Adobe PDF Library Files CS6</Value>
      <Value lang="cs_CZ">Adobe PDF Library Files CS6</Value>
      <Value lang="da_DK">Adobe PDF Library Files CS6</Value>
      <Value lang="de_DE">Adobe PDF Library Files CS6</Value>
      <Value lang="el_GR">Adobe PDF Library Files CS6</Value>
      <Value lang="en_GB">Adobe PDF Library Files CS6</Value>
      <Value lang="en_MX">Adobe PDF Library Files CS6</Value>
      <Value lang="en_US">Adobe PDF Library Files CS6</Value>
      <Value lang="en_XC">Adobe PDF Library Files CS6</Value>
      <Value lang="en_XM">Adobe PDF Library Files CS6</Value>
      <Value lang="es_ES">Adobe PDF Library Files CS6</Value>
      <Value lang="es_MX">Adobe PDF Library Files CS6</Value>
      <Value lang="es_QM">Adobe PDF Library Files CS6</Value>
      <Value lang="et_EE">Adobe PDF Library Files CS6</Value>
      <Value lang="fi_FI">Adobe PDF Library Files CS6</Value>
      <Value lang="fr_CA">Adobe PDF Library Files CS6</Value>
      <Value lang="fr_FR">Adobe PDF Library Files CS6</Value>
      <Value lang="fr_MX">Adobe PDF Library Files CS6</Value>
      <Value lang="fr_XM">Adobe PDF Library Files CS6</Value>
      <Value lang="he_IL">Adobe PDF Library Files CS6</Value>
      <Value lang="hi_IN">Adobe PDF Library Files CS6</Value>
      <Value lang="hr_HR">Adobe PDF Library Files CS6</Value>
      <Value lang="hu_HU">Adobe PDF Library Files CS6</Value>
      <Value lang="is_IS">Adobe PDF Library Files CS6</Value>
      <Value lang="it_IT">Adobe PDF Library Files CS6</Value>
      <Value lang="ja_JP">Adobe PDF Library Files CS6</Value>
      <Value lang="ko_KR">Adobe PDF Library Files CS6</Value>
      <Value lang="lt_LT">Adobe PDF Library Files CS6</Value>
      <Value lang="lv_LV">Adobe PDF Library Files CS6</Value>
      <Value lang="mk_MK">Adobe PDF Library Files CS6</Value>
      <Value lang="nb_NO">Adobe PDF Library Files CS6</Value>
      <Value lang="nl_NL">Adobe PDF Library Files CS6</Value>
      <Value lang="nn_NO">Adobe PDF Library Files CS6</Value>
      <Value lang="no_NO">Adobe PDF Library Files CS6</Value>
      <Value lang="pl_PL">Adobe PDF Library Files CS6</Value>
      <Value lang="pt_BR">Adobe PDF Library Files CS6</Value>
      <Value lang="ro_RO">Adobe PDF Library Files CS6</Value>
      <Value lang="ru_RU">Adobe PDF Library Files CS6</Value>
      <Value lang="sh_YU">Adobe PDF Library Files CS6</Value>
      <Value lang="sk_SK">Adobe PDF Library Files CS6</Value>
      <Value lang="sl_SI">Adobe PDF Library Files CS6</Value>
      <Value lang="sq_AL">Adobe PDF Library Files CS6</Value>
      <Value lang="sv_SE">Adobe PDF Library Files CS6</Value>
      <Value lang="th_TH">Adobe PDF Library Files CS6</Value>
      <Value lang="tr_TR">Adobe PDF Library Files CS6</Value>
      <Value lang="uk_UA">Adobe PDF Library Files CS6</Value>
      <Value lang="vi_VN">Adobe PDF Library Files CS6</Value>
      <Value lang="zh_CN">Adobe PDF Library Files CS6</Value>
      <Value lang="zh_TW">Adobe PDF Library Files CS6</Value>
      <Value lang="en_AE">Adobe PDF Library Files CS6</Value>
      <Value lang="en_IL">Adobe PDF Library Files CS6</Value>
      <Value lang="fr_MA">Adobe PDF Library Files CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0", "ChannelID", "AdobePDFLibraryFilesCS6-10.9")
INSERT INTO PayloadData VALUES("{557F9FD3-EED8-43D7-AF29-0F19CA832728}", "0", "ChannelInfo", '<Channel enable="1" id="AdobePDFLibraryFilesCS6-10.9">
    <DisplayName>Adobe PDF Library Files CS6</DisplayName>
  </Channel>')
