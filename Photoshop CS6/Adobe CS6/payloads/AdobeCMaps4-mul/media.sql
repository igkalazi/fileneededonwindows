CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{75E32A51-6DB4-4533-91A8-1A1382555BFB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{617D9FA9-B02D-4144-A3EC-7E8730873005}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{00738D5C-0069-4488-97CF-C1D168D84DB8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{4E974201-F091-49F0-8A75-93B353D3A1F4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{C40C2646-BF6E-4525-B642-5C0FEB6B786E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{AF945A36-E196-4535-97F5-09E05BCBC0ED}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{EFC4FBF4-A002-41CC-9049-5CEF12B09F8D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{007147E3-185B-4AC7-A63A-F912BC5E5FD7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{9CE7749F-D0FB-4CCE-806D-D047782FB6B9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{7CECD0BD-7991-45EC-83FC-B5769B5579A4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{E12C5603-B20B-47B3-86EB-2B8DF87D3B8F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{7A0ABBF1-CAF2-4B28-90EC-4279B4F9505F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{2F0AF2F2-DCA9-496A-B15C-A091D2402C51}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{B9762CA7-F8DC-467A-B559-E7BB2F52FF7A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "{6835FFB7-0E68-4995-AA10-5F0300808029}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}"/>')
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0" , "ValidationSig", "e4XMc2J+MIVTQdM+nSDS9+lrt8rO9mXqvM7WJwnIQERByDP9jsDkmWDic62OWMQdEZl0/9RHWiFwKEFrDW4ZZayKZwE860QyVgKprFu6W/0rlncAFEI/BRZq6/IV4hm5ERuLypItm99vjcWqfb2ZhXxhcddFP5VhSNgt4pt3LvuRKWG966Tde8cpAjuBXLzyqy9ZXcckZxHN36VHcPwvung8uzYhZQMhm1/P+ustNtWX2o3DFk3mbtw9946y3YtltWjHoTd9RY2saADHZX+ctuKFoRj38QOywoyH/44AvWQICu/oPEHca41QiOrv/+pqPaxUq7RSXo8KGSvXvBluy7MKi6we1bavCTuk8i9Z0FMSQGGJil5014seF96A9CIPpNo5Bl+Gb+g9SJ4wmY9R54LeJMRzUTyT+YsK8jpvP3ryJtwdc8t5is7ODfkl5HO8Sce8qq/QVmZps1SICHDTxTyILPBgZUAp4gRMTV5BjZ+DsZhbsGuVjuxo4k8iKwyR")
INSERT INTO Payloads VALUES	("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "CoreTech", "AdobeCMaps CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-19 12:59:45.483000</Property>
    <Property name="TargetName">AdobeCMaps4-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}</Property>
    <Property name="ProductName">Adobe CMaps CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\TypeSupport\CS6\CMaps</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeCMaps CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{75E32A51-6DB4-4533-91A8-1A1382555BFB}</AdobeCode>
    <AdobeCode>{617D9FA9-B02D-4144-A3EC-7E8730873005}</AdobeCode>
    <AdobeCode>{00738D5C-0069-4488-97CF-C1D168D84DB8}</AdobeCode>
    <AdobeCode>{4E974201-F091-49F0-8A75-93B353D3A1F4}</AdobeCode>
    <AdobeCode>{C40C2646-BF6E-4525-B642-5C0FEB6B786E}</AdobeCode>
    <AdobeCode>{AF945A36-E196-4535-97F5-09E05BCBC0ED}</AdobeCode>
    <AdobeCode>{EFC4FBF4-A002-41CC-9049-5CEF12B09F8D}</AdobeCode>
    <AdobeCode>{007147E3-185B-4AC7-A63A-F912BC5E5FD7}</AdobeCode>
    <AdobeCode>{9CE7749F-D0FB-4CCE-806D-D047782FB6B9}</AdobeCode>
    <AdobeCode>{7CECD0BD-7991-45EC-83FC-B5769B5579A4}</AdobeCode>
    <AdobeCode>{E12C5603-B20B-47B3-86EB-2B8DF87D3B8F}</AdobeCode>
    <AdobeCode>{7A0ABBF1-CAF2-4B28-90EC-4279B4F9505F}</AdobeCode>
    <AdobeCode>{2F0AF2F2-DCA9-496A-B15C-A091D2402C51}</AdobeCode>
    <AdobeCode>{B9762CA7-F8DC-467A-B559-E7BB2F52FF7A}</AdobeCode>
    <AdobeCode>{6835FFB7-0E68-4995-AA10-5F0300808029}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeCMapsCS6-4.0">
    <DisplayName>Adobe CMaps CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="6904009"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>6904009</TotalSize>
      <MaxPathComponent>/TypeSupport/CS6/CMaps\UniJISX02132004-UTF32-H</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="6904009"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	<Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Ii][Nn][Gg] [Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Uu][Dd][Ii][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Ff][Tt][Ee][Rr][Ff][Xx]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ss][Hh][Ee][Ll][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Dd][Nn][Gg] [Cc][Oo][Nn][Vv][Ee][Rr][Tt][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee][Pp][Rr][Oo][Xx][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ee][Vv][Ii][Cc][Ee][Cc][Ee][Nn][Tt][Rr][Aa][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Rr][Ee][Aa][Mm][Ww][Ee][Aa][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Dd][Ss][Cc][Rr][Ii][Pp][Tt] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Rr][Aa][Ss][Hh][Rr][Ee][Pp][Oo][Rr][Tt][Ee][Rr]App\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh][Pp][Ll][Aa][Yy][Ee][Rr]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 7 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 8 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Cc][Oo][Pp][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Pp][Rr][Oo]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][[Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Tt][Oo][Cc][Kk] [Pp][Hh][Oo][Tt][Oo][Ss] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 4\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 5\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Oo][Rr][Gg][Aa][Nn][Ii][Zz][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Ee][Dd][Ii][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Ii][Mm][Aa][Gg][Ee][Rr][Ee][Aa][Dd][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee] [Dd][Vv][Dd]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Oo][Nn][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]10\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Cc][Oo][Mm]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Tt][Mm][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Ee][Ll][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Rr][Aa][Mm][Ee][Mm][Aa][Kk][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee][Cc][Aa][Pp][Tt][Ii][Vv][Aa][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Mm][Ii][Dd][Dd][Ll][Ee][Tt][Ii][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^3[Dd] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]\.[Ee][Xx][Ee]</Process>
    </Win32>
<OSX>
      <Process processType="Adobe" blocking="1">^[Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Ii][Nn][Gg] [Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Bb][Ee][Tt][Aa]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]4</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Aa][Ff][Tt][Ee][Rr] [Ee][Ff][Ff][Ee][Cc][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Ff][Tt][Ee][Rr] [Ee][Ff][Ff][Ee][Cc][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Hh][Ee][Ll][Pp] [Vv][Ii][Ee][Ww][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Mm][Aa][Gg][Ee][Rr][Ee][Aa][Dd][Yy]*</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Gg][Oo][Ll][Ii][Vv][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee] [Cc][Ss][23]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Cc][Rr][Aa][Ss][Hh][Cc][Rr][Aa][Ss][Hh] [Rr][Ee][Pp][Oo][Rr][Tt][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Ss][Hh][Ee][Ll][Ll]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ff][Ee][Xx][Tt][Nn][Ff][Oo][Rr][Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee][Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ee][Vv][Ii][Cc][Ee] [Cc][Ee][Nn][Tt][Rr][Aa][Ll]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Rr][Ee][Aa][Mm][Ww][Ee][Aa][Vv][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Nn][Cc][Oo][Rr][Ee][Dd][Vv][Dd]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Dd][Ss][Cc][Rr][Ii][Pp][Tt] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]*</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ff][Ll][Aa][Ss][Hh] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Pp][Ll][Aa][Yy][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Ss][Tt][Aa][Ll][Ll] [Ff][Ll][Aa][Ss][Hh] [Pp][Ll][Aa][Yy][Ee][Rr] 9 [Uu][Bb]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Nn][Cc][Oo][Pp][Yy] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Cc][Oo][Pp][Yy] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Uu][Dd][Ii][Tt][Ii][Oo][Nn]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]*</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ii][Ss][Tt][Ii][Ll][Ll][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Oo][Nn][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Bb][Rr][Ii][Dd][Gg][Ee] [Cc][Ss]4$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Mm][Ee][Dd][Ii][Aa] [Pp][Ll][Aa][Yy][Ee][Rr]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Dd][Rr][Ii][Vv][Ee] [Cc][Ss]4$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Mm][Ee][Dd][Ii][Aa] [Pp][Ll][Aa][Yy][Ee][Rr]$</Process>
    </OSX>
  </ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe CMaps CS6</Value>
      <Value lang="be_BY">Adobe CMaps CS6</Value>
      <Value lang="bg_BG">Adobe CMaps CS6</Value>
      <Value lang="ca_ES">Adobe CMaps CS6</Value>
      <Value lang="cs_CZ">Adobe CMaps CS6</Value>
      <Value lang="da_DK">Adobe CMaps CS6</Value>
      <Value lang="de_DE">Adobe CMaps CS6</Value>
      <Value lang="el_GR">Adobe CMaps CS6</Value>
      <Value lang="en_GB">Adobe CMaps CS6</Value>
      <Value lang="en_MX">Adobe CMaps CS6</Value>
      <Value lang="en_US">Adobe CMaps CS6</Value>
      <Value lang="en_XC">Adobe CMaps CS6</Value>
      <Value lang="en_XM">Adobe CMaps CS6</Value>
      <Value lang="es_ES">Adobe CMaps CS6</Value>
      <Value lang="es_MX">Adobe CMaps CS6</Value>
      <Value lang="es_QM">Adobe CMaps CS6</Value>
      <Value lang="et_EE">Adobe CMaps CS6</Value>
      <Value lang="fi_FI">Adobe CMaps CS6</Value>
      <Value lang="fr_CA">Adobe CMaps CS6</Value>
      <Value lang="fr_FR">Adobe CMaps CS6</Value>
      <Value lang="fr_MX">Adobe CMaps CS6</Value>
      <Value lang="fr_XM">Adobe CMaps CS6</Value>
      <Value lang="he_IL">Adobe CMaps CS6</Value>
      <Value lang="hi_IN">Adobe CMaps CS6</Value>
      <Value lang="hr_HR">Adobe CMaps CS6</Value>
      <Value lang="hu_HU">Adobe CMaps CS6</Value>
      <Value lang="is_IS">Adobe CMaps CS6</Value>
      <Value lang="it_IT">Adobe CMaps CS6</Value>
      <Value lang="ja_JP">Adobe CMaps CS6</Value>
      <Value lang="ko_KR">Adobe CMaps CS6</Value>
      <Value lang="lt_LT">Adobe CMaps CS6</Value>
      <Value lang="lv_LV">Adobe CMaps CS6</Value>
      <Value lang="mk_MK">Adobe CMaps CS6</Value>
      <Value lang="nb_NO">Adobe CMaps CS6</Value>
      <Value lang="nl_NL">Adobe CMaps CS6</Value>
      <Value lang="nn_NO">Adobe CMaps CS6</Value>
      <Value lang="no_NO">Adobe CMaps CS6</Value>
      <Value lang="pl_PL">Adobe CMaps CS6</Value>
      <Value lang="pt_BR">Adobe CMaps CS6</Value>
      <Value lang="ro_RO">Adobe CMaps CS6</Value>
      <Value lang="ru_RU">Adobe CMaps CS6</Value>
      <Value lang="sh_YU">Adobe CMaps CS6</Value>
      <Value lang="sk_SK">Adobe CMaps CS6</Value>
      <Value lang="sl_SI">Adobe CMaps CS6</Value>
      <Value lang="sq_AL">Adobe CMaps CS6</Value>
      <Value lang="sv_SE">Adobe CMaps CS6</Value>
      <Value lang="th_TH">Adobe CMaps CS6</Value>
      <Value lang="tr_TR">Adobe CMaps CS6</Value>
      <Value lang="uk_UA">Adobe CMaps CS6</Value>
      <Value lang="vi_VN">Adobe CMaps CS6</Value>
      <Value lang="zh_CN">Adobe CMaps CS6</Value>
      <Value lang="zh_TW">Adobe CMaps CS6</Value>
      <Value lang="en_AE">Adobe CMaps CS6</Value>
      <Value lang="en_IL">Adobe CMaps CS6</Value>
      <Value lang="fr_MA">Adobe CMaps CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0", "ChannelID", "AdobeCMapsCS6-4.0")
INSERT INTO PayloadData VALUES("{E8B1DAAA-0B6B-44E6-A2D3-8E418EA0EA85}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeCMapsCS6-4.0">
    <DisplayName>Adobe CMaps CS6</DisplayName>
  </Channel>')
