CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO Payloads VALUES	("{9D2A060F-AC6B-11E0-8C00-00215AEA26C9}", "NonAdobePayload", "Microsoft Visual C++ 2010 Redistributable Package (x64)", "10.0.40219.1", "normal")
INSERT INTO PayloadData VALUES("{9D2A060F-AC6B-11E0-8C00-00215AEA26C9}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.0.0"><BuildInfo>
		<Property name="Created">08/04/11 13:22:37</Property>
		<Property name="TargetName">Microsoft VC 2010 Redist (x64)</Property>
		<Property name="ProcessorFamily">x64</Property>
	</BuildInfo><InstallerProperties>
		<Property name="AdobeCode">{9D2A060F-AC6B-11E0-8C00-00215AEA26C9}</Property>
		<Property name="ProductName">Microsoft Visual C++ 2010 Redistributable Package (x64)</Property>
		<Property name="ProductVersion">10.0.40219.1</Property>
	</InstallerProperties><InstallDir>
		<Platform isFixed="1" name="Default" folderName="">[AdobeProgramFiles]</Platform>
	</InstallDir><Languages languageIndependent="1"/><Satisfies>
		<ProductInfo>
			<Family>NonAdobePayload</Family>
			<ProductName>Microsoft Visual C++ 2010 Redistributable Package (x64)</ProductName>
			<ProductVersion>10.0.40219.1</ProductVersion>
		</ProductInfo>
	</Satisfies><InstallDestinationMetadata relocatableSize="0" sysDriveSize="10485760">
		<Destination>
			<Root>[AdobeProgramFiles]</Root>
			<TotalSize>10485760</TotalSize>
			<MaxPathComponent>/</MaxPathComponent>
		</Destination>
	</InstallDestinationMetadata><ThirdPartyComponent>
		<Metadata>
			<Type>application</Type>
			<Name>vcredist_x64.exe</Name>
		</Metadata>
		<Arguments delimiter="" prefix="">
			<Install>
				<Argument name="/q"/>
				<Argument name="/norestart"/>
			</Install>
			<Repair>
				<Argument name="/q"/>
				<Argument name="/norestart"/>
			</Repair>
		</Arguments>
		<SuccessCodes>
			<Install>
				<Range lowerBound="0" upperBound="0"/>
				<Range lowerBound="3010" upperBound="3010"/>
			</Install>
			<Repair>
				<Range lowerBound="0" upperBound="0"/>
				<Range lowerBound="3010" upperBound="3010"/>
			</Repair>
		</SuccessCodes>
		<Capabilities>
			<Install>
				<Value lang="en_US">You just installed Microsoft Visual C++ 2010 Redistributable Package (x64).</Value>
			</Install>
			<Repair isRepairable="1">
				<Value lang="en_US">You just repaired Microsoft Visual C++ 2010 Redistributable Package (x64).</Value>
			</Repair>
			<Uninstall isUninstallable="0">
				<Value lang="en_US">Microsoft Visual C++ 2010 Redistributable Package (x64) was not removed. Please use the "Add/Remove Programs" utility in the Control Panel to remove Microsoft Visual C++ 2010 Redistributable Package (x64).</Value>
			</Uninstall>
		</Capabilities>
		<ErrorMessages>
			<Install>
				<Value lang="en_US">Failed to install Microsoft Visual C++ 2010 Redistributable Package (x64). Please try installing it by double clicking on the executable at "[mediaPath]", or download and install the latest Microsoft Visual C++ 2010 Redistributable Package (x64) from Microsoft website - www.microsoft.com</Value>
			</Install>
			<Repair>
				<Value lang="en_US">Failed to repair Microsoft Visual C++ 2010 Redistributable Package (x64). Please try repairing it by double clicking on the executable at "[mediaPath]", or download and install the latest Microsoft Visual C++ 2010 Redistributable Package (x64) from Microsoft website - www.microsoft.com</Value>
			</Repair>
		</ErrorMessages>
	</ThirdPartyComponent></PayloadInfo>')
