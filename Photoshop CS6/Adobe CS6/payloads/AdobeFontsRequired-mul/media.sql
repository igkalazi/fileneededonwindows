CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{CBDD7465-CE11-4A58-9497-C370B65923F1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{2B789F07-2585-49FB-9A53-FB177B76C9B6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{6A5EB18D-4233-4A86-97E5-8DFAF878E009}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{2ADEBA39-5AA3-4AA3-91B1-B139BE9A9670}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{46382B59-8AE9-4110-A9BD-1DAE4DF716C6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{92F12423-0348-4CD3-8F46-9554D3E6A584}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{EB86B118-A326-477A-931E-20D3B4B0FABD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{CA5C8D51-D99A-426C-97BE-8A0D0F46C6C7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{3F2EDF52-F5EA-4568-9145-FCF4B6730C4B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{6ED79D14-0DD0-4F3B-ABD3-BA922F4566CC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{D2C571DB-893B-44EF-AEB7-E63361CFDE41}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{F9814922-981D-4D9F-9FBF-51C82139FB08}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{2873A3B2-C540-4930-9304-62AB9ED170DE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{F2F3DDFB-C1FE-4983-96CF-09A17546200B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{121CDAEB-B7E3-44B0-9085-D927029D8EAF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{E900978A-48E0-47EE-888F-0573140A3518}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{BA5D2F61-D181-440D-B2DF-0732AD3CAB70}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{7E7BF117-AC58-4107-9C57-3EB4BAFA6243}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{D24358E4-5FD7-4B22-9C25-8191CCE6FBC4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "{F71B0169-77C6-4664-A3ED-750A9B47863D}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{311CDC89-AC18-4344-9EC9-0225328C73D3}"/>')
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0" , "ValidationSig", "dwCvcBY+MKgW5pCegDjgAhvC8H7WeBgeRBj0vPnC51dXTJbI1gJ8PpkC95dKLmMnazBqx/SAQFrVaSHVlx8CvcKsYIPC+Ubdyj0Fav8XRiem4ui4e27BQAGzfvTNRh4o2nYXgsKKO0FSr/5LyTG/TgfSWMnGH4FhvJ6MTU/ixnRlMtwIpwRAk6p9KNoZtOTT8x95te0gz/oUNQEqkzaXEak7mNcpG/VugYa6o3ApUv6ty6uDF8RHQrYu12UFCsu5ukfEoHOdD3+giCzkyDBdKg3YxgAfAEgw8Wl6yVmIG+d9P55SKBdSQNBxrhD7RwZ2EPNa/Cj2sDDZrzltvbHmooltKaZOmUxZ7UnWR9de2roS+5ogF06JaMNHR1ELWptZSmvlYHIwBRF1dcfXqdT0qfardjBWShN6sO9bVuGYRjpvi01bOUu5eKwXfgYzPXYyYg02tWQbVqFpmZjXbyLixF3sX4XvSNFf4q76/DcqFA6VPT+2UC0U5eo/Cw8bwHW2")
INSERT INTO Branding VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5UeXBlU3Vw
cG9ydC1JbnN0YWxsZXItQWRvYmVDb2RlPC9EYXRhPg0KICAgIDwvUGF5bG9hZD4NCjwvQ29uZmln
dXJhdGlvbj4NCg==')
INSERT INTO Payloads VALUES	("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "Type Group", "Required Common Fonts Installation", "2.0", "normal")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-02-15 11:54:00.827000</Property>
    <Property name="TargetName">AdobeFontsRequired-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{311CDC89-AC18-4344-9EC9-0225328C73D3}</Property>
    <Property name="ProductName">Adobe Fonts Required</Property>
    <Property name="ProductVersion">2.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[FontsFolder]</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>Type Group</Family>
		<ProductName>Required Common Fonts Installation</ProductName>
	<ProductVersion>2.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{CBDD7465-CE11-4A58-9497-C370B65923F1}</AdobeCode>
    <AdobeCode>{2B789F07-2585-49FB-9A53-FB177B76C9B6}</AdobeCode>
    <AdobeCode>{6A5EB18D-4233-4A86-97E5-8DFAF878E009}</AdobeCode>
    <AdobeCode>{2ADEBA39-5AA3-4AA3-91B1-B139BE9A9670}</AdobeCode>
    <AdobeCode>{46382B59-8AE9-4110-A9BD-1DAE4DF716C6}</AdobeCode>
    <AdobeCode>{92F12423-0348-4CD3-8F46-9554D3E6A584}</AdobeCode>
    <AdobeCode>{EB86B118-A326-477A-931E-20D3B4B0FABD}</AdobeCode>
    <AdobeCode>{CA5C8D51-D99A-426C-97BE-8A0D0F46C6C7}</AdobeCode>
    <AdobeCode>{3F2EDF52-F5EA-4568-9145-FCF4B6730C4B}</AdobeCode>
    <AdobeCode>{6ED79D14-0DD0-4F3B-ABD3-BA922F4566CC}</AdobeCode>
    <AdobeCode>{D2C571DB-893B-44EF-AEB7-E63361CFDE41}</AdobeCode>
    <AdobeCode>{F9814922-981D-4D9F-9FBF-51C82139FB08}</AdobeCode>
    <AdobeCode>{2873A3B2-C540-4930-9304-62AB9ED170DE}</AdobeCode>
    <AdobeCode>{F2F3DDFB-C1FE-4983-96CF-09A17546200B}</AdobeCode>
    <AdobeCode>{121CDAEB-B7E3-44B0-9085-D927029D8EAF}</AdobeCode>
    <AdobeCode>{E900978A-48E0-47EE-888F-0573140A3518}</AdobeCode>
    <AdobeCode>{BA5D2F61-D181-440D-B2DF-0732AD3CAB70}</AdobeCode>
    <AdobeCode>{7E7BF117-AC58-4107-9C57-3EB4BAFA6243}</AdobeCode>
    <AdobeCode>{D24358E4-5FD7-4B22-9C25-8191CCE6FBC4}</AdobeCode>
    <AdobeCode>{F71B0169-77C6-4664-A3ED-750A9B47863D}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeFontsRequired-2.0">
    <DisplayName>Adobe Fonts Required</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\FontsRequired\2.0\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="117267738"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>238</TotalSize>
      <MaxPathComponent>/FontsRequired/2.0/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[FontsFolder]</Root>
      <TotalSize>117267500</TotalSize>
      <MaxPathComponent>LetterGothicStd-BoldSlanted.otf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="117267738"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">2.0</Value>
      <Value lang="be_BY">2.0</Value>
      <Value lang="bg_BG">2.0</Value>
      <Value lang="ca_ES">2.0</Value>
      <Value lang="cs_CZ">2.0</Value>
      <Value lang="da_DK">2.0</Value>
      <Value lang="de_DE">2.0</Value>
      <Value lang="el_GR">2.0</Value>
      <Value lang="en_GB">2.0</Value>
      <Value lang="en_MX">2.0</Value>
      <Value lang="en_US">2.0</Value>
      <Value lang="en_XC">2.0</Value>
      <Value lang="en_XM">2.0</Value>
      <Value lang="es_ES">2.0</Value>
      <Value lang="es_MX">2.0</Value>
      <Value lang="es_QM">2.0</Value>
      <Value lang="et_EE">2.0</Value>
      <Value lang="fi_FI">2.0</Value>
      <Value lang="fr_CA">2.0</Value>
      <Value lang="fr_FR">2.0</Value>
      <Value lang="fr_MX">2.0</Value>
      <Value lang="fr_XM">2.0</Value>
      <Value lang="he_IL">2.0</Value>
      <Value lang="hi_IN">2.0</Value>
      <Value lang="hr_HR">2.0</Value>
      <Value lang="hu_HU">2.0</Value>
      <Value lang="is_IS">2.0</Value>
      <Value lang="it_IT">2.0</Value>
      <Value lang="ja_JP">2.0</Value>
      <Value lang="ko_KR">2.0</Value>
      <Value lang="lt_LT">2.0</Value>
      <Value lang="lv_LV">2.0</Value>
      <Value lang="mk_MK">2.0</Value>
      <Value lang="nb_NO">2.0</Value>
      <Value lang="nl_NL">2.0</Value>
      <Value lang="nn_NO">2.0</Value>
      <Value lang="no_NO">2.0</Value>
      <Value lang="pl_PL">2.0</Value>
      <Value lang="pt_BR">2.0</Value>
      <Value lang="ro_RO">2.0</Value>
      <Value lang="ru_RU">2.0</Value>
      <Value lang="sh_YU">2.0</Value>
      <Value lang="sk_SK">2.0</Value>
      <Value lang="sl_SI">2.0</Value>
      <Value lang="sq_AL">2.0</Value>
      <Value lang="sv_SE">2.0</Value>
      <Value lang="th_TH">2.0</Value>
      <Value lang="tr_TR">2.0</Value>
      <Value lang="uk_UA">2.0</Value>
      <Value lang="vi_VN">2.0</Value>
      <Value lang="zh_CN">2.0</Value>
      <Value lang="zh_TW">2.0</Value>
      <Value lang="en_AE">2.0</Value>
      <Value lang="en_IL">2.0</Value>
      <Value lang="fr_MA">2.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Fonts Required</Value>
      <Value lang="be_BY">Adobe Fonts Required</Value>
      <Value lang="bg_BG">Adobe Fonts Required</Value>
      <Value lang="ca_ES">Adobe Fonts Required</Value>
      <Value lang="cs_CZ">Adobe Fonts Required</Value>
      <Value lang="da_DK">Adobe Fonts Required</Value>
      <Value lang="de_DE">Adobe Fonts Required</Value>
      <Value lang="el_GR">Adobe Fonts Required</Value>
      <Value lang="en_GB">Adobe Fonts Required</Value>
      <Value lang="en_MX">Adobe Fonts Required</Value>
      <Value lang="en_US">Adobe Fonts Required</Value>
      <Value lang="en_XC">Adobe Fonts Required</Value>
      <Value lang="en_XM">Adobe Fonts Required</Value>
      <Value lang="es_ES">Adobe Fonts Required</Value>
      <Value lang="es_MX">Adobe Fonts Required</Value>
      <Value lang="es_QM">Adobe Fonts Required</Value>
      <Value lang="et_EE">Adobe Fonts Required</Value>
      <Value lang="fi_FI">Adobe Fonts Required</Value>
      <Value lang="fr_CA">Adobe Fonts Required</Value>
      <Value lang="fr_FR">Adobe Fonts Required</Value>
      <Value lang="fr_MX">Adobe Fonts Required</Value>
      <Value lang="fr_XM">Adobe Fonts Required</Value>
      <Value lang="he_IL">Adobe Fonts Required</Value>
      <Value lang="hi_IN">Adobe Fonts Required</Value>
      <Value lang="hr_HR">Adobe Fonts Required</Value>
      <Value lang="hu_HU">Adobe Fonts Required</Value>
      <Value lang="is_IS">Adobe Fonts Required</Value>
      <Value lang="it_IT">Adobe Fonts Required</Value>
      <Value lang="ja_JP">Adobe Fonts Required</Value>
      <Value lang="ko_KR">Adobe Fonts Required</Value>
      <Value lang="lt_LT">Adobe Fonts Required</Value>
      <Value lang="lv_LV">Adobe Fonts Required</Value>
      <Value lang="mk_MK">Adobe Fonts Required</Value>
      <Value lang="nb_NO">Adobe Fonts Required</Value>
      <Value lang="nl_NL">Adobe Fonts Required</Value>
      <Value lang="nn_NO">Adobe Fonts Required</Value>
      <Value lang="no_NO">Adobe Fonts Required</Value>
      <Value lang="pl_PL">Adobe Fonts Required</Value>
      <Value lang="pt_BR">Adobe Fonts Required</Value>
      <Value lang="ro_RO">Adobe Fonts Required</Value>
      <Value lang="ru_RU">Adobe Fonts Required</Value>
      <Value lang="sh_YU">Adobe Fonts Required</Value>
      <Value lang="sk_SK">Adobe Fonts Required</Value>
      <Value lang="sl_SI">Adobe Fonts Required</Value>
      <Value lang="sq_AL">Adobe Fonts Required</Value>
      <Value lang="sv_SE">Adobe Fonts Required</Value>
      <Value lang="th_TH">Adobe Fonts Required</Value>
      <Value lang="tr_TR">Adobe Fonts Required</Value>
      <Value lang="uk_UA">Adobe Fonts Required</Value>
      <Value lang="vi_VN">Adobe Fonts Required</Value>
      <Value lang="zh_CN">Adobe Fonts Required</Value>
      <Value lang="zh_TW">Adobe Fonts Required</Value>
      <Value lang="en_AE">Adobe Fonts Required</Value>
      <Value lang="en_IL">Adobe Fonts Required</Value>
      <Value lang="fr_MA">Adobe Fonts Required</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0", "AMTConfigPath", "[AdobeCommon]\FontsRequired\2.0\AMT\component.xml")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0", "ChannelID", "AdobeFontsRequired-2.0")
INSERT INTO PayloadData VALUES("{311CDC89-AC18-4344-9EC9-0225328C73D3}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeFontsRequired-2.0">
    <DisplayName>Adobe Fonts Required</DisplayName>
  </Channel>')
