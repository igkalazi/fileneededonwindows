CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{393435ED-BF4D-4553-9442-757572660A94}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{5E061DEE-FF92-416B-ABAD-E78CC5A909BF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{AABB08A6-5D28-48FB-8F54-FA8B22005501}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{348C0B8A-DAFE-4441-B130-74C364BECD04}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{BCCF8D81-AF9A-4521-857E-25C58D6528BC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{8C8E1FBD-2203-4708-AF37-01EB42C2EBD4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{72F6E903-6F9F-4BD9-8FAB-4A349911FF69}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{4DA71233-E557-43D5-A2C3-CE2932FCD70E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{9A0B19F9-B469-4762-8C60-FDBC8A5EC1A3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "{7AD363EC-F42F-4362-A7E1-73C67027AC80}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{26F763C9-076F-473D-9A0E-4050C973737C}"/>')
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0" , "ValidationSig", "OKoCXuRPqumQ30knUiMVrajB7rw8O6oZH4jNQ4TSES0y89UBZZu9CbhRJI5536171HGCrlgFd601Lrws5WlaNABCZBSWEJstc8dtmhdt0hILU8HLuXXbiVUC0uIxVIP3vNy/sAg4IQQfs8T7kprjx0OzH/t9dcKTsi8/TzjQ4LnA1OpJLRGfGTPGjj+GDKKjHX9s7ylVKjJ36bVqln5zEqJ40qFNrfaZ2QjliAC0qGpRl9RbFAtjyA/hAkBhfNZV8f92Ob/cXlmk0H8UWjgJ+wPm8PuUicTWifBKRFbPQvKn+gb+xokJHAo9Ff8XBBn5Qp0iPq0Khx8le/X+LliYxj67kTyByUhWYLu6rByCWKG3O3QAfSiP3/DpcumDvwbdLwzoPyg8j08mP67tMP6ntkfeXcbqcUkAxw8dh49Abh7daoFke5loGNBrmDLNJoeWXQoc+G2JW7rADKsqCXuh/Kze+b/dALEtTFYhJk9UDrix9lri+JZhl8ftSFx3JE63")
INSERT INTO Payloads VALUES	("{26F763C9-076F-473D-9A0E-4050C973737C}", "CoreTech", "AdobeColorJA CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-15 16:30:01.009000</Property>
    <Property name="TargetName">AdobeColorJA_ExtraSettings4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{26F763C9-076F-473D-9A0E-4050C973737C}</Property>
    <Property name="ProductName">Adobe Color JA Extra Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>en_US</Language>
    <Language>ar_AE</Language>
    <Language>cs_CZ</Language>
    <Language>da_DK</Language>
    <Language>de_DE</Language>
    <Language>el_GR</Language>
    <Language>en_GB</Language>
    <Language>en_AE</Language>
    <Language>en_IL</Language>
    <Language>es_ES</Language>
    <Language>fi_FI</Language>
    <Language>fr_FR</Language>
    <Language>fr_MA</Language>
    <Language>he_IL</Language>
    <Language>hu_HU</Language>
    <Language>it_IT</Language>
    <Language>nb_NO</Language>
    <Language>nl_NL</Language>
    <Language>pl_PL</Language>
    <Language>pt_BR</Language>
    <Language>ro_RO</Language>
    <Language>ru_RU</Language>
    <Language>sv_SE</Language>
    <Language>tr_TR</Language>
    <Language>uk_UA</Language>
    <Language>fr_CA</Language>
    <Language>es_MX</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorJA CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{393435ED-BF4D-4553-9442-757572660A94}</AdobeCode>
    <AdobeCode>{5E061DEE-FF92-416B-ABAD-E78CC5A909BF}</AdobeCode>
    <AdobeCode>{AABB08A6-5D28-48FB-8F54-FA8B22005501}</AdobeCode>
    <AdobeCode>{348C0B8A-DAFE-4441-B130-74C364BECD04}</AdobeCode>
    <AdobeCode>{BCCF8D81-AF9A-4521-857E-25C58D6528BC}</AdobeCode>
    <AdobeCode>{8C8E1FBD-2203-4708-AF37-01EB42C2EBD4}</AdobeCode>
    <AdobeCode>{72F6E903-6F9F-4BD9-8FAB-4A349911FF69}</AdobeCode>
    <AdobeCode>{4DA71233-E557-43D5-A2C3-CE2932FCD70E}</AdobeCode>
    <AdobeCode>{9A0B19F9-B469-4762-8C60-FDBC8A5EC1A3}</AdobeCode>
    <AdobeCode>{7AD363EC-F42F-4362-A7E1-73C67027AC80}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorJAExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color JA Extra Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2867004"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2867004</TotalSize>
      <MaxPathComponent>/Color/Settings/ExtraSettings\Japan General Purpose.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2867004"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="en_US">4.0</Value>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="es_MX">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="en_US">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="ar_AE">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="cs_CZ">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="da_DK">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="de_DE">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="el_GR">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="en_GB">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="en_AE">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="en_IL">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="es_ES">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="fi_FI">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="fr_FR">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="fr_MA">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="he_IL">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="hu_HU">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="it_IT">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="nb_NO">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="nl_NL">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="pl_PL">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="pt_BR">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="ro_RO">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="ru_RU">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="sv_SE">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="tr_TR">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="uk_UA">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="fr_CA">Adobe Color JA Extra Settings CS6</Value>
      <Value lang="es_MX">Adobe Color JA Extra Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0", "ChannelID", "AdobeColorJAExtraSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{26F763C9-076F-473D-9A0E-4050C973737C}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorJAExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color JA Extra Settings CS6</DisplayName>
  </Channel>')
