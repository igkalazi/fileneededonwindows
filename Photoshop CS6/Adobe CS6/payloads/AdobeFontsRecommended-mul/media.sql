CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{E39A5ABD-516B-46F9-A042-694BBA6A4E08}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{CDE36054-7E90-40F7-99BC-544663700AB9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{CA6A3A47-79CC-441C-9AEA-4763FB0D7103}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{DE188850-ECD0-4F60-A5C5-C736C6697F39}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{569E00C5-894E-465A-92AC-61E59DFBB2F9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{6EB24DD9-6A54-43BB-B65B-69E521E885CF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{5D1AA4FD-3DBD-419B-9820-50EA9134621C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{833A05DB-9874-4F6F-9193-F603F7679FEA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{09E577BB-E976-45D8-945B-95A230DC7D65}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{0AA663D3-745D-4BEC-A869-0704387DFAE1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{9D5273F3-012C-48BE-BE13-E6FE02D2449D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{1D69DB69-A530-4F67-823C-04315C5A219A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{2D42C223-8F91-430A-AF02-302BCFFF78CE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{F237C6A6-EC8A-4039-936F-190697BBB3C3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{9AB95E57-2CE6-4229-99B4-1E37B3B586F3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "{751FD2F7-BACA-4E00-8889-22314FCF1AA3}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}"/>')
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0" , "ValidationSig", "fDYbx8f/gQx5KzgQkFqO4bqcQplBHYE1xP3wfgX22zQ8Le3C4xTNh/ytZ6AisrDt9ZyYjNSy2ODM/Dox0nnkJ5VrnSD4a0V1ux0y1ofbrub+01wD1U8QwO5Q6ybbM7gt41zWMcGHJkjSqQWXhKPHuC3gmZqjUeST7MUpV8tWyY1ujpMLmaeqsK3qHIMmqXznUNusuICmcNwyAGXiS/6R4o6EjtxaiFB7wvV4jJhdQ4+QMDfrHYBOQhQqNmjGBf3Nw8QRZJz/e/qKGd06SAuPnh/4DDcP5qT6Q+WiWESDwQdxX7RKfc9v9qTy/AW62wdytGW9vMFvR+pwJz7cLSQ7HeC20dXCM+bqm4ws4qZHAoz9fnwEKPajvU7SAIu4lhsrsVrUwqWks6EwI4XqdFvZ1Niw++XkH7vuOzmjubl7arLExXmpGH+C9ggulK4JzFjfXtfdfLzzSZrhul6R+0Kx6xsKROIq4/pfvF+Mhip3fMzARy3CwAc8fBS82UwXFGB9")
INSERT INTO Branding VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5UeXBlU3Vw
cG9ydC1JbnN0YWxsZXItQWRvYmVDb2RlPC9EYXRhPg0KICAgIDwvUGF5bG9hZD4NCjwvQ29uZmln
dXJhdGlvbj4NCg==')
INSERT INTO Payloads VALUES	("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "Type Group", "Recommended Common Fonts Installation", "2.0", "normal")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-26 16:13:01.198000</Property>
    <Property name="TargetName">AdobeFontsRecommended-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}</Property>
    <Property name="ProductName">Adobe Fonts Recommended</Property>
    <Property name="ProductVersion">2.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[FontsFolder]</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>Type Group</Family>
		<ProductName>Recommended Common Fonts Installation</ProductName>
	<ProductVersion>2.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{E39A5ABD-516B-46F9-A042-694BBA6A4E08}</AdobeCode>
    <AdobeCode>{CDE36054-7E90-40F7-99BC-544663700AB9}</AdobeCode>
    <AdobeCode>{CA6A3A47-79CC-441C-9AEA-4763FB0D7103}</AdobeCode>
    <AdobeCode>{DE188850-ECD0-4F60-A5C5-C736C6697F39}</AdobeCode>
    <AdobeCode>{569E00C5-894E-465A-92AC-61E59DFBB2F9}</AdobeCode>
    <AdobeCode>{6EB24DD9-6A54-43BB-B65B-69E521E885CF}</AdobeCode>
    <AdobeCode>{5D1AA4FD-3DBD-419B-9820-50EA9134621C}</AdobeCode>
    <AdobeCode>{833A05DB-9874-4F6F-9193-F603F7679FEA}</AdobeCode>
    <AdobeCode>{09E577BB-E976-45D8-945B-95A230DC7D65}</AdobeCode>
    <AdobeCode>{0AA663D3-745D-4BEC-A869-0704387DFAE1}</AdobeCode>
    <AdobeCode>{9D5273F3-012C-48BE-BE13-E6FE02D2449D}</AdobeCode>
    <AdobeCode>{1D69DB69-A530-4F67-823C-04315C5A219A}</AdobeCode>
    <AdobeCode>{2D42C223-8F91-430A-AF02-302BCFFF78CE}</AdobeCode>
    <AdobeCode>{F237C6A6-EC8A-4039-936F-190697BBB3C3}</AdobeCode>
    <AdobeCode>{9AB95E57-2CE6-4229-99B4-1E37B3B586F3}</AdobeCode>
    <AdobeCode>{751FD2F7-BACA-4E00-8889-22314FCF1AA3}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeFontsRecommended-2.0">
    <DisplayName>Adobe Fonts Recommended</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\FontsRecommended\2.0\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="104486890"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>238</TotalSize>
      <MaxPathComponent>/FontsRecommended/2.0/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[FontsFolder]</Root>
      <TotalSize>104486652</TotalSize>
      <MaxPathComponent>ACaslonPro-SemiboldItalic.otf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="104486890"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">2.0</Value>
      <Value lang="be_BY">2.0</Value>
      <Value lang="bg_BG">2.0</Value>
      <Value lang="ca_ES">2.0</Value>
      <Value lang="cs_CZ">2.0</Value>
      <Value lang="da_DK">2.0</Value>
      <Value lang="de_DE">2.0</Value>
      <Value lang="el_GR">2.0</Value>
      <Value lang="en_GB">2.0</Value>
      <Value lang="en_MX">2.0</Value>
      <Value lang="en_US">2.0</Value>
      <Value lang="en_XC">2.0</Value>
      <Value lang="en_XM">2.0</Value>
      <Value lang="es_ES">2.0</Value>
      <Value lang="es_MX">2.0</Value>
      <Value lang="es_QM">2.0</Value>
      <Value lang="et_EE">2.0</Value>
      <Value lang="fi_FI">2.0</Value>
      <Value lang="fr_CA">2.0</Value>
      <Value lang="fr_FR">2.0</Value>
      <Value lang="fr_MX">2.0</Value>
      <Value lang="fr_XM">2.0</Value>
      <Value lang="he_IL">2.0</Value>
      <Value lang="hi_IN">2.0</Value>
      <Value lang="hr_HR">2.0</Value>
      <Value lang="hu_HU">2.0</Value>
      <Value lang="is_IS">2.0</Value>
      <Value lang="it_IT">2.0</Value>
      <Value lang="ja_JP">2.0</Value>
      <Value lang="ko_KR">2.0</Value>
      <Value lang="lt_LT">2.0</Value>
      <Value lang="lv_LV">2.0</Value>
      <Value lang="mk_MK">2.0</Value>
      <Value lang="nb_NO">2.0</Value>
      <Value lang="nl_NL">2.0</Value>
      <Value lang="nn_NO">2.0</Value>
      <Value lang="no_NO">2.0</Value>
      <Value lang="pl_PL">2.0</Value>
      <Value lang="pt_BR">2.0</Value>
      <Value lang="ro_RO">2.0</Value>
      <Value lang="ru_RU">2.0</Value>
      <Value lang="sh_YU">2.0</Value>
      <Value lang="sk_SK">2.0</Value>
      <Value lang="sl_SI">2.0</Value>
      <Value lang="sq_AL">2.0</Value>
      <Value lang="sv_SE">2.0</Value>
      <Value lang="th_TH">2.0</Value>
      <Value lang="tr_TR">2.0</Value>
      <Value lang="uk_UA">2.0</Value>
      <Value lang="vi_VN">2.0</Value>
      <Value lang="zh_CN">2.0</Value>
      <Value lang="zh_TW">2.0</Value>
      <Value lang="en_AE">2.0</Value>
      <Value lang="en_IL">2.0</Value>
      <Value lang="fr_MA">2.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Fonts Recommended</Value>
      <Value lang="be_BY">Adobe Fonts Recommended</Value>
      <Value lang="bg_BG">Adobe Fonts Recommended</Value>
      <Value lang="ca_ES">Adobe Fonts Recommended</Value>
      <Value lang="cs_CZ">Adobe Fonts Recommended</Value>
      <Value lang="da_DK">Adobe Fonts Recommended</Value>
      <Value lang="de_DE">Adobe Fonts Recommended</Value>
      <Value lang="el_GR">Adobe Fonts Recommended</Value>
      <Value lang="en_GB">Adobe Fonts Recommended</Value>
      <Value lang="en_MX">Adobe Fonts Recommended</Value>
      <Value lang="en_US">Adobe Fonts Recommended</Value>
      <Value lang="en_XC">Adobe Fonts Recommended</Value>
      <Value lang="en_XM">Adobe Fonts Recommended</Value>
      <Value lang="es_ES">Adobe Fonts Recommended</Value>
      <Value lang="es_MX">Adobe Fonts Recommended</Value>
      <Value lang="es_QM">Adobe Fonts Recommended</Value>
      <Value lang="et_EE">Adobe Fonts Recommended</Value>
      <Value lang="fi_FI">Adobe Fonts Recommended</Value>
      <Value lang="fr_CA">Adobe Fonts Recommended</Value>
      <Value lang="fr_FR">Adobe Fonts Recommended</Value>
      <Value lang="fr_MX">Adobe Fonts Recommended</Value>
      <Value lang="fr_XM">Adobe Fonts Recommended</Value>
      <Value lang="he_IL">Adobe Fonts Recommended</Value>
      <Value lang="hi_IN">Adobe Fonts Recommended</Value>
      <Value lang="hr_HR">Adobe Fonts Recommended</Value>
      <Value lang="hu_HU">Adobe Fonts Recommended</Value>
      <Value lang="is_IS">Adobe Fonts Recommended</Value>
      <Value lang="it_IT">Adobe Fonts Recommended</Value>
      <Value lang="ja_JP">Adobe Fonts Recommended</Value>
      <Value lang="ko_KR">Adobe Fonts Recommended</Value>
      <Value lang="lt_LT">Adobe Fonts Recommended</Value>
      <Value lang="lv_LV">Adobe Fonts Recommended</Value>
      <Value lang="mk_MK">Adobe Fonts Recommended</Value>
      <Value lang="nb_NO">Adobe Fonts Recommended</Value>
      <Value lang="nl_NL">Adobe Fonts Recommended</Value>
      <Value lang="nn_NO">Adobe Fonts Recommended</Value>
      <Value lang="no_NO">Adobe Fonts Recommended</Value>
      <Value lang="pl_PL">Adobe Fonts Recommended</Value>
      <Value lang="pt_BR">Adobe Fonts Recommended</Value>
      <Value lang="ro_RO">Adobe Fonts Recommended</Value>
      <Value lang="ru_RU">Adobe Fonts Recommended</Value>
      <Value lang="sh_YU">Adobe Fonts Recommended</Value>
      <Value lang="sk_SK">Adobe Fonts Recommended</Value>
      <Value lang="sl_SI">Adobe Fonts Recommended</Value>
      <Value lang="sq_AL">Adobe Fonts Recommended</Value>
      <Value lang="sv_SE">Adobe Fonts Recommended</Value>
      <Value lang="th_TH">Adobe Fonts Recommended</Value>
      <Value lang="tr_TR">Adobe Fonts Recommended</Value>
      <Value lang="uk_UA">Adobe Fonts Recommended</Value>
      <Value lang="vi_VN">Adobe Fonts Recommended</Value>
      <Value lang="zh_CN">Adobe Fonts Recommended</Value>
      <Value lang="zh_TW">Adobe Fonts Recommended</Value>
      <Value lang="en_AE">Adobe Fonts Recommended</Value>
      <Value lang="en_IL">Adobe Fonts Recommended</Value>
      <Value lang="fr_MA">Adobe Fonts Recommended</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0", "AMTConfigPath", "[AdobeCommon]\FontsRecommended\2.0\AMT\component.xml")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0", "ChannelID", "AdobeFontsRecommended-2.0")
INSERT INTO PayloadData VALUES("{DE7C6FA1-AF75-48A8-B495-CFAD529BCC3D}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeFontsRecommended-2.0">
    <DisplayName>Adobe Fonts Recommended</DisplayName>
  </Channel>')
