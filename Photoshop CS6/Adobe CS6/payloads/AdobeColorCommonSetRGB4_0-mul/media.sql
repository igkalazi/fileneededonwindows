CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{1767A3B0-3684-49F5-B4CB-71B0B6394EB8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{68A4A8F9-3DE8-47F2-BC9D-1C1CB4E5D2DF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{80FC4840-6688-4C5E-8551-C9A2BD9643F8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{7A47BA79-66C8-45A3-8DBA-278CB0F9EAC2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{DC6F80ED-184A-4FA3-88FE-AFE1C2616128}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{80741FC3-0411-4AC2-A4A7-20185882761E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{DBD80AE5-F05F-4D15-A30D-4E4CAC267626}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{02C12514-9196-4E0E-B75C-B83CEE1EFB33}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{F4BDCA90-1710-4259-9F1D-CE7A375EB0C2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "{598A9D65-3818-4D95-8B0B-24A2358CAD50}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{C7B1C1B3-368D-4C32-A818-83F1554EB398}"/>')
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0" , "ValidationSig", "e8YcKNoUBzjqfkYveU2jIU24z0g/fGbeQ5AEsiPc6XkbNSA4yB+hBiLidO51nUmFnQLHwLGj0LK4yCBoPY+jAJdvC88O+d2ZPWZ2Yo7zvJtHhO/6SR0k6+oqLDj5NluKokueAw43x02EvVVn2tyd4ZtmALLKkei7mdTJNKkLYMPdhSkDWk/ndJteUUkC6iar3Noelxd3ugvy3CVg779V51POSE6fYUeIzsOP35M3I+SR9ttu41t1JwRGcqa8PZnq+5TttxKG0UDtK/kwou0182u5K7E2UW+dwxlAQE8Tp1H6wiL80v0xY4x7rfkEKFuSd5FoFMTgZc7MLdR43yMPqtgXQP5W+7vRWc7wMCwoZ9Y2LvaPXGmrbLOz7kaZuzr89Savt6zU6vv2RaOoha5TGK1+ki/RSRYqvhz+9wnwooKtHY0X6eTsIkMFYsLhY8GJP5kVQiHrT1qe6JcCMkO736V77PbFJbFb4959pm9XsyfAVOkU06oVYAzzdiKIAgnv")
INSERT INTO Payloads VALUES	("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "CoreTech", "AdobeColorCommonSetRGB CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-09 00:17:56.676000</Property>
    <Property name="TargetName">AdobeColorCommonSetRGB4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{C7B1C1B3-368D-4C32-A818-83F1554EB398}</Property>
    <Property name="ProductName">AdobeColorCommonSetRGB</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorCommonSetRGB CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{1767A3B0-3684-49F5-B4CB-71B0B6394EB8}</AdobeCode>
    <AdobeCode>{68A4A8F9-3DE8-47F2-BC9D-1C1CB4E5D2DF}</AdobeCode>
    <AdobeCode>{80FC4840-6688-4C5E-8551-C9A2BD9643F8}</AdobeCode>
    <AdobeCode>{7A47BA79-66C8-45A3-8DBA-278CB0F9EAC2}</AdobeCode>
    <AdobeCode>{DC6F80ED-184A-4FA3-88FE-AFE1C2616128}</AdobeCode>
    <AdobeCode>{80741FC3-0411-4AC2-A4A7-20185882761E}</AdobeCode>
    <AdobeCode>{DBD80AE5-F05F-4D15-A30D-4E4CAC267626}</AdobeCode>
    <AdobeCode>{02C12514-9196-4E0E-B75C-B83CEE1EFB33}</AdobeCode>
    <AdobeCode>{F4BDCA90-1710-4259-9F1D-CE7A375EB0C2}</AdobeCode>
    <AdobeCode>{598A9D65-3818-4D95-8B0B-24A2358CAD50}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorCommonSetRGB-4.0">
    <DisplayName>AdobeColorCommonSetRGB</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="7972"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>7972</TotalSize>
      <MaxPathComponent>/Color/Profiles/Recommended\sRGB Color Space Profile.icm</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="7972"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">AdobeColorCommonSetRGB</Value>
      <Value lang="be_BY">AdobeColorCommonSetRGB</Value>
      <Value lang="bg_BG">AdobeColorCommonSetRGB</Value>
      <Value lang="ca_ES">AdobeColorCommonSetRGB</Value>
      <Value lang="cs_CZ">AdobeColorCommonSetRGB</Value>
      <Value lang="da_DK">AdobeColorCommonSetRGB</Value>
      <Value lang="de_DE">AdobeColorCommonSetRGB</Value>
      <Value lang="el_GR">AdobeColorCommonSetRGB</Value>
      <Value lang="en_GB">AdobeColorCommonSetRGB</Value>
      <Value lang="en_MX">AdobeColorCommonSetRGB</Value>
      <Value lang="en_US">AdobeColorCommonSetRGB</Value>
      <Value lang="en_XC">AdobeColorCommonSetRGB</Value>
      <Value lang="en_XM">AdobeColorCommonSetRGB</Value>
      <Value lang="es_ES">AdobeColorCommonSetRGB</Value>
      <Value lang="es_MX">AdobeColorCommonSetRGB</Value>
      <Value lang="es_QM">AdobeColorCommonSetRGB</Value>
      <Value lang="et_EE">AdobeColorCommonSetRGB</Value>
      <Value lang="fi_FI">AdobeColorCommonSetRGB</Value>
      <Value lang="fr_CA">AdobeColorCommonSetRGB</Value>
      <Value lang="fr_FR">AdobeColorCommonSetRGB</Value>
      <Value lang="fr_MX">AdobeColorCommonSetRGB</Value>
      <Value lang="fr_XM">AdobeColorCommonSetRGB</Value>
      <Value lang="he_IL">AdobeColorCommonSetRGB</Value>
      <Value lang="hi_IN">AdobeColorCommonSetRGB</Value>
      <Value lang="hr_HR">AdobeColorCommonSetRGB</Value>
      <Value lang="hu_HU">AdobeColorCommonSetRGB</Value>
      <Value lang="is_IS">AdobeColorCommonSetRGB</Value>
      <Value lang="it_IT">AdobeColorCommonSetRGB</Value>
      <Value lang="ja_JP">AdobeColorCommonSetRGB</Value>
      <Value lang="ko_KR">AdobeColorCommonSetRGB</Value>
      <Value lang="lt_LT">AdobeColorCommonSetRGB</Value>
      <Value lang="lv_LV">AdobeColorCommonSetRGB</Value>
      <Value lang="mk_MK">AdobeColorCommonSetRGB</Value>
      <Value lang="nb_NO">AdobeColorCommonSetRGB</Value>
      <Value lang="nl_NL">AdobeColorCommonSetRGB</Value>
      <Value lang="nn_NO">AdobeColorCommonSetRGB</Value>
      <Value lang="no_NO">AdobeColorCommonSetRGB</Value>
      <Value lang="pl_PL">AdobeColorCommonSetRGB</Value>
      <Value lang="pt_BR">AdobeColorCommonSetRGB</Value>
      <Value lang="ro_RO">AdobeColorCommonSetRGB</Value>
      <Value lang="ru_RU">AdobeColorCommonSetRGB</Value>
      <Value lang="sh_YU">AdobeColorCommonSetRGB</Value>
      <Value lang="sk_SK">AdobeColorCommonSetRGB</Value>
      <Value lang="sl_SI">AdobeColorCommonSetRGB</Value>
      <Value lang="sq_AL">AdobeColorCommonSetRGB</Value>
      <Value lang="sv_SE">AdobeColorCommonSetRGB</Value>
      <Value lang="th_TH">AdobeColorCommonSetRGB</Value>
      <Value lang="tr_TR">AdobeColorCommonSetRGB</Value>
      <Value lang="uk_UA">AdobeColorCommonSetRGB</Value>
      <Value lang="vi_VN">AdobeColorCommonSetRGB</Value>
      <Value lang="zh_CN">AdobeColorCommonSetRGB</Value>
      <Value lang="zh_TW">AdobeColorCommonSetRGB</Value>
      <Value lang="en_AE">AdobeColorCommonSetRGB</Value>
      <Value lang="en_IL">AdobeColorCommonSetRGB</Value>
      <Value lang="fr_MA">AdobeColorCommonSetRGB</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0", "ChannelID", "AdobeColorCommonSetRGB-4.0")
INSERT INTO PayloadData VALUES("{C7B1C1B3-368D-4C32-A818-83F1554EB398}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorCommonSetRGB-4.0">
    <DisplayName>AdobeColorCommonSetRGB</DisplayName>
  </Channel>')
