CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}"/>')
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0" , "ValidationSig", "GvwhLej8Wshx6Vw24QUoqtDM0YYzB1W+hDkcgFYumNluYXS6LsrLHMtBKSNEqxKWnD0HAtcHNe+3JsKyK322OAgqx/4swORH6vgz0FBge887pwJlmzaq/2RQdXxUn7weaPpPjDyr4Bbess7SPzCmniKfBdcwuyE7Y7iQpKxHf3W6lJWUmF1dYzSXSmVaZqdabYMJv1wJDsItkiNH0MmbBJsXc27KUAwj/QRu2MRUnR6DYWFr3oJkO5EmESAMXWKWVn3ndEtRnppKype4RI2+FnkH2G2VN57oFoDHVOnKvs8e0t7sT/oFDCpImXAQMSpb3wGdaw3X4X4jlaNJbtFu9ivM5rn9ip4qkLjNxolw1ffwt/AcJH/dyK9PV6Z8qkP6Y9yScFh3Zlt7r9jx/9YlpQYHQkXWRYuDu70/zqkHMvBjtmIoWCTPgC08EuIz+37OMBNBXjrPE+H7kuNtaP98WR2fpoPiLysRnNM9DOgWrgcBu2Sj+ImoHCOsr5imjWBr")
INSERT INTO Payloads VALUES	("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "NonAdobePayload", "PDF Settings CS6", "11.0", "normal")
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-11-24 18:45:13.704000</Property>
    <Property name="TargetName">AdobePDFSettings11-ja_JP</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}</Property>
    <Property name="ProductName">PDF Settings CS6</Property>
    <Property name="ProductVersion">11.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="0" name="Default" folderName="">[Common]\Adobe\Adobe PDF</Platform>
    <Platform isFixed="0" name="OSX" folderName="">[Common]/Adobe/Adobe PDF</Platform>
    <Platform isFixed="0" name="Win32" folderName="">[SharedApplicationData]/Adobe/Adobe PDF</Platform>
  </InstallDir><Languages>
    <Language>ja_JP</Language>
    <Language>ko_KR</Language>
    <Language>zh_CN</Language>
    <Language>zh_TW</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>NonAdobePayload</Family>
		<ProductName>PDF Settings CS6</ProductName>
	<ProductVersion>11.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Channel enable="1" id="PDFSettingsCS6-11.0">
    <DisplayName>PDF Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2257920">
    <Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>2257920</TotalSize>
      <MaxPathComponent>AdobePDFSettings11-ja_JP.msi</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2257920"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ja_JP">11.0</Value>
      <Value lang="ko_KR">11.0</Value>
      <Value lang="zh_CN">11.0</Value>
      <Value lang="zh_TW">11.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ja_JP">PDF Settings CS6</Value>
      <Value lang="ko_KR">PDF Settings CS6</Value>
      <Value lang="zh_CN">PDF Settings CS6</Value>
      <Value lang="zh_TW">PDF Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences><ThirdPartyComponent modifierID="{BDD28224-5908-4C35-A8DF-0379324AF58B}">
							<Metadata>
								<Type>msiPackage</Type>
								<Name>AdobePDFSettings11-ja_JP.msi</Name>
								<Properties>
									<Property name="ProductCode">{ECE66592-E920-4C43-9D1C-9BC12483EC42}</Property>
								</Properties>
                
							</Metadata>
							<Arguments prefix="" delimiter="=">
								<Install>
                  <Argument name="ADOBE_SETUP">1</Argument>
                </Install>
								<Repair>
                  <Argument name="ADOBE_SETUP">1</Argument>
                </Repair>
							</Arguments>
							<SuccessCodes>
								<Install>
									<Range lowerBound="0" upperBound="0"/>
								</Install>
							</SuccessCodes>
							<Capabilities>
								<Install stringID="abc">
									<Value lang="en_US">You just installed Adobe PDF Settings CS6</Value>
								<Value lang="ja_JP">You just installed Adobe PDF Settings CS6 {ja_JP} </Value>
        <Value lang="ko_KR">You just installed Adobe PDF Settings CS6 {ko_KR} </Value>
        <Value lang="zh_CN">You just installed Adobe PDF Settings CS6 {zh_CN} </Value>
        <Value lang="zh_TW">You just installed Adobe PDF Settings CS6 {zh_TW} </Value>
      </Install>
								<Repair isRepairable="1" stringID="pqr">
									<Value lang="en_US">Adobe PDF Settings CS6 was reinstalled</Value>
								<Value lang="ja_JP">Adobe PDF Settings CS6 was reinstalled {ja_JP} </Value>
        <Value lang="ko_KR">Adobe PDF Settings CS6 was reinstalled {ko_KR} </Value>
        <Value lang="zh_CN">Adobe PDF Settings CS6 was reinstalled {zh_CN} </Value>
        <Value lang="zh_TW">Adobe PDF Settings CS6 was reinstalled {zh_TW} </Value>
      </Repair>
								<Uninstall stringID="xyz" isUninstallable="1">
									<Value lang="en_US">Adobe PDF Settings CS6 was uninstalled</Value>
								<Value lang="ja_JP">Adobe PDF Settings CS6 was uninstalled {ja_JP} </Value>
        <Value lang="ko_KR">Adobe PDF Settings CS6 was uninstalled {ko_KR} </Value>
        <Value lang="zh_CN">Adobe PDF Settings CS6 was uninstalled {zh_CN} </Value>
        <Value lang="zh_TW">Adobe PDF Settings CS6 was uninstalled {zh_TW} </Value>
      </Uninstall>
							</Capabilities>
						</ThirdPartyComponent></PayloadInfo>')
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0", "ChannelID", "PDFSettingsCS6-11.0")
INSERT INTO PayloadData VALUES("{5C0D1B2E-2D70-48AB-9A0B-0E1E1CC084B1}", "0", "ChannelInfo", '<Channel enable="1" id="PDFSettingsCS6-11.0">
    <DisplayName>PDF Settings CS6</DisplayName>
  </Channel>')
