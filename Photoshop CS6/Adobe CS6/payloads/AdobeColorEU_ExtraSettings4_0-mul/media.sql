CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{F52F93DC-70C5-4AB9-A9D9-A03D311124DD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{E2AA7A09-F140-4ADE-84A2-609A72654C39}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{488C41FB-86EC-4140-AAA4-B99A97841F18}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{10432CF2-773A-468B-ABCF-BF628FBE1787}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{202983ED-5968-4375-AA1C-268E32B5BD5E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{9C109A13-4DB0-4807-B118-2C78B90EE106}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{C0BE6E49-2171-4114-B634-661A9EAAAE29}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{F9446E9E-6110-4A32-B3CA-98B5D9D85D77}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{4E32F1FA-C404-4849-A4DF-7E18EA16A5C4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "{C9FE8E8E-6EC8-4D49-ACF2-9CAA872A461C}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{51C77DC1-5C75-4491-8645-A17CC33F5A36}"/>')
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0" , "ValidationSig", "H+Sr8SLReoVWX4vhR1uLbipLMYV6V6saBZ3uDIFek1TyWWtwSHRWNuJ9G1iymVShid97hKLWo1c9biTkruqqZa8oa700zqJHJhTCWHQ+FuHCtkgdZ/HyH1h1uqzNMU9du2sL3WDfcuy6UWi5i8u0klJJ98s7U1n+bBHcfXuvSZzjiZ6wrhwMKTc0hWAJZmGQvMUChmDZ2Mn68F3j+Qlhx+tz77hS23F5l681nGVt1byC9c4DPdfabtuPd+gTTJa1pYFrPmaN7szso5Ub8tap0n2YsoigmOVT5kZN3Pd9btofBiC5XdCsET8lDt5oFnPjXyJ0nvzYBZjwucMe3dT6XkOEWesFBeL9GusCDvJ/+bc3UzNx+8jAoMzfV+1R0LYhIWHlWRwFdxeseWkaVCnnipVpJYEGpWTucaQcpnyebJevb23HyBtj0ODvyXN+d2vnglIIeh0M4eybFLKsHeKeetrcQzXDm5qjENMHzm2zdojFLb3ytJr7wzHBqeb8IQ8r")
INSERT INTO Payloads VALUES	("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "CoreTech", "AdobeColorEU CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-09 00:21:12.007000</Property>
    <Property name="TargetName">AdobeColorEU_ExtraSettings4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{51C77DC1-5C75-4491-8645-A17CC33F5A36}</Property>
    <Property name="ProductName">Adobe Color EU Extra Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>ja_JP</Language>
    <Language>ko_KR</Language>
    <Language>zh_CN</Language>
    <Language>zh_TW</Language>
    <Language>fr_CA</Language>
    <Language>en_US</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorEU CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{F52F93DC-70C5-4AB9-A9D9-A03D311124DD}</AdobeCode>
    <AdobeCode>{E2AA7A09-F140-4ADE-84A2-609A72654C39}</AdobeCode>
    <AdobeCode>{488C41FB-86EC-4140-AAA4-B99A97841F18}</AdobeCode>
    <AdobeCode>{10432CF2-773A-468B-ABCF-BF628FBE1787}</AdobeCode>
    <AdobeCode>{202983ED-5968-4375-AA1C-268E32B5BD5E}</AdobeCode>
    <AdobeCode>{9C109A13-4DB0-4807-B118-2C78B90EE106}</AdobeCode>
    <AdobeCode>{C0BE6E49-2171-4114-B634-661A9EAAAE29}</AdobeCode>
    <AdobeCode>{F9446E9E-6110-4A32-B3CA-98B5D9D85D77}</AdobeCode>
    <AdobeCode>{4E32F1FA-C404-4849-A4DF-7E18EA16A5C4}</AdobeCode>
    <AdobeCode>{C9FE8E8E-6EC8-4D49-ACF2-9CAA872A461C}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorEUExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color EU Extra Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2023544"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2023544</TotalSize>
      <MaxPathComponent>/Color/Settings/ExtraSettings\Europe General Purpose 3.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2023544"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="en_US">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ja_JP">Adobe Color EU Extra Settings CS6</Value>
      <Value lang="ko_KR">Adobe Color EU Extra Settings CS6</Value>
      <Value lang="zh_CN">Adobe Color EU Extra Settings CS6</Value>
      <Value lang="zh_TW">Adobe Color EU Extra Settings CS6</Value>
      <Value lang="fr_CA">Adobe Color EU Extra Settings CS6</Value>
      <Value lang="en_US">Adobe Color EU Extra Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0", "ChannelID", "AdobeColorEUExtraSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{51C77DC1-5C75-4491-8645-A17CC33F5A36}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorEUExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color EU Extra Settings CS6</DisplayName>
  </Channel>')
