CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{11C0657F-2BD4-4CBC-87F3-9455DC91886E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{A71720BB-9352-4EEA-8EFD-D527BC52CD94}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{48ECEFF8-F9DA-42BD-B903-87DB0A962C19}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{768CCA13-5BD4-4F36-9AB4-4E769BB79A90}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{304D0F07-7581-4171-AF73-9344D504FB1A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{924698BF-AE78-4179-9BE0-A80632348792}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{3FAA040C-4D26-4D9D-9868-0BBE14842726}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{D5C65301-A798-4B5F-AA27-21ADC0A93C2E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{907B69FE-596B-49B2-9BC0-D285B4898A63}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{0C296FB8-E438-4206-BD14-848A92166B11}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{C8378250-34C7-4CF6-B97B-5E39295DBE3B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{FB9C5685-3ED9-4941-BDAF-AD1F0E37B381}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{C69EA06F-A746-488E-8A95-10D6B6DE9F5A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{B32F08BC-2EEA-4AE0-9B83-F0822702D4FF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{C7856AE7-3296-4854-B651-85B8785EB33E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "{24105411-30E1-4230-8044-7D2513B74EB6}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{8085C16A-3148-40B4-BC8B-12ED59C9C478}"/>')
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0" , "ValidationSig", "YJdiX9HTDgn3V0QEvKAO5SxfQzRr3xMz/N7OtwenW/ngkErbENQCoihyrtCiwlTQCy3Y3Vl1E4AQ7ObZVFx2URM24DLRoGzm64g9Vcv0Nqcd5Bnd2M6C1AQaf1AIWRvt836tMDPi/4E6vi4AE9xbDbsH36IwSHFVK2TuatXVoJ7hOC7htKJecWwGJPpZrWHmpcF4lkqzwWU5/5D47kP6vMWBieE7lSQbsg+2sGr7uL1FQV6oOmk5spZ9e2jr0kZpfHXrcXvHR+m27YkOJdQcadh9qKw4p0zZw9o1kBJmOFOFv7aT9YjDklD08CWAvT9FAloR7GqlKIwX1piYzE+7U73FyLOAFwuixGi1P6AzRCxxX37eByMKu4o+P8ZYFH5D79Ao+rBYF7XarQzRp46R/A82ZFuNgJXBf8q8PT2Bh16Lf5WZa0hwiZbpVfjRcY1t3sGcMvoF076gkN5/xJOQrb7QtRRtcCO/gRwCUBWw/xV4sKzP+cZZT0xkEubGgu4t")
INSERT INTO Branding VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5UeXBlU3Vw
cG9ydC1JbnN0YWxsZXItQWRvYmVDb2RlPC9EYXRhPg0KICAgIDwvUGF5bG9hZD4NCjwvQ29uZmln
dXJhdGlvbj4NCg==')
INSERT INTO Payloads VALUES	("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "Type Group", "Recommended Common Fonts Installation x64", "2.0", "normal")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-26 16:39:47.447000</Property>
    <Property name="TargetName">AdobeFontsRecommended_x64-mul</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{8085C16A-3148-40B4-BC8B-12ED59C9C478}</Property>
    <Property name="ProductName">Adobe Fonts Recommended x64</Property>
    <Property name="ProductVersion">2.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[FontsFolder]</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>Type Group</Family>
		<ProductName>Recommended Common Fonts Installation x64</ProductName>
	<ProductVersion>2.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{11C0657F-2BD4-4CBC-87F3-9455DC91886E}</AdobeCode>
    <AdobeCode>{A71720BB-9352-4EEA-8EFD-D527BC52CD94}</AdobeCode>
    <AdobeCode>{48ECEFF8-F9DA-42BD-B903-87DB0A962C19}</AdobeCode>
    <AdobeCode>{768CCA13-5BD4-4F36-9AB4-4E769BB79A90}</AdobeCode>
    <AdobeCode>{304D0F07-7581-4171-AF73-9344D504FB1A}</AdobeCode>
    <AdobeCode>{924698BF-AE78-4179-9BE0-A80632348792}</AdobeCode>
    <AdobeCode>{3FAA040C-4D26-4D9D-9868-0BBE14842726}</AdobeCode>
    <AdobeCode>{D5C65301-A798-4B5F-AA27-21ADC0A93C2E}</AdobeCode>
    <AdobeCode>{907B69FE-596B-49B2-9BC0-D285B4898A63}</AdobeCode>
    <AdobeCode>{0C296FB8-E438-4206-BD14-848A92166B11}</AdobeCode>
    <AdobeCode>{C8378250-34C7-4CF6-B97B-5E39295DBE3B}</AdobeCode>
    <AdobeCode>{FB9C5685-3ED9-4941-BDAF-AD1F0E37B381}</AdobeCode>
    <AdobeCode>{C69EA06F-A746-488E-8A95-10D6B6DE9F5A}</AdobeCode>
    <AdobeCode>{B32F08BC-2EEA-4AE0-9B83-F0822702D4FF}</AdobeCode>
    <AdobeCode>{C7856AE7-3296-4854-B651-85B8785EB33E}</AdobeCode>
    <AdobeCode>{24105411-30E1-4230-8044-7D2513B74EB6}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeFontsRecommendedx64-2.0">
    <DisplayName>Adobe Fonts Recommended x64</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\FontsRecommended\2.0\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="104486890"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>238</TotalSize>
      <MaxPathComponent>/FontsRecommended/2.0/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[FontsFolder]</Root>
      <TotalSize>104486652</TotalSize>
      <MaxPathComponent>ACaslonPro-SemiboldItalic.otf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="104486890"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">2.0</Value>
      <Value lang="be_BY">2.0</Value>
      <Value lang="bg_BG">2.0</Value>
      <Value lang="ca_ES">2.0</Value>
      <Value lang="cs_CZ">2.0</Value>
      <Value lang="da_DK">2.0</Value>
      <Value lang="de_DE">2.0</Value>
      <Value lang="el_GR">2.0</Value>
      <Value lang="en_GB">2.0</Value>
      <Value lang="en_MX">2.0</Value>
      <Value lang="en_US">2.0</Value>
      <Value lang="en_XC">2.0</Value>
      <Value lang="en_XM">2.0</Value>
      <Value lang="es_ES">2.0</Value>
      <Value lang="es_MX">2.0</Value>
      <Value lang="es_QM">2.0</Value>
      <Value lang="et_EE">2.0</Value>
      <Value lang="fi_FI">2.0</Value>
      <Value lang="fr_CA">2.0</Value>
      <Value lang="fr_FR">2.0</Value>
      <Value lang="fr_MX">2.0</Value>
      <Value lang="fr_XM">2.0</Value>
      <Value lang="he_IL">2.0</Value>
      <Value lang="hi_IN">2.0</Value>
      <Value lang="hr_HR">2.0</Value>
      <Value lang="hu_HU">2.0</Value>
      <Value lang="is_IS">2.0</Value>
      <Value lang="it_IT">2.0</Value>
      <Value lang="ja_JP">2.0</Value>
      <Value lang="ko_KR">2.0</Value>
      <Value lang="lt_LT">2.0</Value>
      <Value lang="lv_LV">2.0</Value>
      <Value lang="mk_MK">2.0</Value>
      <Value lang="nb_NO">2.0</Value>
      <Value lang="nl_NL">2.0</Value>
      <Value lang="nn_NO">2.0</Value>
      <Value lang="no_NO">2.0</Value>
      <Value lang="pl_PL">2.0</Value>
      <Value lang="pt_BR">2.0</Value>
      <Value lang="ro_RO">2.0</Value>
      <Value lang="ru_RU">2.0</Value>
      <Value lang="sh_YU">2.0</Value>
      <Value lang="sk_SK">2.0</Value>
      <Value lang="sl_SI">2.0</Value>
      <Value lang="sq_AL">2.0</Value>
      <Value lang="sv_SE">2.0</Value>
      <Value lang="th_TH">2.0</Value>
      <Value lang="tr_TR">2.0</Value>
      <Value lang="uk_UA">2.0</Value>
      <Value lang="vi_VN">2.0</Value>
      <Value lang="zh_CN">2.0</Value>
      <Value lang="zh_TW">2.0</Value>
      <Value lang="en_AE">2.0</Value>
      <Value lang="en_IL">2.0</Value>
      <Value lang="fr_MA">2.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Fonts Recommended x64</Value>
      <Value lang="be_BY">Adobe Fonts Recommended x64</Value>
      <Value lang="bg_BG">Adobe Fonts Recommended x64</Value>
      <Value lang="ca_ES">Adobe Fonts Recommended x64</Value>
      <Value lang="cs_CZ">Adobe Fonts Recommended x64</Value>
      <Value lang="da_DK">Adobe Fonts Recommended x64</Value>
      <Value lang="de_DE">Adobe Fonts Recommended x64</Value>
      <Value lang="el_GR">Adobe Fonts Recommended x64</Value>
      <Value lang="en_GB">Adobe Fonts Recommended x64</Value>
      <Value lang="en_MX">Adobe Fonts Recommended x64</Value>
      <Value lang="en_US">Adobe Fonts Recommended x64</Value>
      <Value lang="en_XC">Adobe Fonts Recommended x64</Value>
      <Value lang="en_XM">Adobe Fonts Recommended x64</Value>
      <Value lang="es_ES">Adobe Fonts Recommended x64</Value>
      <Value lang="es_MX">Adobe Fonts Recommended x64</Value>
      <Value lang="es_QM">Adobe Fonts Recommended x64</Value>
      <Value lang="et_EE">Adobe Fonts Recommended x64</Value>
      <Value lang="fi_FI">Adobe Fonts Recommended x64</Value>
      <Value lang="fr_CA">Adobe Fonts Recommended x64</Value>
      <Value lang="fr_FR">Adobe Fonts Recommended x64</Value>
      <Value lang="fr_MX">Adobe Fonts Recommended x64</Value>
      <Value lang="fr_XM">Adobe Fonts Recommended x64</Value>
      <Value lang="he_IL">Adobe Fonts Recommended x64</Value>
      <Value lang="hi_IN">Adobe Fonts Recommended x64</Value>
      <Value lang="hr_HR">Adobe Fonts Recommended x64</Value>
      <Value lang="hu_HU">Adobe Fonts Recommended x64</Value>
      <Value lang="is_IS">Adobe Fonts Recommended x64</Value>
      <Value lang="it_IT">Adobe Fonts Recommended x64</Value>
      <Value lang="ja_JP">Adobe Fonts Recommended x64</Value>
      <Value lang="ko_KR">Adobe Fonts Recommended x64</Value>
      <Value lang="lt_LT">Adobe Fonts Recommended x64</Value>
      <Value lang="lv_LV">Adobe Fonts Recommended x64</Value>
      <Value lang="mk_MK">Adobe Fonts Recommended x64</Value>
      <Value lang="nb_NO">Adobe Fonts Recommended x64</Value>
      <Value lang="nl_NL">Adobe Fonts Recommended x64</Value>
      <Value lang="nn_NO">Adobe Fonts Recommended x64</Value>
      <Value lang="no_NO">Adobe Fonts Recommended x64</Value>
      <Value lang="pl_PL">Adobe Fonts Recommended x64</Value>
      <Value lang="pt_BR">Adobe Fonts Recommended x64</Value>
      <Value lang="ro_RO">Adobe Fonts Recommended x64</Value>
      <Value lang="ru_RU">Adobe Fonts Recommended x64</Value>
      <Value lang="sh_YU">Adobe Fonts Recommended x64</Value>
      <Value lang="sk_SK">Adobe Fonts Recommended x64</Value>
      <Value lang="sl_SI">Adobe Fonts Recommended x64</Value>
      <Value lang="sq_AL">Adobe Fonts Recommended x64</Value>
      <Value lang="sv_SE">Adobe Fonts Recommended x64</Value>
      <Value lang="th_TH">Adobe Fonts Recommended x64</Value>
      <Value lang="tr_TR">Adobe Fonts Recommended x64</Value>
      <Value lang="uk_UA">Adobe Fonts Recommended x64</Value>
      <Value lang="vi_VN">Adobe Fonts Recommended x64</Value>
      <Value lang="zh_CN">Adobe Fonts Recommended x64</Value>
      <Value lang="zh_TW">Adobe Fonts Recommended x64</Value>
      <Value lang="en_AE">Adobe Fonts Recommended x64</Value>
      <Value lang="en_IL">Adobe Fonts Recommended x64</Value>
      <Value lang="fr_MA">Adobe Fonts Recommended x64</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0", "AMTConfigPath", "[AdobeCommon]\FontsRecommended\2.0\AMT\component.xml")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0", "ChannelID", "AdobeFontsRecommendedx64-2.0")
INSERT INTO PayloadData VALUES("{8085C16A-3148-40B4-BC8B-12ED59C9C478}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeFontsRecommendedx64-2.0">
    <DisplayName>Adobe Fonts Recommended x64</DisplayName>
  </Channel>')
