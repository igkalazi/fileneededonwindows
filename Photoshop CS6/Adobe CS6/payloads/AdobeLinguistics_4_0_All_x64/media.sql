CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "", "required", "NonAdobePayload", "Microsoft Visual C++ 2008 Redistributable Package (x64)", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{759A3B3E-72E1-4180-AA23-EFEDC96FEC50}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{4DAE6609-46FE-4e07-BF82-086F4BFA49EE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{0C3AEF62-0DA9-41CA-B745-F4D1D0C5A333}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{F9C2612B-2AC4-41FB-9043-EE9568D0C47F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{EA4F8D87-30E8-4E64-AE63-4D7FAC40B2B5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{95C58E48-8350-4A49-828A-668A651D1BFA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{4A586C17-7B36-4919-8F79-D7D69881112D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{BAD9D75E-3ACD-47BA-A96E-C2ED725E36DE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{D50F622C-8CF9-4A62-B00B-1129ABBDF3F3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{2204AA00-FD33-4BBD-A2EF-2779394E0901}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{EDD47993-4036-4AF9-899C-A77A42F8B51B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{DE235A9D-5AD1-4113-A6BE-51CE2B5D89B8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{0F357EF1-F481-4909-A34C-24678545E8E4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{8C162AB5-9ED3-48B1-9BB9-B085DADA6765}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "{1B5CE3E1-466D-40AE-A0CA-326DF91E5450}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{C41A769E-27ED-44F7-8A11-F2E32F538E05}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\UserDictionaryMigrationTool\UDMT.exe</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\UserDictionaryMigrationTool\AdobeLinguistic.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0" , "ValidationSig", "IG+Rcppg4ipDKwTukKgCx0XW8W8Sd8oBBx30m9/WZDdsD95YDuOwRNHf8SBwOIGZuMpLiHfr0icmba9wBIM/awSlJwefjIqc4nyPjzu0hcOc8nOKEXy9MwVsNYRdggacYLRXKDs32gIO3wzPo3tf9UurVoWe2MsdskexDTiEJ6LyqS3gOlOQpIO5GukDK+fNnKczBkk+ZCySXxXLCRfkuEOvf4eTkVDzc4o6R6I6EVXv0O0NpWuWlFXbYfdh/d9lYfTnqrRCmcM+eTaaA+vOAjwBVLbb441J7zEVsoKCPQl2gNY5hVAUwrudkyndJhiHdtiFK7/8WnrEl9yzR8Q3LD7KH7Ihi1zRtVedaqHXEhojFSqeMkcPI/DY7VWyq/s+2mfpac2cB8YpHzN0aJ08yicvHZO+xzKQTQMd38HbmD8jT/vNjG8SGr6HljM4qy4jwnFH1yomCsKKVB3SnFovpWmpb7UK52oUb06TMmQprKtVw2X++vvJRQqc0ubJdfMD")
INSERT INTO Branding VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5MaW5ndWlz
dGljcy1JbnN0YWxsZXItQWRvYmVDb2RlPC9EYXRhPg0KICAgIDwvUGF5bG9hZD4NCjwvQ29uZmln
dXJhdGlvbj4=')
INSERT INTO Payloads VALUES	("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "CoreTech", "Adobe Linguistics CS6 x64", "6.0.0", "normal")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-12-19 16:37:10.224000</Property>
    <Property name="TargetName">AdobeLinguistics_4_0_All_x64</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{C41A769E-27ED-44F7-8A11-F2E32F538E05}</Property>
    <Property name="ProductName">Adobe Linguistics CS6 x64</Property>
    <Property name="ProductVersion">6.0.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Linguistics CS6 x64</ProductName>
	<ProductVersion>6.0.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Dependencies>
    <ProductInfo type="required">
      <Family>NonAdobePayload</Family>
      <ProductName>Microsoft Visual C++ 2008 Redistributable Package (x64)</ProductName>
    </ProductInfo>
  </Dependencies><Upgrades>
    <AdobeCode>{759A3B3E-72E1-4180-AA23-EFEDC96FEC50}</AdobeCode>
    <AdobeCode>{4DAE6609-46FE-4e07-BF82-086F4BFA49EE}</AdobeCode>
    <AdobeCode>{0C3AEF62-0DA9-41CA-B745-F4D1D0C5A333}</AdobeCode>
    <AdobeCode>{F9C2612B-2AC4-41FB-9043-EE9568D0C47F}</AdobeCode>
    <AdobeCode>{EA4F8D87-30E8-4E64-AE63-4D7FAC40B2B5}</AdobeCode>
    <AdobeCode>{95C58E48-8350-4A49-828A-668A651D1BFA}</AdobeCode>
    <AdobeCode>{4A586C17-7B36-4919-8F79-D7D69881112D}</AdobeCode>
    <AdobeCode>{BAD9D75E-3ACD-47BA-A96E-C2ED725E36DE}</AdobeCode>
    <AdobeCode>{D50F622C-8CF9-4A62-B00B-1129ABBDF3F3}</AdobeCode>
    <AdobeCode>{2204AA00-FD33-4BBD-A2EF-2779394E0901}</AdobeCode>
    <AdobeCode>{EDD47993-4036-4AF9-899C-A77A42F8B51B}</AdobeCode>
    <AdobeCode>{DE235A9D-5AD1-4113-A6BE-51CE2B5D89B8}</AdobeCode>
    <AdobeCode>{0F357EF1-F481-4909-A34C-24678545E8E4}</AdobeCode>
    <AdobeCode>{8C162AB5-9ED3-48B1-9BB9-B085DADA6765}</AdobeCode>
    <AdobeCode>{1B5CE3E1-466D-40AE-A0CA-326DF91E5450}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeLinguisticsCS6x64-6.0.0">
    <DisplayName>Adobe Linguistics CS6 x64</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\Linguistics\6.0\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="43963404"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>43963404</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Proximity/11.00/NC-L3S4R8PNTX\grm190120050401phon.env</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="43963404"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">6.0.0</Value>
      <Value lang="be_BY">6.0.0</Value>
      <Value lang="bg_BG">6.0.0</Value>
      <Value lang="ca_ES">6.0.0</Value>
      <Value lang="cs_CZ">6.0.0</Value>
      <Value lang="da_DK">6.0.0</Value>
      <Value lang="de_DE">6.0.0</Value>
      <Value lang="el_GR">6.0.0</Value>
      <Value lang="en_GB">6.0.0</Value>
      <Value lang="en_MX">6.0.0</Value>
      <Value lang="en_US">6.0.0</Value>
      <Value lang="en_XC">6.0.0</Value>
      <Value lang="en_XM">6.0.0</Value>
      <Value lang="es_ES">6.0.0</Value>
      <Value lang="es_MX">6.0.0</Value>
      <Value lang="es_QM">6.0.0</Value>
      <Value lang="et_EE">6.0.0</Value>
      <Value lang="fi_FI">6.0.0</Value>
      <Value lang="fr_CA">6.0.0</Value>
      <Value lang="fr_FR">6.0.0</Value>
      <Value lang="fr_MX">6.0.0</Value>
      <Value lang="fr_XM">6.0.0</Value>
      <Value lang="he_IL">6.0.0</Value>
      <Value lang="hi_IN">6.0.0</Value>
      <Value lang="hr_HR">6.0.0</Value>
      <Value lang="hu_HU">6.0.0</Value>
      <Value lang="is_IS">6.0.0</Value>
      <Value lang="it_IT">6.0.0</Value>
      <Value lang="ja_JP">6.0.0</Value>
      <Value lang="ko_KR">6.0.0</Value>
      <Value lang="lt_LT">6.0.0</Value>
      <Value lang="lv_LV">6.0.0</Value>
      <Value lang="mk_MK">6.0.0</Value>
      <Value lang="nb_NO">6.0.0</Value>
      <Value lang="nl_NL">6.0.0</Value>
      <Value lang="nn_NO">6.0.0</Value>
      <Value lang="no_NO">6.0.0</Value>
      <Value lang="pl_PL">6.0.0</Value>
      <Value lang="pt_BR">6.0.0</Value>
      <Value lang="ro_RO">6.0.0</Value>
      <Value lang="ru_RU">6.0.0</Value>
      <Value lang="sh_YU">6.0.0</Value>
      <Value lang="sk_SK">6.0.0</Value>
      <Value lang="sl_SI">6.0.0</Value>
      <Value lang="sq_AL">6.0.0</Value>
      <Value lang="sv_SE">6.0.0</Value>
      <Value lang="th_TH">6.0.0</Value>
      <Value lang="tr_TR">6.0.0</Value>
      <Value lang="uk_UA">6.0.0</Value>
      <Value lang="vi_VN">6.0.0</Value>
      <Value lang="zh_CN">6.0.0</Value>
      <Value lang="zh_TW">6.0.0</Value>
      <Value lang="en_AE">6.0.0</Value>
      <Value lang="en_IL">6.0.0</Value>
      <Value lang="fr_MA">6.0.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Linguistics CS6 x64</Value>
      <Value lang="be_BY">Adobe Linguistics CS6 x64</Value>
      <Value lang="bg_BG">Adobe Linguistics CS6 x64</Value>
      <Value lang="ca_ES">Adobe Linguistics CS6 x64</Value>
      <Value lang="cs_CZ">Adobe Linguistics CS6 x64</Value>
      <Value lang="da_DK">Adobe Linguistics CS6 x64</Value>
      <Value lang="de_DE">Adobe Linguistics CS6 x64</Value>
      <Value lang="el_GR">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_GB">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_MX">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_US">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_XC">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_XM">Adobe Linguistics CS6 x64</Value>
      <Value lang="es_ES">Adobe Linguistics CS6 x64</Value>
      <Value lang="es_MX">Adobe Linguistics CS6 x64</Value>
      <Value lang="es_QM">Adobe Linguistics CS6 x64</Value>
      <Value lang="et_EE">Adobe Linguistics CS6 x64</Value>
      <Value lang="fi_FI">Adobe Linguistics CS6 x64</Value>
      <Value lang="fr_CA">Adobe Linguistics CS6 x64</Value>
      <Value lang="fr_FR">Adobe Linguistics CS6 x64</Value>
      <Value lang="fr_MX">Adobe Linguistics CS6 x64</Value>
      <Value lang="fr_XM">Adobe Linguistics CS6 x64</Value>
      <Value lang="he_IL">Adobe Linguistics CS6 x64</Value>
      <Value lang="hi_IN">Adobe Linguistics CS6 x64</Value>
      <Value lang="hr_HR">Adobe Linguistics CS6 x64</Value>
      <Value lang="hu_HU">Adobe Linguistics CS6 x64</Value>
      <Value lang="is_IS">Adobe Linguistics CS6 x64</Value>
      <Value lang="it_IT">Adobe Linguistics CS6 x64</Value>
      <Value lang="ja_JP">Adobe Linguistics CS6 x64</Value>
      <Value lang="ko_KR">Adobe Linguistics CS6 x64</Value>
      <Value lang="lt_LT">Adobe Linguistics CS6 x64</Value>
      <Value lang="lv_LV">Adobe Linguistics CS6 x64</Value>
      <Value lang="mk_MK">Adobe Linguistics CS6 x64</Value>
      <Value lang="nb_NO">Adobe Linguistics CS6 x64</Value>
      <Value lang="nl_NL">Adobe Linguistics CS6 x64</Value>
      <Value lang="nn_NO">Adobe Linguistics CS6 x64</Value>
      <Value lang="no_NO">Adobe Linguistics CS6 x64</Value>
      <Value lang="pl_PL">Adobe Linguistics CS6 x64</Value>
      <Value lang="pt_BR">Adobe Linguistics CS6 x64</Value>
      <Value lang="ro_RO">Adobe Linguistics CS6 x64</Value>
      <Value lang="ru_RU">Adobe Linguistics CS6 x64</Value>
      <Value lang="sh_YU">Adobe Linguistics CS6 x64</Value>
      <Value lang="sk_SK">Adobe Linguistics CS6 x64</Value>
      <Value lang="sl_SI">Adobe Linguistics CS6 x64</Value>
      <Value lang="sq_AL">Adobe Linguistics CS6 x64</Value>
      <Value lang="sv_SE">Adobe Linguistics CS6 x64</Value>
      <Value lang="th_TH">Adobe Linguistics CS6 x64</Value>
      <Value lang="tr_TR">Adobe Linguistics CS6 x64</Value>
      <Value lang="uk_UA">Adobe Linguistics CS6 x64</Value>
      <Value lang="vi_VN">Adobe Linguistics CS6 x64</Value>
      <Value lang="zh_CN">Adobe Linguistics CS6 x64</Value>
      <Value lang="zh_TW">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_AE">Adobe Linguistics CS6 x64</Value>
      <Value lang="en_IL">Adobe Linguistics CS6 x64</Value>
      <Value lang="fr_MA">Adobe Linguistics CS6 x64</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0", "AMTConfigPath", "[AdobeCommon]\Linguistics\6.0\AMT\component.xml")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0", "ChannelID", "AdobeLinguisticsCS6x64-6.0.0")
INSERT INTO PayloadData VALUES("{C41A769E-27ED-44F7-8A11-F2E32F538E05}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeLinguisticsCS6x64-6.0.0">
    <DisplayName>Adobe Linguistics CS6 x64</DisplayName>
  </Channel>')
