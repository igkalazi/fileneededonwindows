CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{2DB470E1-4E3D-4F99-BC99-1B96340DBD4E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{D3A52DD1-77BF-4D08-B692-F39820405EA0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{2C3FC99F-CD5D-4E71-B9F9-9A5BD74AB61C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{A91F37D6-78B6-4A6B-BF97-2310DB16A5DC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{906CDCD3-D83A-497C-B7CF-D489E638D35B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{D6E8A942-E06D-40A1-95AF-61F6D5EA1A6E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{8802A675-6E6D-4B1A-9DBC-414D5BFB15E1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{25A3BB29-A84F-4DAC-84CE-69D64EE98ED4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{21C39AA4-2460-42EA-90A4-4725F0AD2023}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{FDBCA6B1-CEB9-4AF3-8CD6-5DC0C7D6FF3E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{38D198F3-DD47-4572-B430-BCE9ECBAE6E9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{137AB11A-2C96-4F91-8C46-D1CFB83F0DE1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{97CBBA6A-B22C-440F-A8E1-2C4064CD1253}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{2E476A3F-3C92-484A-9604-50BDCCCF2A42}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "{239492CE-6CA4-4E48-B56F-40BB369E9F0F}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{99290358-A784-4218-A7BA-954AE5F9207C}"/>')
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0" , "ValidationSig", "FrKJ4LdXDOaibPauEJ1FRqxD+jtGIbobqsJpkqTKv43Z+j5oJjiBTRGPJuhA5kqB+yHeFWuX0Rmf6akQzo08WNzNDawEcEv2aYS9sZEo97vOIA3edfFhMOSqf4sz2Cxvv0FI5aSB238Q2KuusSzYjv8Wnofly3cBKCfs49Ix3qoU22AF6hxpPFEfL6RF0UyceHamwOPjwoQoWeLTOiiO3Zog17+8Aij82LhERXfruwDVkTGNY0iVq2zu8jqXcR/a5PZ8e5ZtLt7c1WFWbhLCr7d1T6JGh2JPq2cSOSZF9WzM7SBDSo9Ng+y3+QKyoDTNmU1qGUkGLvzVT1hunOhQVZlVnTd94HYeuuItWj60/ipqN+62eg5d7sskiqV+25Xh1XMclIY1CBvyYChWwKEzVMYcY5VJtajP5jK/4FZvk0JjLH49Q3iQDG66+xXFTDV6ZzC7IrwumYbDG+Dmw8RpnLX0Z9rMpNq4YSHT5RZYl7H+tvgqsPDS0a/RtpX8JL6l")
INSERT INTO Payloads VALUES	("{99290358-A784-4218-A7BA-954AE5F9207C}", "CoreTech", "AdobeCMaps x64 CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-19 13:08:28.876000</Property>
    <Property name="TargetName">AdobeCMaps4_x64-mul</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{99290358-A784-4218-A7BA-954AE5F9207C}</Property>
    <Property name="ProductName">Adobe CMaps x64 CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\TypeSupport\CS6\CMaps</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeCMaps x64 CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{2DB470E1-4E3D-4F99-BC99-1B96340DBD4E}</AdobeCode>
    <AdobeCode>{D3A52DD1-77BF-4D08-B692-F39820405EA0}</AdobeCode>
    <AdobeCode>{2C3FC99F-CD5D-4E71-B9F9-9A5BD74AB61C}</AdobeCode>
    <AdobeCode>{A91F37D6-78B6-4A6B-BF97-2310DB16A5DC}</AdobeCode>
    <AdobeCode>{906CDCD3-D83A-497C-B7CF-D489E638D35B}</AdobeCode>
    <AdobeCode>{D6E8A942-E06D-40A1-95AF-61F6D5EA1A6E}</AdobeCode>
    <AdobeCode>{8802A675-6E6D-4B1A-9DBC-414D5BFB15E1}</AdobeCode>
    <AdobeCode>{25A3BB29-A84F-4DAC-84CE-69D64EE98ED4}</AdobeCode>
    <AdobeCode>{21C39AA4-2460-42EA-90A4-4725F0AD2023}</AdobeCode>
    <AdobeCode>{FDBCA6B1-CEB9-4AF3-8CD6-5DC0C7D6FF3E}</AdobeCode>
    <AdobeCode>{38D198F3-DD47-4572-B430-BCE9ECBAE6E9}</AdobeCode>
    <AdobeCode>{137AB11A-2C96-4F91-8C46-D1CFB83F0DE1}</AdobeCode>
    <AdobeCode>{97CBBA6A-B22C-440F-A8E1-2C4064CD1253}</AdobeCode>
    <AdobeCode>{2E476A3F-3C92-484A-9604-50BDCCCF2A42}</AdobeCode>
    <AdobeCode>{239492CE-6CA4-4E48-B56F-40BB369E9F0F}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeCMapsx64CS6-4.0">
    <DisplayName>Adobe CMaps x64 CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="6904009"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>6904009</TotalSize>
      <MaxPathComponent>/TypeSupport/CS6/CMaps\UniJISX02132004-UTF32-H</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="6904009"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	<Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Ii][Nn][Gg] [Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Uu][Dd][Ii][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Ff][Tt][Ee][Rr][Ff][Xx]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ss][Hh][Ee][Ll][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Dd][Nn][Gg] [Cc][Oo][Nn][Vv][Ee][Rr][Tt][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee][Pp][Rr][Oo][Xx][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ee][Vv][Ii][Cc][Ee][Cc][Ee][Nn][Tt][Rr][Aa][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Rr][Ee][Aa][Mm][Ww][Ee][Aa][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Dd][Ss][Cc][Rr][Ii][Pp][Tt] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Rr][Aa][Ss][Hh][Rr][Ee][Pp][Oo][Rr][Tt][Ee][Rr]App\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh][Pp][Ll][Aa][Yy][Ee][Rr]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 7 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 8 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Cc][Oo][Pp][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Pp][Rr][Oo]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][[Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Tt][Oo][Cc][Kk] [Pp][Hh][Oo][Tt][Oo][Ss] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 4\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 5\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Oo][Rr][Gg][Aa][Nn][Ii][Zz][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Ee][Dd][Ii][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Ii][Mm][Aa][Gg][Ee][Rr][Ee][Aa][Dd][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee] [Dd][Vv][Dd]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Oo][Nn][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]10\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Cc][Oo][Mm]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Tt][Mm][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Ee][Ll][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Rr][Aa][Mm][Ee][Mm][Aa][Kk][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee][Cc][Aa][Pp][Tt][Ii][Vv][Aa][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Mm][Ii][Dd][Dd][Ll][Ee][Tt][Ii][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^3[Dd] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]\.[Ee][Xx][Ee]</Process>
    </Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe CMaps x64 CS6</Value>
      <Value lang="be_BY">Adobe CMaps x64 CS6</Value>
      <Value lang="bg_BG">Adobe CMaps x64 CS6</Value>
      <Value lang="ca_ES">Adobe CMaps x64 CS6</Value>
      <Value lang="cs_CZ">Adobe CMaps x64 CS6</Value>
      <Value lang="da_DK">Adobe CMaps x64 CS6</Value>
      <Value lang="de_DE">Adobe CMaps x64 CS6</Value>
      <Value lang="el_GR">Adobe CMaps x64 CS6</Value>
      <Value lang="en_GB">Adobe CMaps x64 CS6</Value>
      <Value lang="en_MX">Adobe CMaps x64 CS6</Value>
      <Value lang="en_US">Adobe CMaps x64 CS6</Value>
      <Value lang="en_XC">Adobe CMaps x64 CS6</Value>
      <Value lang="en_XM">Adobe CMaps x64 CS6</Value>
      <Value lang="es_ES">Adobe CMaps x64 CS6</Value>
      <Value lang="es_MX">Adobe CMaps x64 CS6</Value>
      <Value lang="es_QM">Adobe CMaps x64 CS6</Value>
      <Value lang="et_EE">Adobe CMaps x64 CS6</Value>
      <Value lang="fi_FI">Adobe CMaps x64 CS6</Value>
      <Value lang="fr_CA">Adobe CMaps x64 CS6</Value>
      <Value lang="fr_FR">Adobe CMaps x64 CS6</Value>
      <Value lang="fr_MX">Adobe CMaps x64 CS6</Value>
      <Value lang="fr_XM">Adobe CMaps x64 CS6</Value>
      <Value lang="he_IL">Adobe CMaps x64 CS6</Value>
      <Value lang="hi_IN">Adobe CMaps x64 CS6</Value>
      <Value lang="hr_HR">Adobe CMaps x64 CS6</Value>
      <Value lang="hu_HU">Adobe CMaps x64 CS6</Value>
      <Value lang="is_IS">Adobe CMaps x64 CS6</Value>
      <Value lang="it_IT">Adobe CMaps x64 CS6</Value>
      <Value lang="ja_JP">Adobe CMaps x64 CS6</Value>
      <Value lang="ko_KR">Adobe CMaps x64 CS6</Value>
      <Value lang="lt_LT">Adobe CMaps x64 CS6</Value>
      <Value lang="lv_LV">Adobe CMaps x64 CS6</Value>
      <Value lang="mk_MK">Adobe CMaps x64 CS6</Value>
      <Value lang="nb_NO">Adobe CMaps x64 CS6</Value>
      <Value lang="nl_NL">Adobe CMaps x64 CS6</Value>
      <Value lang="nn_NO">Adobe CMaps x64 CS6</Value>
      <Value lang="no_NO">Adobe CMaps x64 CS6</Value>
      <Value lang="pl_PL">Adobe CMaps x64 CS6</Value>
      <Value lang="pt_BR">Adobe CMaps x64 CS6</Value>
      <Value lang="ro_RO">Adobe CMaps x64 CS6</Value>
      <Value lang="ru_RU">Adobe CMaps x64 CS6</Value>
      <Value lang="sh_YU">Adobe CMaps x64 CS6</Value>
      <Value lang="sk_SK">Adobe CMaps x64 CS6</Value>
      <Value lang="sl_SI">Adobe CMaps x64 CS6</Value>
      <Value lang="sq_AL">Adobe CMaps x64 CS6</Value>
      <Value lang="sv_SE">Adobe CMaps x64 CS6</Value>
      <Value lang="th_TH">Adobe CMaps x64 CS6</Value>
      <Value lang="tr_TR">Adobe CMaps x64 CS6</Value>
      <Value lang="uk_UA">Adobe CMaps x64 CS6</Value>
      <Value lang="vi_VN">Adobe CMaps x64 CS6</Value>
      <Value lang="zh_CN">Adobe CMaps x64 CS6</Value>
      <Value lang="zh_TW">Adobe CMaps x64 CS6</Value>
      <Value lang="en_AE">Adobe CMaps x64 CS6</Value>
      <Value lang="en_IL">Adobe CMaps x64 CS6</Value>
      <Value lang="fr_MA">Adobe CMaps x64 CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0", "ChannelID", "AdobeCMapsx64CS6-4.0")
INSERT INTO PayloadData VALUES("{99290358-A784-4218-A7BA-954AE5F9207C}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeCMapsx64CS6-4.0">
    <DisplayName>Adobe CMaps x64 CS6</DisplayName>
  </Channel>')
