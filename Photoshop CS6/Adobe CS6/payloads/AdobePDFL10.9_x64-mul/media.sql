CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{BC1F7C3F-19C0-44EF-AD7A-C2DF9F180A33}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{72019B39-A1BF-49CF-BCF9-36436836F7C6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{E87A706C-648A-415D-B3FA-0A802E36B867}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{0B2AFA88-FF00-445C-BBA1-321A96190CC7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{FBA3B6AD-EC8E-4B3E-95F5-6E19D3F522DA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{1E10A9E5-CA55-4FB8-B090-8EDFA24ACFC0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{C28FA07C-01CE-416A-AC95-ABF2709F636A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{60C2635C-69D3-4144-A054-2893E943BDE3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{6DC9A0F4-9E06-4D10-869D-9A2B20D23D7B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "{65906E60-A413-4DE1-8780-3D927F904486}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{246C4B99-19F7-4475-9787-5FF8595B86D7}"/>')
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0" , "ValidationSig", "TKart9029EDOTSoeAmvnNKm4zF0AksSEKCTKTaMxlFTvjPmdPDjdAVko0tTE6IEa1w8epghDlnWqxt914gIORp+TwccaZWBJ/OdFt0rPaXc/86Zcl+mNi0uJEv3smFtUuw+xPounmeRGUaxC7OHstxEEFS9c1lA7o752amypgMUhNRAiyO7/6CeeLFj/Gjx5Js68ChgixjwK2vfmVGbI99CiHxlizIJRYdGTiS5L+x17YXw6IqY1VOqVLfi6/vVTrDH61ilfsDPFnYlSnDYMfAMSg8Z/DY7NFXdN34VORNoJ3QAcdI+8FdNtoQwKNvfOJRUlESrWqY/sXLjmkVOMgY9eLpv0/9iI5AjZWFvj12rilOXdhOLaRnyWz/qsVwcMvbp7Ef729PNgR9AC6joo1fseJQk+2zr71ChnYr5a0wOrsTBkc5Rs+t7ePRnfKOG3dWodG1N1V3FgnU2s8Rq471SThtK02nnSidbN6Uy0lvmTlwZ5r7xcMygIrQgzJ+PI")
INSERT INTO Payloads VALUES	("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "CoreTech", "AdobePDFL x64 CS6", "10.9", "normal")
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-11-28 17:25:51.796000</Property>
    <Property name="TargetName">AdobePDFL10.9_x64-mul</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{246C4B99-19F7-4475-9787-5FF8595B86D7}</Property>
    <Property name="ProductName">Adobe PDF Library Files x64 CS6</Property>
    <Property name="ProductVersion">10.9</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\PDFL\10.9</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobePDFL x64 CS6</ProductName>
	<ProductVersion>10.9</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{BC1F7C3F-19C0-44EF-AD7A-C2DF9F180A33}</AdobeCode>
    <AdobeCode>{72019B39-A1BF-49CF-BCF9-36436836F7C6}</AdobeCode>
    <AdobeCode>{E87A706C-648A-415D-B3FA-0A802E36B867}</AdobeCode>
    <AdobeCode>{0B2AFA88-FF00-445C-BBA1-321A96190CC7}</AdobeCode>
    <AdobeCode>{FBA3B6AD-EC8E-4B3E-95F5-6E19D3F522DA}</AdobeCode>
    <AdobeCode>{1E10A9E5-CA55-4FB8-B090-8EDFA24ACFC0}</AdobeCode>
    <AdobeCode>{C28FA07C-01CE-416A-AC95-ABF2709F636A}</AdobeCode>
    <AdobeCode>{60C2635C-69D3-4144-A054-2893E943BDE3}</AdobeCode>
    <AdobeCode>{6DC9A0F4-9E06-4D10-869D-9A2B20D23D7B}</AdobeCode>
    <AdobeCode>{65906E60-A413-4DE1-8780-3D927F904486}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobePDFLibraryFilesx64CS6-10.9">
    <DisplayName>Adobe PDF Library Files x64 CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="71046768"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>71046768</TotalSize>
      <MaxPathComponent>/PDFL/10.9/Fonts\AdobeArabic-BoldItalic.otf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="71046768"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">10.9</Value>
      <Value lang="be_BY">10.9</Value>
      <Value lang="bg_BG">10.9</Value>
      <Value lang="ca_ES">10.9</Value>
      <Value lang="cs_CZ">10.9</Value>
      <Value lang="da_DK">10.9</Value>
      <Value lang="de_DE">10.9</Value>
      <Value lang="el_GR">10.9</Value>
      <Value lang="en_GB">10.9</Value>
      <Value lang="en_MX">10.9</Value>
      <Value lang="en_US">10.9</Value>
      <Value lang="en_XC">10.9</Value>
      <Value lang="en_XM">10.9</Value>
      <Value lang="es_ES">10.9</Value>
      <Value lang="es_MX">10.9</Value>
      <Value lang="es_QM">10.9</Value>
      <Value lang="et_EE">10.9</Value>
      <Value lang="fi_FI">10.9</Value>
      <Value lang="fr_CA">10.9</Value>
      <Value lang="fr_FR">10.9</Value>
      <Value lang="fr_MX">10.9</Value>
      <Value lang="fr_XM">10.9</Value>
      <Value lang="he_IL">10.9</Value>
      <Value lang="hi_IN">10.9</Value>
      <Value lang="hr_HR">10.9</Value>
      <Value lang="hu_HU">10.9</Value>
      <Value lang="is_IS">10.9</Value>
      <Value lang="it_IT">10.9</Value>
      <Value lang="ja_JP">10.9</Value>
      <Value lang="ko_KR">10.9</Value>
      <Value lang="lt_LT">10.9</Value>
      <Value lang="lv_LV">10.9</Value>
      <Value lang="mk_MK">10.9</Value>
      <Value lang="nb_NO">10.9</Value>
      <Value lang="nl_NL">10.9</Value>
      <Value lang="nn_NO">10.9</Value>
      <Value lang="no_NO">10.9</Value>
      <Value lang="pl_PL">10.9</Value>
      <Value lang="pt_BR">10.9</Value>
      <Value lang="ro_RO">10.9</Value>
      <Value lang="ru_RU">10.9</Value>
      <Value lang="sh_YU">10.9</Value>
      <Value lang="sk_SK">10.9</Value>
      <Value lang="sl_SI">10.9</Value>
      <Value lang="sq_AL">10.9</Value>
      <Value lang="sv_SE">10.9</Value>
      <Value lang="th_TH">10.9</Value>
      <Value lang="tr_TR">10.9</Value>
      <Value lang="uk_UA">10.9</Value>
      <Value lang="vi_VN">10.9</Value>
      <Value lang="zh_CN">10.9</Value>
      <Value lang="zh_TW">10.9</Value>
      <Value lang="en_AE">10.9</Value>
      <Value lang="en_IL">10.9</Value>
      <Value lang="fr_MA">10.9</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="be_BY">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="bg_BG">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="ca_ES">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="cs_CZ">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="da_DK">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="de_DE">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="el_GR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_GB">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_MX">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_US">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_XC">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_XM">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="es_ES">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="es_MX">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="es_QM">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="et_EE">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fi_FI">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fr_CA">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fr_FR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fr_MX">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fr_XM">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="he_IL">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="hi_IN">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="hr_HR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="hu_HU">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="is_IS">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="it_IT">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="ja_JP">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="ko_KR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="lt_LT">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="lv_LV">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="mk_MK">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="nb_NO">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="nl_NL">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="nn_NO">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="no_NO">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="pl_PL">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="pt_BR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="ro_RO">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="ru_RU">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="sh_YU">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="sk_SK">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="sl_SI">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="sq_AL">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="sv_SE">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="th_TH">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="tr_TR">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="uk_UA">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="vi_VN">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="zh_CN">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="zh_TW">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_AE">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="en_IL">Adobe PDF Library Files x64 CS6</Value>
      <Value lang="fr_MA">Adobe PDF Library Files x64 CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0", "ChannelID", "AdobePDFLibraryFilesx64CS6-10.9")
INSERT INTO PayloadData VALUES("{246C4B99-19F7-4475-9787-5FF8595B86D7}", "0", "ChannelInfo", '<Channel enable="1" id="AdobePDFLibraryFilesx64CS6-10.9">
    <DisplayName>Adobe PDF Library Files x64 CS6</DisplayName>
  </Channel>')
