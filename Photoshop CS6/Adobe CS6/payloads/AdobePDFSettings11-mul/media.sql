CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "{A5C2B21F-52B6-4D7D-8677-F403750B2455}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{7E91BB17-16A1-42CE-9502-D6C98BE04920}"/>')
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0" , "ValidationSig", "W9vf6I4naE3PC2mut2UtQf1QRHwrHDj0S7FraTYSUyE/hpstj1gUX7EmTNbN1P23Z9LLxkepzQ/qas6+kWA1K10HvG5c9EeF/dvgBHj5KZ2QBDKTOmr7f6Tl/6fQuiAMQQTq+uLGf//iXm7wTCAeXXrDEgPA2iy0I2R/BQWokWai/E7jusmTFBkbdPfn34mkwXunqWBSJbu7WjRwMDMJUS+HV164qYbR0BHGlqe5DPBDB4WZkE2dHUpg437zKVWQjzxp4P5iVB7q2WFbmTwYNW9xaiJL6L+zt+DEUfO66ADBuVta8WrGxH00ILhZHQEvpZGq7dqOEfA2Fa2t8RDVB5qkUBVgTg/Ta/AHoMmMLZk8nRGAQkV+R+vSl46HaCCC0HjjBNokAhrFUCb6cSBzjBT2c1lLILkHEIyRQph10O+zBWl0bw15DXOXC5ijpp716ntdEHPsPajReqosqccs/pYk9QXCU5RnD9yigu/iu9QoU+gFpZ0xxatfz7R63j77")
INSERT INTO Payloads VALUES	("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "NonAdobePayload", "PDF Settings CS6", "11.0", "normal")
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-16 17:12:46.338000</Property>
    <Property name="TargetName">AdobePDFSettings11-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{7E91BB17-16A1-42CE-9502-D6C98BE04920}</Property>
    <Property name="ProductName">PDF Settings CS6</Property>
    <Property name="ProductVersion">11.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="0" name="Default" folderName="">[Common]\Adobe\Adobe PDF</Platform>
    <Platform isFixed="0" name="OSX" folderName="">[Common]/Adobe/Adobe PDF</Platform>
    <Platform isFixed="0" name="Win32" folderName="">[SharedApplicationData]/Adobe/Adobe PDF</Platform>
  </InstallDir><Languages>
    <Language>en_US</Language>
    <Language>en_GB</Language>
    <Language>de_DE</Language>
    <Language>fr_FR</Language>
    <Language>ar_AE</Language>
    <Language>cs_CZ</Language>
    <Language>da_DK</Language>
    <Language>nl_NL</Language>
    <Language>fi_FI</Language>
    <Language>el_GR</Language>
    <Language>hu_HU</Language>
    <Language>it_IT</Language>
    <Language>nb_NO</Language>
    <Language>pl_PL</Language>
    <Language>pt_BR</Language>
    <Language>ru_RU</Language>
    <Language>es_ES</Language>
    <Language>sv_SE</Language>
    <Language>tr_TR</Language>
    <Language>sq_AL</Language>
    <Language>he_IL</Language>
    <Language>no_NO</Language>
    <Language>sl_SI</Language>
    <Language>en_AE</Language>
    <Language>en_IL</Language>
    <Language>fr_MA</Language>
    <Language>he_IL</Language>
    <Language>ro_RO</Language>
    <Language>uk_UA</Language>
    <Language>es_MX</Language>
    <Language>fr_CA</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>NonAdobePayload</Family>
		<ProductName>PDF Settings CS6</ProductName>
	<ProductVersion>11.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{A5C2B21F-52B6-4D7D-8677-F403750B2455}</AdobeCode>
  </Upgrades><Channel enable="1" id="PDFSettingsCS6-11.0">
    <DisplayName>PDF Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2259968">
    <Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>2259968</TotalSize>
      <MaxPathComponent>AdobePDFSettings11-mul.msi</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2259968"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="en_US">11.0</Value>
      <Value lang="en_GB">11.0</Value>
      <Value lang="de_DE">11.0</Value>
      <Value lang="fr_FR">11.0</Value>
      <Value lang="ar_AE">11.0</Value>
      <Value lang="cs_CZ">11.0</Value>
      <Value lang="da_DK">11.0</Value>
      <Value lang="nl_NL">11.0</Value>
      <Value lang="fi_FI">11.0</Value>
      <Value lang="el_GR">11.0</Value>
      <Value lang="hu_HU">11.0</Value>
      <Value lang="it_IT">11.0</Value>
      <Value lang="nb_NO">11.0</Value>
      <Value lang="pl_PL">11.0</Value>
      <Value lang="pt_BR">11.0</Value>
      <Value lang="ru_RU">11.0</Value>
      <Value lang="es_ES">11.0</Value>
      <Value lang="sv_SE">11.0</Value>
      <Value lang="tr_TR">11.0</Value>
      <Value lang="sq_AL">11.0</Value>
      <Value lang="he_IL">11.0</Value>
      <Value lang="no_NO">11.0</Value>
      <Value lang="sl_SI">11.0</Value>
      <Value lang="en_AE">11.0</Value>
      <Value lang="en_IL">11.0</Value>
      <Value lang="fr_MA">11.0</Value>
      <Value lang="he_IL">11.0</Value>
      <Value lang="ro_RO">11.0</Value>
      <Value lang="uk_UA">11.0</Value>
      <Value lang="es_MX">11.0</Value>
      <Value lang="fr_CA">11.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="en_US">PDF Settings CS6</Value>
      <Value lang="en_GB">PDF Settings CS6</Value>
      <Value lang="de_DE">PDF Settings CS6</Value>
      <Value lang="fr_FR">PDF Settings CS6</Value>
      <Value lang="ar_AE">PDF Settings CS6</Value>
      <Value lang="cs_CZ">PDF Settings CS6</Value>
      <Value lang="da_DK">PDF Settings CS6</Value>
      <Value lang="nl_NL">PDF Settings CS6</Value>
      <Value lang="fi_FI">PDF Settings CS6</Value>
      <Value lang="el_GR">PDF Settings CS6</Value>
      <Value lang="hu_HU">PDF Settings CS6</Value>
      <Value lang="it_IT">PDF Settings CS6</Value>
      <Value lang="nb_NO">PDF Settings CS6</Value>
      <Value lang="pl_PL">PDF Settings CS6</Value>
      <Value lang="pt_BR">PDF Settings CS6</Value>
      <Value lang="ru_RU">PDF Settings CS6</Value>
      <Value lang="es_ES">PDF Settings CS6</Value>
      <Value lang="sv_SE">PDF Settings CS6</Value>
      <Value lang="tr_TR">PDF Settings CS6</Value>
      <Value lang="sq_AL">PDF Settings CS6</Value>
      <Value lang="he_IL">PDF Settings CS6</Value>
      <Value lang="no_NO">PDF Settings CS6</Value>
      <Value lang="sl_SI">PDF Settings CS6</Value>
      <Value lang="en_AE">PDF Settings CS6</Value>
      <Value lang="en_IL">PDF Settings CS6</Value>
      <Value lang="fr_MA">PDF Settings CS6</Value>
      <Value lang="he_IL">PDF Settings CS6</Value>
      <Value lang="ro_RO">PDF Settings CS6</Value>
      <Value lang="uk_UA">PDF Settings CS6</Value>
      <Value lang="es_MX">PDF Settings CS6</Value>
      <Value lang="fr_CA">PDF Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences><ThirdPartyComponent modifierID="{7705FB8E-A46B-4106-9367-541CEA94F802}">
							<Metadata>
								<Type>msiPackage</Type>
								<Name>AdobePDFSettings11-mul.msi</Name>
								<Properties>
									<Property name="ProductCode">{BFEAAE77-BD7F-4534-B286-9C5CB4697EB1}</Property>
								</Properties>
                
							</Metadata>
							<Arguments prefix="" delimiter="=">
								<Install>
                  <Argument name="ADOBE_SETUP">1</Argument>
                </Install>
								<Repair>
                  <Argument name="ADOBE_SETUP">1</Argument>
                </Repair>
							</Arguments>
							<SuccessCodes>
								<Install>
									<Range lowerBound="0" upperBound="0"/>
								</Install>
							</SuccessCodes>
							<Capabilities>
								<Install stringID="abc">
									<Value lang="en_US">You just installed Adobe PDF Settings CS6</Value>
								<Value lang="en_GB">You just installed Adobe PDF Settings CS6 {en_GB} </Value>
        <Value lang="de_DE">You just installed Adobe PDF Settings CS6 {de_DE} </Value>
        <Value lang="fr_FR">You just installed Adobe PDF Settings CS6 {fr_FR} </Value>
        <Value lang="ar_AE">You just installed Adobe PDF Settings CS6 {ar_AE} </Value>
        <Value lang="cs_CZ">You just installed Adobe PDF Settings CS6 {cs_CZ} </Value>
        <Value lang="da_DK">You just installed Adobe PDF Settings CS6 {da_DK} </Value>
        <Value lang="nl_NL">You just installed Adobe PDF Settings CS6 {nl_NL} </Value>
        <Value lang="fi_FI">You just installed Adobe PDF Settings CS6 {fi_FI} </Value>
        <Value lang="el_GR">You just installed Adobe PDF Settings CS6 {el_GR} </Value>
        <Value lang="hu_HU">You just installed Adobe PDF Settings CS6 {hu_HU} </Value>
        <Value lang="it_IT">You just installed Adobe PDF Settings CS6 {it_IT} </Value>
        <Value lang="nb_NO">You just installed Adobe PDF Settings CS6 {nb_NO} </Value>
        <Value lang="pl_PL">You just installed Adobe PDF Settings CS6 {pl_PL} </Value>
        <Value lang="pt_BR">You just installed Adobe PDF Settings CS6 {pt_BR} </Value>
        <Value lang="ru_RU">You just installed Adobe PDF Settings CS6 {ru_RU} </Value>
        <Value lang="es_ES">You just installed Adobe PDF Settings CS6 {es_ES} </Value>
        <Value lang="sv_SE">You just installed Adobe PDF Settings CS6 {sv_SE} </Value>
        <Value lang="tr_TR">You just installed Adobe PDF Settings CS6 {tr_TR} </Value>
        <Value lang="sq_AL">You just installed Adobe PDF Settings CS6 {sq_AL} </Value>
        <Value lang="he_IL">You just installed Adobe PDF Settings CS6 {he_IL} </Value>
        <Value lang="no_NO">You just installed Adobe PDF Settings CS6 {no_NO} </Value>
        <Value lang="sl_SI">You just installed Adobe PDF Settings CS6 {sl_SI} </Value>
        <Value lang="en_AE">You just installed Adobe PDF Settings CS6 {en_AE} </Value>
        <Value lang="en_IL">You just installed Adobe PDF Settings CS6 {en_IL} </Value>
        <Value lang="fr_MA">You just installed Adobe PDF Settings CS6 {fr_MA} </Value>
        <Value lang="ro_RO">You just installed Adobe PDF Settings CS6 {ro_RO} </Value>
        <Value lang="uk_UA">You just installed Adobe PDF Settings CS6 {uk_UA} </Value>
        <Value lang="es_MX">You just installed Adobe PDF Settings CS6 {es_MX} </Value>
        <Value lang="fr_CA">You just installed Adobe PDF Settings CS6 {fr_CA} </Value>
      </Install>
								<Repair isRepairable="1" stringID="pqr">
									<Value lang="en_US">Adobe PDF Settings CS6 was reinstalled</Value>
								<Value lang="en_GB">Adobe PDF Settings CS6 was reinstalled {en_GB} </Value>
        <Value lang="de_DE">Adobe PDF Settings CS6 was reinstalled {de_DE} </Value>
        <Value lang="fr_FR">Adobe PDF Settings CS6 was reinstalled {fr_FR} </Value>
        <Value lang="ar_AE">Adobe PDF Settings CS6 was reinstalled {ar_AE} </Value>
        <Value lang="cs_CZ">Adobe PDF Settings CS6 was reinstalled {cs_CZ} </Value>
        <Value lang="da_DK">Adobe PDF Settings CS6 was reinstalled {da_DK} </Value>
        <Value lang="nl_NL">Adobe PDF Settings CS6 was reinstalled {nl_NL} </Value>
        <Value lang="fi_FI">Adobe PDF Settings CS6 was reinstalled {fi_FI} </Value>
        <Value lang="el_GR">Adobe PDF Settings CS6 was reinstalled {el_GR} </Value>
        <Value lang="hu_HU">Adobe PDF Settings CS6 was reinstalled {hu_HU} </Value>
        <Value lang="it_IT">Adobe PDF Settings CS6 was reinstalled {it_IT} </Value>
        <Value lang="nb_NO">Adobe PDF Settings CS6 was reinstalled {nb_NO} </Value>
        <Value lang="pl_PL">Adobe PDF Settings CS6 was reinstalled {pl_PL} </Value>
        <Value lang="pt_BR">Adobe PDF Settings CS6 was reinstalled {pt_BR} </Value>
        <Value lang="ru_RU">Adobe PDF Settings CS6 was reinstalled {ru_RU} </Value>
        <Value lang="es_ES">Adobe PDF Settings CS6 was reinstalled {es_ES} </Value>
        <Value lang="sv_SE">Adobe PDF Settings CS6 was reinstalled {sv_SE} </Value>
        <Value lang="tr_TR">Adobe PDF Settings CS6 was reinstalled {tr_TR} </Value>
        <Value lang="sq_AL">Adobe PDF Settings CS6 was reinstalled {sq_AL} </Value>
        <Value lang="he_IL">Adobe PDF Settings CS6 was reinstalled {he_IL} </Value>
        <Value lang="no_NO">Adobe PDF Settings CS6 was reinstalled {no_NO} </Value>
        <Value lang="sl_SI">Adobe PDF Settings CS6 was reinstalled {sl_SI} </Value>
        <Value lang="en_AE">Adobe PDF Settings CS6 was reinstalled {en_AE} </Value>
        <Value lang="en_IL">Adobe PDF Settings CS6 was reinstalled {en_IL} </Value>
        <Value lang="fr_MA">Adobe PDF Settings CS6 was reinstalled {fr_MA} </Value>
        <Value lang="ro_RO">Adobe PDF Settings CS6 was reinstalled {ro_RO} </Value>
        <Value lang="uk_UA">Adobe PDF Settings CS6 was reinstalled {uk_UA} </Value>
        <Value lang="es_MX">Adobe PDF Settings CS6 was reinstalled {es_MX} </Value>
        <Value lang="fr_CA">Adobe PDF Settings CS6 was reinstalled {fr_CA} </Value>
      </Repair>
								<Uninstall stringID="xyz" isUninstallable="1">
									<Value lang="en_US">Adobe PDF Settings CS6 was uninstalled</Value>
								<Value lang="en_GB">Adobe PDF Settings CS6 was uninstalled {en_GB} </Value>
        <Value lang="de_DE">Adobe PDF Settings CS6 was uninstalled {de_DE} </Value>
        <Value lang="fr_FR">Adobe PDF Settings CS6 was uninstalled {fr_FR} </Value>
        <Value lang="ar_AE">Adobe PDF Settings CS6 was uninstalled {ar_AE} </Value>
        <Value lang="cs_CZ">Adobe PDF Settings CS6 was uninstalled {cs_CZ} </Value>
        <Value lang="da_DK">Adobe PDF Settings CS6 was uninstalled {da_DK} </Value>
        <Value lang="nl_NL">Adobe PDF Settings CS6 was uninstalled {nl_NL} </Value>
        <Value lang="fi_FI">Adobe PDF Settings CS6 was uninstalled {fi_FI} </Value>
        <Value lang="el_GR">Adobe PDF Settings CS6 was uninstalled {el_GR} </Value>
        <Value lang="hu_HU">Adobe PDF Settings CS6 was uninstalled {hu_HU} </Value>
        <Value lang="it_IT">Adobe PDF Settings CS6 was uninstalled {it_IT} </Value>
        <Value lang="nb_NO">Adobe PDF Settings CS6 was uninstalled {nb_NO} </Value>
        <Value lang="pl_PL">Adobe PDF Settings CS6 was uninstalled {pl_PL} </Value>
        <Value lang="pt_BR">Adobe PDF Settings CS6 was uninstalled {pt_BR} </Value>
        <Value lang="ru_RU">Adobe PDF Settings CS6 was uninstalled {ru_RU} </Value>
        <Value lang="es_ES">Adobe PDF Settings CS6 was uninstalled {es_ES} </Value>
        <Value lang="sv_SE">Adobe PDF Settings CS6 was uninstalled {sv_SE} </Value>
        <Value lang="tr_TR">Adobe PDF Settings CS6 was uninstalled {tr_TR} </Value>
        <Value lang="sq_AL">Adobe PDF Settings CS6 was uninstalled {sq_AL} </Value>
        <Value lang="he_IL">Adobe PDF Settings CS6 was uninstalled {he_IL} </Value>
        <Value lang="no_NO">Adobe PDF Settings CS6 was uninstalled {no_NO} </Value>
        <Value lang="sl_SI">Adobe PDF Settings CS6 was uninstalled {sl_SI} </Value>
        <Value lang="en_AE">Adobe PDF Settings CS6 was uninstalled {en_AE} </Value>
        <Value lang="en_IL">Adobe PDF Settings CS6 was uninstalled {en_IL} </Value>
        <Value lang="fr_MA">Adobe PDF Settings CS6 was uninstalled {fr_MA} </Value>
        <Value lang="ro_RO">Adobe PDF Settings CS6 was uninstalled {ro_RO} </Value>
        <Value lang="uk_UA">Adobe PDF Settings CS6 was uninstalled {uk_UA} </Value>
        <Value lang="es_MX">Adobe PDF Settings CS6 was uninstalled {es_MX} </Value>
        <Value lang="fr_CA">Adobe PDF Settings CS6 was uninstalled {fr_CA} </Value>
      </Uninstall>
							</Capabilities>
						</ThirdPartyComponent></PayloadInfo>')
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0", "ChannelID", "PDFSettingsCS6-11.0")
INSERT INTO PayloadData VALUES("{7E91BB17-16A1-42CE-9502-D6C98BE04920}", "0", "ChannelInfo", '<Channel enable="1" id="PDFSettingsCS6-11.0">
    <DisplayName>PDF Settings CS6</DisplayName>
  </Channel>')
