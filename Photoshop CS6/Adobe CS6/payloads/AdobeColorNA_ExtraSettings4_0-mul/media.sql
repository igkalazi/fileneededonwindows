CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{B7FF69B2-4797-40C5-A06B-A522A317FFC3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{F8270930-E244-46C5-BD13-1F032179AFB0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{CABE67E2-C3D3-467D-BC92-99B2AFCD3A33}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{E868C2F6-ABC0-449F-B7E8-D34E31FF9AEE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{AA162854-CC59-4420-914A-C8D389D1DD5D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{D98EBF5C-20AE-4D50-AED4-ED4EFA800EDE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{83F1CE78-C7A1-4837-8A97-C1FA24DD09C1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{FC0AD246-8531-44F2-AE71-F08D8FA23764}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{240969A7-AA8F-4F22-BC97-809514DBDD27}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "{79293CD7-3021-4E21-8F7A-A86F20A36346}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{42C0738D-8D50-45B7-BC51-4BC609133E3A}"/>')
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0" , "ValidationSig", "a4cRWePx3XjMxoCOhzTktrDQTZo5hwxhR6zgZ011AenN3NdWoVnHb+Yk0WHW3GCbvoWAOA5tyr2DheHGOgLEGQVhnGdfrKsmh+mxNZ6EgkbrEZ5DzwF0DtbGSLixFlkP7BWmhOCKSJQ3hh57D0XFF/SZLOemr/6v9pcf5o0mLXovdSXVxx9OD8bQ8P5hfTYvEyICTkQL8OfVMR0zM1s4GnMMWfx/cxaiLcgATI5JaL8yVJQxzHqwTC4/oHYyp9hXnvZy9Q8bJVeFmOWbSgWLag9yM6DCoXqVcFYsXyUitOLaJkKy2sHrAF/taguaUxqLko+KiEcKUo1y5iAEPMkbHxAlrr2zGULuxXB6AOUq+6eSnBNiEDKyKHFwOhAWuyMcTSJjLhUjNsPrBtBNHEQmSYSxoqrOY9eN479Rz6CsJ+7aOwowFraibTFhpzC0/siOmfYhQHukNTvBxf4sx2Lv742jsGZ0gXT+rosjmurUvisUtoX8rGIwRabArfBu+afd")
INSERT INTO Payloads VALUES	("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "CoreTech", "AdobeColorNA CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-15 16:33:32.730000</Property>
    <Property name="TargetName">AdobeColorNA_ExtraSettings4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{42C0738D-8D50-45B7-BC51-4BC609133E3A}</Property>
    <Property name="ProductName">Adobe Color NA Extra Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>ar_AE</Language>
    <Language>cs_CZ</Language>
    <Language>da_DK</Language>
    <Language>de_DE</Language>
    <Language>el_GR</Language>
    <Language>en_GB</Language>
    <Language>en_AE</Language>
    <Language>en_IL</Language>
    <Language>es_ES</Language>
    <Language>fi_FI</Language>
    <Language>fr_FR</Language>
    <Language>fr_MA</Language>
    <Language>he_IL</Language>
    <Language>hu_HU</Language>
    <Language>it_IT</Language>
    <Language>nb_NO</Language>
    <Language>nl_NL</Language>
    <Language>pl_PL</Language>
    <Language>pt_BR</Language>
    <Language>ro_RO</Language>
    <Language>ru_RU</Language>
    <Language>sv_SE</Language>
    <Language>tr_TR</Language>
    <Language>uk_UA</Language>
    <Language>es_MX</Language>
    <Language>ja_JP</Language>
    <Language>ko_KR</Language>
    <Language>zh_CN</Language>
    <Language>zh_TW</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorNA CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{B7FF69B2-4797-40C5-A06B-A522A317FFC3}</AdobeCode>
    <AdobeCode>{F8270930-E244-46C5-BD13-1F032179AFB0}</AdobeCode>
    <AdobeCode>{CABE67E2-C3D3-467D-BC92-99B2AFCD3A33}</AdobeCode>
    <AdobeCode>{E868C2F6-ABC0-449F-B7E8-D34E31FF9AEE}</AdobeCode>
    <AdobeCode>{AA162854-CC59-4420-914A-C8D389D1DD5D}</AdobeCode>
    <AdobeCode>{D98EBF5C-20AE-4D50-AED4-ED4EFA800EDE}</AdobeCode>
    <AdobeCode>{83F1CE78-C7A1-4837-8A97-C1FA24DD09C1}</AdobeCode>
    <AdobeCode>{FC0AD246-8531-44F2-AE71-F08D8FA23764}</AdobeCode>
    <AdobeCode>{240969A7-AA8F-4F22-BC97-809514DBDD27}</AdobeCode>
    <AdobeCode>{79293CD7-3021-4E21-8F7A-A86F20A36346}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorNAExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color NA Extra Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2390004"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2390004</TotalSize>
      <MaxPathComponent>/Color/Settings/ExtraSettings\North America General Purpose.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2390004"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="cs_CZ">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="da_DK">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="de_DE">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="el_GR">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="en_GB">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="en_AE">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="en_IL">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="es_ES">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="fi_FI">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="fr_FR">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="fr_MA">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="he_IL">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="hu_HU">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="it_IT">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="nb_NO">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="nl_NL">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="pl_PL">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="pt_BR">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="ro_RO">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="ru_RU">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="sv_SE">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="tr_TR">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="uk_UA">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="es_MX">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="ja_JP">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="ko_KR">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="zh_CN">Adobe Color NA Extra Settings CS6</Value>
      <Value lang="zh_TW">Adobe Color NA Extra Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0", "ChannelID", "AdobeColorNAExtraSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{42C0738D-8D50-45B7-BC51-4BC609133E3A}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorNAExtraSettingsCS6-4.0">
    <DisplayName>Adobe Color NA Extra Settings CS6</DisplayName>
  </Channel>')
