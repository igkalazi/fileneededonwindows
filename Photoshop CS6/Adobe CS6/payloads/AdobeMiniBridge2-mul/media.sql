CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "", "critical", "CoreTech", "Adobe SwitchBoard 2.0", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "", "required", "NonAdobePayload", "Microsoft Visual C++ 2005 Redistributable Package (x86)", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "", "required", "NonAdobePayload", "Microsoft Visual C++ 2008 Redistributable Package (x86)", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{84883474-159A-4CBC-97C7-505D6D221866}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{12505BAC-ED5F-4813-8327-AE54F008882D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{9A46650B-6B78-472B-83B6-997E8E7266E5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{304F86EA-42E9-4CE3-9458-2214DEA3C540}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{8A7965C0-7D26-4943-99A8-C334AA25A559}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{EBD9E02F-FBF2-4E82-B0B6-56A1A46C3267}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{4613309A-DEAE-4D81-92E2-69D684B22AC4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{3C9BE3D6-3DC0-4756-80CB-37DCFE4698BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{847D5939-0E39-43C0-9D3A-BB729A11D398}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{AB74C2BB-BA3F-482A-AF3D-DBA2016347DD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{48416FC2-9949-4AB3-8312-FA828DCD272B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{093283A9-230D-4566-95A7-561261BA419D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{5BBC27D9-2634-4E5C-B351-77363901AAB4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{C845E6EF-316C-4B5D-8468-E7F506604887}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{569FE4EC-A005-40EC-B887-1F275EED770D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{CDBE0C6D-18AF-4F0B-ADF5-07F0CE6DBBDC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{E929FB30-9AEF-4455-B9CE-552D60F821E3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{5A10BFDD-C787-47CA-88F9-9019955EB7DA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{394CCE73-AD4B-480F-A0E6-FDC4CFB4E926}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{B5D1C1C6-268C-4513-A6A8-BE678AAC1AA3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{490E441B-18E6-4A1A-AD78-0EADC4C9E812}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{5D9013E7-39F9-4732-A413-69EF4CDA0757}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{1EDA6530-4384-49EF-AD6D-573AD78EFC6C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{DC622AAB-CE9F-4D48-9629-F554EC03755B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{0A10675F-9C21-47B7-94F2-3102F4D291A1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{45936C68-C7CD-4481-A686-C51AF205AF7A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{9972FA59-A5AD-4662-8C1B-956B3A10746F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{13EC728D-F478-4347-B493-727E5F3B6225}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{79C67047-6E6C-4205-A681-70F3EAE6DFF8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{0F9DF6CB-F5AF-4822-8B02-116024F33647}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{9055761D-59A3-408F-80B5-E0B48C0F248A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{E24E3517-D442-4F4F-AD1C-F015A8299FAC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{D99DBF25-C129-4E11-A4BF-280F6706DD8F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{C71057DD-546E-468A-94F0-A6B97EDF2FAD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{46DD2DB6-9114-4C80-B6DB-7A074512A644}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{79403DC6-022D-499A-B29A-D985379FC549}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{C41EC6D1-890A-4A49-B196-81E951E1DAC3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{91DE27C3-330A-4A7C-82BE-28DEBDD42EA6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{2FF85DFB-8327-431E-B682-B439285BC71B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{D7239BAF-F96A-46A5-AD5D-93E598F09BEA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{6D904447-991B-41E4-9BDA-D82839726F74}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{163B0BAA-D144-432D-AAF3-66AEDBB6F0AD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{06BDD292-5B1C-4DFF-A350-D9EC7471399A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{CC1F4CFD-B7B6-4870-A586-E476BD3CA33C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{98BFBF74-AE20-4EED-AD3F-161CF827F083}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{DB5D65D2-5EB9-4A07-A2B2-876D7413D7AE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{B5133D18-92E1-48C2-8B81-DA845DB900B9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{8A52E3E3-2719-42AD-AF66-4C3AE0581B52}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{489BFD02-BD36-4746-B071-D53F3DEEDB2F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{FABC0758-7854-456E-A867-BC29CA90671B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{D3BD2315-1A2C-4823-9378-DD681CB84C12}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{D883F5C4-5544-46ED-ABA0-C852C0378FFC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{45C08B1A-9947-4AA5-BDC0-2C23133733FD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{34B21707-FA81-4D2C-9ACB-0D3035297EC8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{AA3842C0-C440-4FA0-9B21-836FA00B0C59}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{5E21D862-2420-4B80-AB26-6A51DB2184D0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{E787A200-713A-4365-AE82-DC466CDBE3F8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{04927242-CB98-417B-B412-BF8476B994BC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{C601AC54-8AC1-4A9A-837B-060D024B020A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{DFD9D930-9759-40BE-8E09-6EBD4794F196}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{A79B9DB2-B8D8-4F94-A9D3-EBDDC92E3B5F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "{50AC2B57-DF60-4D93-A35E-9EB5613ADB19}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{B98E0DCB-1A75-4394-9813-3A114AA3891D}"/>')
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0" , "ValidationSig", "rkWtUFENbmzkE6CwP0F7UtePkQfZTmhCjIErwtVSZ1YGHCSaGKpIo2g1B38+uHPI3KRFGkCmNSTe7dmS0U9U70c37ITkqt2jm7IXouJpQCV5heax+1fg04+tioWwRZ9jIFkRQgSuWr2o4L3lICvotwDGw/Jcm+/y/GqXjnaAEEiE95zVFWKfjTJi2fheWv+CtRfpShwHHJvK9x8j6Em9T3ar5NDb7jPudFA4OfyanjfihBOiJBelRbDb/QpUnVpq1Tk+S80qGzyT+VIOBbcm+ZQZxTfIbihJlpeCPybUH6khiSBkIuMlpnc9XQ9eYdUC2jjnieccRAw+DjDrSnlDTzC4OvR2Y9qbPL72ygcgIIyQcKx1LgPu0nAXWksjkaMGgitPEJNxUF8Leb0P8DX0ieG6cb8vazt5DZq3wOTO6KProu7o0xmQr1LmfWCQg3vVJegqQWLXFuK/Kobg092UscZx6ow9PNpfbRgJzzOkRG+C7H30TUBI+WyKE4f9e2g5")
INSERT INTO Payloads VALUES	("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "Standard", "Adobe Mini Bridge CS6", "2", "normal")
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-02-24 11:37:30.914000</Property>
    <Property name="TargetName">AdobeMiniBridge2-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{B98E0DCB-1A75-4394-9813-3A114AA3891D}</Property>
    <Property name="ProductName">Adobe Mini Bridge CS6</Property>
    <Property name="ProductVersion">2</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\CS6ServiceManager\extensions\Adobe Mini Bridge CS6</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>Standard</Family>
		<ProductName>Adobe Mini Bridge CS6</ProductName>
	<ProductVersion>2</ProductVersion>
    </ProductInfo>
  </Satisfies><Dependencies>
    <ProductInfo type="critical">
      <Family>CoreTech</Family>
      <ProductName>Adobe SwitchBoard 2.0</ProductName>
    </ProductInfo>
    <ProductInfo type="required">
      <Family>NonAdobePayload</Family>
      <ProductName>Microsoft Visual C++ 2005 Redistributable Package (x86)</ProductName>
    </ProductInfo>
    <ProductInfo type="required">
      <Family>NonAdobePayload</Family>
      <ProductName>Microsoft Visual C++ 2008 Redistributable Package (x86)</ProductName>
    </ProductInfo>
  </Dependencies><Upgrades>
    <AdobeCode>{84883474-159A-4CBC-97C7-505D6D221866}</AdobeCode>
    <AdobeCode>{12505BAC-ED5F-4813-8327-AE54F008882D}</AdobeCode>
    <AdobeCode>{9A46650B-6B78-472B-83B6-997E8E7266E5}</AdobeCode>
    <AdobeCode>{304F86EA-42E9-4CE3-9458-2214DEA3C540}</AdobeCode>
    <AdobeCode>{8A7965C0-7D26-4943-99A8-C334AA25A559}</AdobeCode>
    <AdobeCode>{EBD9E02F-FBF2-4E82-B0B6-56A1A46C3267}</AdobeCode>
    <AdobeCode>{4613309A-DEAE-4D81-92E2-69D684B22AC4}</AdobeCode>
    <AdobeCode>{3C9BE3D6-3DC0-4756-80CB-37DCFE4698BB}</AdobeCode>
    <AdobeCode>{847D5939-0E39-43C0-9D3A-BB729A11D398}</AdobeCode>
    <AdobeCode>{AB74C2BB-BA3F-482A-AF3D-DBA2016347DD}</AdobeCode>
    <AdobeCode>{48416FC2-9949-4AB3-8312-FA828DCD272B}</AdobeCode>
    <AdobeCode>{093283A9-230D-4566-95A7-561261BA419D}</AdobeCode>
    <AdobeCode>{5BBC27D9-2634-4E5C-B351-77363901AAB4}</AdobeCode>
    <AdobeCode>{C845E6EF-316C-4B5D-8468-E7F506604887}</AdobeCode>
    <AdobeCode>{569FE4EC-A005-40EC-B887-1F275EED770D}</AdobeCode>
    <AdobeCode>{CDBE0C6D-18AF-4F0B-ADF5-07F0CE6DBBDC}</AdobeCode>
    <AdobeCode>{E929FB30-9AEF-4455-B9CE-552D60F821E3}</AdobeCode>
    <AdobeCode>{5A10BFDD-C787-47CA-88F9-9019955EB7DA}</AdobeCode>
    <AdobeCode>{394CCE73-AD4B-480F-A0E6-FDC4CFB4E926}</AdobeCode>
    <AdobeCode>{B5D1C1C6-268C-4513-A6A8-BE678AAC1AA3}</AdobeCode>
    <AdobeCode>{490E441B-18E6-4A1A-AD78-0EADC4C9E812}</AdobeCode>
    <AdobeCode>{5D9013E7-39F9-4732-A413-69EF4CDA0757}</AdobeCode>
    <AdobeCode>{1EDA6530-4384-49EF-AD6D-573AD78EFC6C}</AdobeCode>
    <AdobeCode>{DC622AAB-CE9F-4D48-9629-F554EC03755B}</AdobeCode>
    <AdobeCode>{0A10675F-9C21-47B7-94F2-3102F4D291A1}</AdobeCode>
    <AdobeCode>{45936C68-C7CD-4481-A686-C51AF205AF7A}</AdobeCode>
    <AdobeCode>{9972FA59-A5AD-4662-8C1B-956B3A10746F}</AdobeCode>
    <AdobeCode>{13EC728D-F478-4347-B493-727E5F3B6225}</AdobeCode>
    <AdobeCode>{79C67047-6E6C-4205-A681-70F3EAE6DFF8}</AdobeCode>
    <AdobeCode>{0F9DF6CB-F5AF-4822-8B02-116024F33647}</AdobeCode>
    <AdobeCode>{9055761D-59A3-408F-80B5-E0B48C0F248A}</AdobeCode>
    <AdobeCode>{E24E3517-D442-4F4F-AD1C-F015A8299FAC}</AdobeCode>
    <AdobeCode>{D99DBF25-C129-4E11-A4BF-280F6706DD8F}</AdobeCode>
    <AdobeCode>{C71057DD-546E-468A-94F0-A6B97EDF2FAD}</AdobeCode>
    <AdobeCode>{46DD2DB6-9114-4C80-B6DB-7A074512A644}</AdobeCode>
    <AdobeCode>{79403DC6-022D-499A-B29A-D985379FC549}</AdobeCode>
    <AdobeCode>{C41EC6D1-890A-4A49-B196-81E951E1DAC3}</AdobeCode>
    <AdobeCode>{91DE27C3-330A-4A7C-82BE-28DEBDD42EA6}</AdobeCode>
    <AdobeCode>{2FF85DFB-8327-431E-B682-B439285BC71B}</AdobeCode>
    <AdobeCode>{D7239BAF-F96A-46A5-AD5D-93E598F09BEA}</AdobeCode>
    <AdobeCode>{6D904447-991B-41E4-9BDA-D82839726F74}</AdobeCode>
    <AdobeCode>{163B0BAA-D144-432D-AAF3-66AEDBB6F0AD}</AdobeCode>
    <AdobeCode>{06BDD292-5B1C-4DFF-A350-D9EC7471399A}</AdobeCode>
    <AdobeCode>{CC1F4CFD-B7B6-4870-A586-E476BD3CA33C}</AdobeCode>
    <AdobeCode>{98BFBF74-AE20-4EED-AD3F-161CF827F083}</AdobeCode>
    <AdobeCode>{DB5D65D2-5EB9-4A07-A2B2-876D7413D7AE}</AdobeCode>
    <AdobeCode>{B5133D18-92E1-48C2-8B81-DA845DB900B9}</AdobeCode>
    <AdobeCode>{8A52E3E3-2719-42AD-AF66-4C3AE0581B52}</AdobeCode>
    <AdobeCode>{489BFD02-BD36-4746-B071-D53F3DEEDB2F}</AdobeCode>
    <AdobeCode>{FABC0758-7854-456E-A867-BC29CA90671B}</AdobeCode>
    <AdobeCode>{D3BD2315-1A2C-4823-9378-DD681CB84C12}</AdobeCode>
    <AdobeCode>{D883F5C4-5544-46ED-ABA0-C852C0378FFC}</AdobeCode>
    <AdobeCode>{45C08B1A-9947-4AA5-BDC0-2C23133733FD}</AdobeCode>
    <AdobeCode>{34B21707-FA81-4D2C-9ACB-0D3035297EC8}</AdobeCode>
    <AdobeCode>{AA3842C0-C440-4FA0-9B21-836FA00B0C59}</AdobeCode>
    <AdobeCode>{5E21D862-2420-4B80-AB26-6A51DB2184D0}</AdobeCode>
    <AdobeCode>{E787A200-713A-4365-AE82-DC466CDBE3F8}</AdobeCode>
    <AdobeCode>{04927242-CB98-417B-B412-BF8476B994BC}</AdobeCode>
    <AdobeCode>{C601AC54-8AC1-4A9A-837B-060D024B020A}</AdobeCode>
    <AdobeCode>{DFD9D930-9759-40BE-8E09-6EBD4794F196}</AdobeCode>
    <AdobeCode>{A79B9DB2-B8D8-4F94-A9D3-EBDDC92E3B5F}</AdobeCode>
    <AdobeCode>{50AC2B57-DF60-4D93-A35E-9EB5613ADB19}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeMiniBridgeCS6-2">
    <DisplayName>Adobe Mini Bridge CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="3789231" sysDriveSize="0"><Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>3789231</TotalSize>
      <MaxPathComponent>PL_MiniBridgePanel_N_D.png</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/CS6ServiceManager/extensions/Adobe Mini Bridge CS6</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="1" name="Assets1_1" size="3789231"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">2</Value>
      <Value lang="be_BY">2</Value>
      <Value lang="bg_BG">2</Value>
      <Value lang="ca_ES">2</Value>
      <Value lang="cs_CZ">2</Value>
      <Value lang="da_DK">2</Value>
      <Value lang="de_DE">2</Value>
      <Value lang="el_GR">2</Value>
      <Value lang="en_GB">2</Value>
      <Value lang="en_MX">2</Value>
      <Value lang="en_US">2</Value>
      <Value lang="en_XC">2</Value>
      <Value lang="en_XM">2</Value>
      <Value lang="es_ES">2</Value>
      <Value lang="es_MX">2</Value>
      <Value lang="es_QM">2</Value>
      <Value lang="et_EE">2</Value>
      <Value lang="fi_FI">2</Value>
      <Value lang="fr_CA">2</Value>
      <Value lang="fr_FR">2</Value>
      <Value lang="fr_MX">2</Value>
      <Value lang="fr_XM">2</Value>
      <Value lang="he_IL">2</Value>
      <Value lang="hi_IN">2</Value>
      <Value lang="hr_HR">2</Value>
      <Value lang="hu_HU">2</Value>
      <Value lang="is_IS">2</Value>
      <Value lang="it_IT">2</Value>
      <Value lang="ja_JP">2</Value>
      <Value lang="ko_KR">2</Value>
      <Value lang="lt_LT">2</Value>
      <Value lang="lv_LV">2</Value>
      <Value lang="mk_MK">2</Value>
      <Value lang="nb_NO">2</Value>
      <Value lang="nl_NL">2</Value>
      <Value lang="nn_NO">2</Value>
      <Value lang="no_NO">2</Value>
      <Value lang="pl_PL">2</Value>
      <Value lang="pt_BR">2</Value>
      <Value lang="ro_RO">2</Value>
      <Value lang="ru_RU">2</Value>
      <Value lang="sh_YU">2</Value>
      <Value lang="sk_SK">2</Value>
      <Value lang="sl_SI">2</Value>
      <Value lang="sq_AL">2</Value>
      <Value lang="sv_SE">2</Value>
      <Value lang="th_TH">2</Value>
      <Value lang="tr_TR">2</Value>
      <Value lang="uk_UA">2</Value>
      <Value lang="vi_VN">2</Value>
      <Value lang="zh_CN">2</Value>
      <Value lang="zh_TW">2</Value>
      <Value lang="en_AE">2</Value>
      <Value lang="en_IL">2</Value>
      <Value lang="fr_MA">2</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Mini Bridge CS6</Value>
      <Value lang="be_BY">Adobe Mini Bridge CS6</Value>
      <Value lang="bg_BG">Adobe Mini Bridge CS6</Value>
      <Value lang="ca_ES">Adobe Mini Bridge CS6</Value>
      <Value lang="cs_CZ">Adobe Mini Bridge CS6</Value>
      <Value lang="da_DK">Adobe Mini Bridge CS6</Value>
      <Value lang="de_DE">Adobe Mini Bridge CS6</Value>
      <Value lang="el_GR">Adobe Mini Bridge CS6</Value>
      <Value lang="en_GB">Adobe Mini Bridge CS6</Value>
      <Value lang="en_MX">Adobe Mini Bridge CS6</Value>
      <Value lang="en_US">Adobe Mini Bridge CS6</Value>
      <Value lang="en_XC">Adobe Mini Bridge CS6</Value>
      <Value lang="en_XM">Adobe Mini Bridge CS6</Value>
      <Value lang="es_ES">Adobe Mini Bridge CS6</Value>
      <Value lang="es_MX">Adobe Mini Bridge CS6</Value>
      <Value lang="es_QM">Adobe Mini Bridge CS6</Value>
      <Value lang="et_EE">Adobe Mini Bridge CS6</Value>
      <Value lang="fi_FI">Adobe Mini Bridge CS6</Value>
      <Value lang="fr_CA">Adobe Mini Bridge CS6</Value>
      <Value lang="fr_FR">Adobe Mini Bridge CS6</Value>
      <Value lang="fr_MX">Adobe Mini Bridge CS6</Value>
      <Value lang="fr_XM">Adobe Mini Bridge CS6</Value>
      <Value lang="he_IL">Adobe Mini Bridge CS6</Value>
      <Value lang="hi_IN">Adobe Mini Bridge CS6</Value>
      <Value lang="hr_HR">Adobe Mini Bridge CS6</Value>
      <Value lang="hu_HU">Adobe Mini Bridge CS6</Value>
      <Value lang="is_IS">Adobe Mini Bridge CS6</Value>
      <Value lang="it_IT">Adobe Mini Bridge CS6</Value>
      <Value lang="ja_JP">Adobe Mini Bridge CS6</Value>
      <Value lang="ko_KR">Adobe Mini Bridge CS6</Value>
      <Value lang="lt_LT">Adobe Mini Bridge CS6</Value>
      <Value lang="lv_LV">Adobe Mini Bridge CS6</Value>
      <Value lang="mk_MK">Adobe Mini Bridge CS6</Value>
      <Value lang="nb_NO">Adobe Mini Bridge CS6</Value>
      <Value lang="nl_NL">Adobe Mini Bridge CS6</Value>
      <Value lang="nn_NO">Adobe Mini Bridge CS6</Value>
      <Value lang="no_NO">Adobe Mini Bridge CS6</Value>
      <Value lang="pl_PL">Adobe Mini Bridge CS6</Value>
      <Value lang="pt_BR">Adobe Mini Bridge CS6</Value>
      <Value lang="ro_RO">Adobe Mini Bridge CS6</Value>
      <Value lang="ru_RU">Adobe Mini Bridge CS6</Value>
      <Value lang="sh_YU">Adobe Mini Bridge CS6</Value>
      <Value lang="sk_SK">Adobe Mini Bridge CS6</Value>
      <Value lang="sl_SI">Adobe Mini Bridge CS6</Value>
      <Value lang="sq_AL">Adobe Mini Bridge CS6</Value>
      <Value lang="sv_SE">Adobe Mini Bridge CS6</Value>
      <Value lang="th_TH">Adobe Mini Bridge CS6</Value>
      <Value lang="tr_TR">Adobe Mini Bridge CS6</Value>
      <Value lang="uk_UA">Adobe Mini Bridge CS6</Value>
      <Value lang="vi_VN">Adobe Mini Bridge CS6</Value>
      <Value lang="zh_CN">Adobe Mini Bridge CS6</Value>
      <Value lang="zh_TW">Adobe Mini Bridge CS6</Value>
      <Value lang="en_AE">Adobe Mini Bridge CS6</Value>
      <Value lang="en_IL">Adobe Mini Bridge CS6</Value>
      <Value lang="fr_MA">Adobe Mini Bridge CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0", "ChannelID", "AdobeMiniBridgeCS6-2")
INSERT INTO PayloadData VALUES("{B98E0DCB-1A75-4394-9813-3A114AA3891D}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeMiniBridgeCS6-2">
    <DisplayName>Adobe Mini Bridge CS6</DisplayName>
  </Channel>')
