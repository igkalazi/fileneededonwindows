CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{5DD50298-5050-4B64-A18B-68EA335FABA5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{F1AF5308-D0E4-4A5C-87CF-7D14F816E106}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{C989664D-98FA-4F1A-823C-55258DC9E32C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{66F947D1-4BBA-4982-9065-A9D703EAF444}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{94488F77-3A5A-4950-81FC-325AA0248734}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{B34AD8D9-733C-4EFA-90E2-BFEC67D40CCC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{03FDAE87-9846-4224-837E-3DF481103B7C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{631B9C79-C45A-4612-8CA4-1AB43D8B2188}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{532F678F-7E2A-45F2-B6F1-DB81710F5571}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{2AB8C652-3EFB-4DB2-B4BD-4ECD79130E15}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{286C7148-879A-404E-AF21-46202B00B9F5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{03DA664F-6397-4AC3-AA39-0CE5A7514748}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{3B4E876A-8069-427E-880C-4EEC38C50AE9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "{DFF412B5-345C-4E4C-8889-83A207B0B8A8}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}"/>')
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0" , "ValidationSig", "WnTB+3qbQ26as3LvZRq1K0N2mH+lkRUkj5ndz5UYBmKEbwUFOeCvOEeyCXDfnyiVG/NU7hN42FxYI2CWuV6P7AxhiH/xN1U/g3DWOdfuZNCv7spbkoEoOVJxyVwGUFaiiIvUKEmRaCJ52jfTZCw/eimZiUl/4wo/rior/qDKPc7Va2FWJWWGL+p594hkQULZ5e0FkUeSJc8RhY0zk77f6Ox29gKJ2oFgvULg3zQCXdqzsnHw0t7UH9wiuwLrTNne7/bAI2Q8fSoGm8uZNgOp8bRnQi0vOtfig2W/E/Mg/f54VYD+vqCMAp2GXSejDL6anUFof7cIsC1S5+ny2/NjEOjlwmS9qdv8+gV1ovWLMPU/MdgTQcWVgU/MFO8FBXKpuKQZO8Yuvl6xQXOacZcEjXyervRfkVfcqjWFkynAAjudX2HgnhprFNQwro2sdJcOFq8OLTWrHWHwk8/GLcOH1XdoQ9AWgI3IaSx10UuJBWXTVgBgmYEK/tozEUfUcoK/")
INSERT INTO Payloads VALUES	("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "CoreTech", "AdobeTypeSupport CS6", "11.0", "normal")
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-11-30 13:32:27.365000</Property>
    <Property name="TargetName">AdobeTypeSupport11-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}</Property>
    <Property name="ProductName">Adobe Type Support CS6</Property>
    <Property name="ProductVersion">11.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\TypeSupport\CS6</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeTypeSupport CS6</ProductName>
	<ProductVersion>11.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{5DD50298-5050-4B64-A18B-68EA335FABA5}</AdobeCode>
    <AdobeCode>{F1AF5308-D0E4-4A5C-87CF-7D14F816E106}</AdobeCode>
    <AdobeCode>{C989664D-98FA-4F1A-823C-55258DC9E32C}</AdobeCode>
    <AdobeCode>{66F947D1-4BBA-4982-9065-A9D703EAF444}</AdobeCode>
    <AdobeCode>{94488F77-3A5A-4950-81FC-325AA0248734}</AdobeCode>
    <AdobeCode>{B34AD8D9-733C-4EFA-90E2-BFEC67D40CCC}</AdobeCode>
    <AdobeCode>{03FDAE87-9846-4224-837E-3DF481103B7C}</AdobeCode>
    <AdobeCode>{631B9C79-C45A-4612-8CA4-1AB43D8B2188}</AdobeCode>
    <AdobeCode>{532F678F-7E2A-45F2-B6F1-DB81710F5571}</AdobeCode>
    <AdobeCode>{2AB8C652-3EFB-4DB2-B4BD-4ECD79130E15}</AdobeCode>
    <AdobeCode>{286C7148-879A-404E-AF21-46202B00B9F5}</AdobeCode>
    <AdobeCode>{03DA664F-6397-4AC3-AA39-0CE5A7514748}</AdobeCode>
    <AdobeCode>{3B4E876A-8069-427E-880C-4EEC38C50AE9}</AdobeCode>
    <AdobeCode>{DFF412B5-345C-4E4C-8889-83A207B0B8A8}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeTypeSupportCS6-11.0">
    <DisplayName>Adobe Type Support CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="5736902"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>5736902</TotalSize>
      <MaxPathComponent>/TypeSupport/CS6/Unicode/Mappings/Adobe\Japanese83pv.txt</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="5736902"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">11.0</Value>
      <Value lang="be_BY">11.0</Value>
      <Value lang="bg_BG">11.0</Value>
      <Value lang="ca_ES">11.0</Value>
      <Value lang="cs_CZ">11.0</Value>
      <Value lang="da_DK">11.0</Value>
      <Value lang="de_DE">11.0</Value>
      <Value lang="el_GR">11.0</Value>
      <Value lang="en_GB">11.0</Value>
      <Value lang="en_MX">11.0</Value>
      <Value lang="en_US">11.0</Value>
      <Value lang="en_XC">11.0</Value>
      <Value lang="en_XM">11.0</Value>
      <Value lang="es_ES">11.0</Value>
      <Value lang="es_MX">11.0</Value>
      <Value lang="es_QM">11.0</Value>
      <Value lang="et_EE">11.0</Value>
      <Value lang="fi_FI">11.0</Value>
      <Value lang="fr_CA">11.0</Value>
      <Value lang="fr_FR">11.0</Value>
      <Value lang="fr_MX">11.0</Value>
      <Value lang="fr_XM">11.0</Value>
      <Value lang="he_IL">11.0</Value>
      <Value lang="hi_IN">11.0</Value>
      <Value lang="hr_HR">11.0</Value>
      <Value lang="hu_HU">11.0</Value>
      <Value lang="is_IS">11.0</Value>
      <Value lang="it_IT">11.0</Value>
      <Value lang="ja_JP">11.0</Value>
      <Value lang="ko_KR">11.0</Value>
      <Value lang="lt_LT">11.0</Value>
      <Value lang="lv_LV">11.0</Value>
      <Value lang="mk_MK">11.0</Value>
      <Value lang="nb_NO">11.0</Value>
      <Value lang="nl_NL">11.0</Value>
      <Value lang="nn_NO">11.0</Value>
      <Value lang="no_NO">11.0</Value>
      <Value lang="pl_PL">11.0</Value>
      <Value lang="pt_BR">11.0</Value>
      <Value lang="ro_RO">11.0</Value>
      <Value lang="ru_RU">11.0</Value>
      <Value lang="sh_YU">11.0</Value>
      <Value lang="sk_SK">11.0</Value>
      <Value lang="sl_SI">11.0</Value>
      <Value lang="sq_AL">11.0</Value>
      <Value lang="sv_SE">11.0</Value>
      <Value lang="th_TH">11.0</Value>
      <Value lang="tr_TR">11.0</Value>
      <Value lang="uk_UA">11.0</Value>
      <Value lang="vi_VN">11.0</Value>
      <Value lang="zh_CN">11.0</Value>
      <Value lang="zh_TW">11.0</Value>
      <Value lang="en_AE">11.0</Value>
      <Value lang="en_IL">11.0</Value>
      <Value lang="fr_MA">11.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Type Support CS6</Value>
      <Value lang="be_BY">Adobe Type Support CS6</Value>
      <Value lang="bg_BG">Adobe Type Support CS6</Value>
      <Value lang="ca_ES">Adobe Type Support CS6</Value>
      <Value lang="cs_CZ">Adobe Type Support CS6</Value>
      <Value lang="da_DK">Adobe Type Support CS6</Value>
      <Value lang="de_DE">Adobe Type Support CS6</Value>
      <Value lang="el_GR">Adobe Type Support CS6</Value>
      <Value lang="en_GB">Adobe Type Support CS6</Value>
      <Value lang="en_MX">Adobe Type Support CS6</Value>
      <Value lang="en_US">Adobe Type Support CS6</Value>
      <Value lang="en_XC">Adobe Type Support CS6</Value>
      <Value lang="en_XM">Adobe Type Support CS6</Value>
      <Value lang="es_ES">Adobe Type Support CS6</Value>
      <Value lang="es_MX">Adobe Type Support CS6</Value>
      <Value lang="es_QM">Adobe Type Support CS6</Value>
      <Value lang="et_EE">Adobe Type Support CS6</Value>
      <Value lang="fi_FI">Adobe Type Support CS6</Value>
      <Value lang="fr_CA">Adobe Type Support CS6</Value>
      <Value lang="fr_FR">Adobe Type Support CS6</Value>
      <Value lang="fr_MX">Adobe Type Support CS6</Value>
      <Value lang="fr_XM">Adobe Type Support CS6</Value>
      <Value lang="he_IL">Adobe Type Support CS6</Value>
      <Value lang="hi_IN">Adobe Type Support CS6</Value>
      <Value lang="hr_HR">Adobe Type Support CS6</Value>
      <Value lang="hu_HU">Adobe Type Support CS6</Value>
      <Value lang="is_IS">Adobe Type Support CS6</Value>
      <Value lang="it_IT">Adobe Type Support CS6</Value>
      <Value lang="ja_JP">Adobe Type Support CS6</Value>
      <Value lang="ko_KR">Adobe Type Support CS6</Value>
      <Value lang="lt_LT">Adobe Type Support CS6</Value>
      <Value lang="lv_LV">Adobe Type Support CS6</Value>
      <Value lang="mk_MK">Adobe Type Support CS6</Value>
      <Value lang="nb_NO">Adobe Type Support CS6</Value>
      <Value lang="nl_NL">Adobe Type Support CS6</Value>
      <Value lang="nn_NO">Adobe Type Support CS6</Value>
      <Value lang="no_NO">Adobe Type Support CS6</Value>
      <Value lang="pl_PL">Adobe Type Support CS6</Value>
      <Value lang="pt_BR">Adobe Type Support CS6</Value>
      <Value lang="ro_RO">Adobe Type Support CS6</Value>
      <Value lang="ru_RU">Adobe Type Support CS6</Value>
      <Value lang="sh_YU">Adobe Type Support CS6</Value>
      <Value lang="sk_SK">Adobe Type Support CS6</Value>
      <Value lang="sl_SI">Adobe Type Support CS6</Value>
      <Value lang="sq_AL">Adobe Type Support CS6</Value>
      <Value lang="sv_SE">Adobe Type Support CS6</Value>
      <Value lang="th_TH">Adobe Type Support CS6</Value>
      <Value lang="tr_TR">Adobe Type Support CS6</Value>
      <Value lang="uk_UA">Adobe Type Support CS6</Value>
      <Value lang="vi_VN">Adobe Type Support CS6</Value>
      <Value lang="zh_CN">Adobe Type Support CS6</Value>
      <Value lang="zh_TW">Adobe Type Support CS6</Value>
      <Value lang="en_AE">Adobe Type Support CS6</Value>
      <Value lang="en_IL">Adobe Type Support CS6</Value>
      <Value lang="fr_MA">Adobe Type Support CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0", "ChannelID", "AdobeTypeSupportCS6-11.0")
INSERT INTO PayloadData VALUES("{A0F72081-99FB-4FFA-AE1A-62B5A656CAC1}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeTypeSupportCS6-11.0">
    <DisplayName>Adobe Type Support CS6</DisplayName>
  </Channel>')
