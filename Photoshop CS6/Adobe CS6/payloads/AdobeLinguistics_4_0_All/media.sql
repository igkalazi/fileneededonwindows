CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{E9EA1334-5A34-4745-9F85-84EE37362C8E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{DC9C77CA-78A6-41df-84E3-E04A2ADFACA0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{5FFBEC31-5110-425B-A49B-01D0C48CCA88}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{1D3E4503-800C-4434-A98E-31CF4C4016FC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{7EFA481F-26FC-45B6-9BBA-A6D45975A016}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{4935FE50-E21E-4D88-B007-59B83004D088}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{084690FC-9FB1-4C59-AED0-CE3A588E86E9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{DD7DA9B6-AFA2-4EE7-A8A6-272D4B054F16}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{4FD5E212-727B-47AC-BBB9-15CE36262576}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{A862C192-DB3B-4963-BF56-6E5CF4805DA4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{E402AC06-AF6F-4C22-BEC1-6E345DC93C64}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{E77992CE-EEA5-4641-9FEB-503C3E728217}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{F23BA6A2-E3C7-477D-91DB-CCB780F14F9E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{0DE9B375-DEAE-48A0-8E5B-30D3F0A44F2D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{5959AA56-47A6-4034-945A-B0C4DEC28721}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "{57D57BEE-9153-45FA-80AE-B95FD3ECC8E7}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\UserDictionaryMigrationTool\UDMT.exe</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\UserDictionaryMigrationTool\AdobeLinguistic.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0" , "ValidationSig", "PjGm5KAqQf0CHjpsyW8Cz/h2oF57JKA+h0LEo2tb1q6MM8Mkq3ljxFpiZ3QsGSBVyYHMJiFd681Vt4MP1Qez+zxx+El4FcRyaYpp2t1XigdN+TzRSpLBVfYj1KHupdxcfJ2c0FyZy8GZQgmPzYl2SsrpHzpZNl75kRE9qou7gSvJtCEhUAPMdXCdhh6YoapMNUxRXFEZxuPwcrlPgcijlZMZTW0vIaOuyYrb6uFdfeJVa075c/BhjE+cMjffg+xPy6Uzl7uZuy3/ibWmVf7xrps8lFMGrde/580OdJOLEImxxbtIghgRgl+cCuSaUbzmD1VjirHvUkGN61gPuUqsU+9lgdx0qJR6duK/gaLeyDv//wPZk7zNXY27TV0BZdNeuAoBuj2rSiCUD5mWtLPlvsWBvEzNl2KjMrlt2+PBjZS7bFsxL5u8jbNf2U1G2Sy2yVdTh0P6iwcDtGizcz+ekZ3qTy5tOhQBz4tyVHyJKhYL8q8qTdWSO9J7xwEwS22Y")
INSERT INTO Payloads VALUES	("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "CoreTech", "Adobe Linguistics CS6", "6.0.0", "normal")
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-12-19 16:32:13.441000</Property>
    <Property name="TargetName">AdobeLinguistics_4_0_All</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}</Property>
    <Property name="ProductName">Adobe Linguistics CS6</Property>
    <Property name="ProductVersion">6.0.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Linguistics CS6</ProductName>
	<ProductVersion>6.0.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{E9EA1334-5A34-4745-9F85-84EE37362C8E}</AdobeCode>
    <AdobeCode>{DC9C77CA-78A6-41df-84E3-E04A2ADFACA0}</AdobeCode>
    <AdobeCode>{5FFBEC31-5110-425B-A49B-01D0C48CCA88}</AdobeCode>
    <AdobeCode>{1D3E4503-800C-4434-A98E-31CF4C4016FC}</AdobeCode>
    <AdobeCode>{7EFA481F-26FC-45B6-9BBA-A6D45975A016}</AdobeCode>
    <AdobeCode>{4935FE50-E21E-4D88-B007-59B83004D088}</AdobeCode>
    <AdobeCode>{084690FC-9FB1-4C59-AED0-CE3A588E86E9}</AdobeCode>
    <AdobeCode>{DD7DA9B6-AFA2-4EE7-A8A6-272D4B054F16}</AdobeCode>
    <AdobeCode>{4FD5E212-727B-47AC-BBB9-15CE36262576}</AdobeCode>
    <AdobeCode>{A862C192-DB3B-4963-BF56-6E5CF4805DA4}</AdobeCode>
    <AdobeCode>{E402AC06-AF6F-4C22-BEC1-6E345DC93C64}</AdobeCode>
    <AdobeCode>{E77992CE-EEA5-4641-9FEB-503C3E728217}</AdobeCode>
    <AdobeCode>{F23BA6A2-E3C7-477D-91DB-CCB780F14F9E}</AdobeCode>
    <AdobeCode>{0DE9B375-DEAE-48A0-8E5B-30D3F0A44F2D}</AdobeCode>
    <AdobeCode>{5959AA56-47A6-4034-945A-B0C4DEC28721}</AdobeCode>
    <AdobeCode>{57D57BEE-9153-45FA-80AE-B95FD3ECC8E7}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeLinguisticsCS6-6.0.0">
    <DisplayName>Adobe Linguistics CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="236" sysDriveSize="43609376"><Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>236</TotalSize>
      <MaxPathComponent>/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>43609376</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Proximity/11.00/NC-L3S4R8PNTX\grm190120050401phon.env</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="43609376"/>
      <Asset flag="1" name="Assets1_1" size="236"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">6.0.0</Value>
      <Value lang="be_BY">6.0.0</Value>
      <Value lang="bg_BG">6.0.0</Value>
      <Value lang="ca_ES">6.0.0</Value>
      <Value lang="cs_CZ">6.0.0</Value>
      <Value lang="da_DK">6.0.0</Value>
      <Value lang="de_DE">6.0.0</Value>
      <Value lang="el_GR">6.0.0</Value>
      <Value lang="en_GB">6.0.0</Value>
      <Value lang="en_MX">6.0.0</Value>
      <Value lang="en_US">6.0.0</Value>
      <Value lang="en_XC">6.0.0</Value>
      <Value lang="en_XM">6.0.0</Value>
      <Value lang="es_ES">6.0.0</Value>
      <Value lang="es_MX">6.0.0</Value>
      <Value lang="es_QM">6.0.0</Value>
      <Value lang="et_EE">6.0.0</Value>
      <Value lang="fi_FI">6.0.0</Value>
      <Value lang="fr_CA">6.0.0</Value>
      <Value lang="fr_FR">6.0.0</Value>
      <Value lang="fr_MX">6.0.0</Value>
      <Value lang="fr_XM">6.0.0</Value>
      <Value lang="he_IL">6.0.0</Value>
      <Value lang="hi_IN">6.0.0</Value>
      <Value lang="hr_HR">6.0.0</Value>
      <Value lang="hu_HU">6.0.0</Value>
      <Value lang="is_IS">6.0.0</Value>
      <Value lang="it_IT">6.0.0</Value>
      <Value lang="ja_JP">6.0.0</Value>
      <Value lang="ko_KR">6.0.0</Value>
      <Value lang="lt_LT">6.0.0</Value>
      <Value lang="lv_LV">6.0.0</Value>
      <Value lang="mk_MK">6.0.0</Value>
      <Value lang="nb_NO">6.0.0</Value>
      <Value lang="nl_NL">6.0.0</Value>
      <Value lang="nn_NO">6.0.0</Value>
      <Value lang="no_NO">6.0.0</Value>
      <Value lang="pl_PL">6.0.0</Value>
      <Value lang="pt_BR">6.0.0</Value>
      <Value lang="ro_RO">6.0.0</Value>
      <Value lang="ru_RU">6.0.0</Value>
      <Value lang="sh_YU">6.0.0</Value>
      <Value lang="sk_SK">6.0.0</Value>
      <Value lang="sl_SI">6.0.0</Value>
      <Value lang="sq_AL">6.0.0</Value>
      <Value lang="sv_SE">6.0.0</Value>
      <Value lang="th_TH">6.0.0</Value>
      <Value lang="tr_TR">6.0.0</Value>
      <Value lang="uk_UA">6.0.0</Value>
      <Value lang="vi_VN">6.0.0</Value>
      <Value lang="zh_CN">6.0.0</Value>
      <Value lang="zh_TW">6.0.0</Value>
      <Value lang="en_AE">6.0.0</Value>
      <Value lang="en_IL">6.0.0</Value>
      <Value lang="fr_MA">6.0.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Linguistics CS6</Value>
      <Value lang="be_BY">Adobe Linguistics CS6</Value>
      <Value lang="bg_BG">Adobe Linguistics CS6</Value>
      <Value lang="ca_ES">Adobe Linguistics CS6</Value>
      <Value lang="cs_CZ">Adobe Linguistics CS6</Value>
      <Value lang="da_DK">Adobe Linguistics CS6</Value>
      <Value lang="de_DE">Adobe Linguistics CS6</Value>
      <Value lang="el_GR">Adobe Linguistics CS6</Value>
      <Value lang="en_GB">Adobe Linguistics CS6</Value>
      <Value lang="en_MX">Adobe Linguistics CS6</Value>
      <Value lang="en_US">Adobe Linguistics CS6</Value>
      <Value lang="en_XC">Adobe Linguistics CS6</Value>
      <Value lang="en_XM">Adobe Linguistics CS6</Value>
      <Value lang="es_ES">Adobe Linguistics CS6</Value>
      <Value lang="es_MX">Adobe Linguistics CS6</Value>
      <Value lang="es_QM">Adobe Linguistics CS6</Value>
      <Value lang="et_EE">Adobe Linguistics CS6</Value>
      <Value lang="fi_FI">Adobe Linguistics CS6</Value>
      <Value lang="fr_CA">Adobe Linguistics CS6</Value>
      <Value lang="fr_FR">Adobe Linguistics CS6</Value>
      <Value lang="fr_MX">Adobe Linguistics CS6</Value>
      <Value lang="fr_XM">Adobe Linguistics CS6</Value>
      <Value lang="he_IL">Adobe Linguistics CS6</Value>
      <Value lang="hi_IN">Adobe Linguistics CS6</Value>
      <Value lang="hr_HR">Adobe Linguistics CS6</Value>
      <Value lang="hu_HU">Adobe Linguistics CS6</Value>
      <Value lang="is_IS">Adobe Linguistics CS6</Value>
      <Value lang="it_IT">Adobe Linguistics CS6</Value>
      <Value lang="ja_JP">Adobe Linguistics CS6</Value>
      <Value lang="ko_KR">Adobe Linguistics CS6</Value>
      <Value lang="lt_LT">Adobe Linguistics CS6</Value>
      <Value lang="lv_LV">Adobe Linguistics CS6</Value>
      <Value lang="mk_MK">Adobe Linguistics CS6</Value>
      <Value lang="nb_NO">Adobe Linguistics CS6</Value>
      <Value lang="nl_NL">Adobe Linguistics CS6</Value>
      <Value lang="nn_NO">Adobe Linguistics CS6</Value>
      <Value lang="no_NO">Adobe Linguistics CS6</Value>
      <Value lang="pl_PL">Adobe Linguistics CS6</Value>
      <Value lang="pt_BR">Adobe Linguistics CS6</Value>
      <Value lang="ro_RO">Adobe Linguistics CS6</Value>
      <Value lang="ru_RU">Adobe Linguistics CS6</Value>
      <Value lang="sh_YU">Adobe Linguistics CS6</Value>
      <Value lang="sk_SK">Adobe Linguistics CS6</Value>
      <Value lang="sl_SI">Adobe Linguistics CS6</Value>
      <Value lang="sq_AL">Adobe Linguistics CS6</Value>
      <Value lang="sv_SE">Adobe Linguistics CS6</Value>
      <Value lang="th_TH">Adobe Linguistics CS6</Value>
      <Value lang="tr_TR">Adobe Linguistics CS6</Value>
      <Value lang="uk_UA">Adobe Linguistics CS6</Value>
      <Value lang="vi_VN">Adobe Linguistics CS6</Value>
      <Value lang="zh_CN">Adobe Linguistics CS6</Value>
      <Value lang="zh_TW">Adobe Linguistics CS6</Value>
      <Value lang="en_AE">Adobe Linguistics CS6</Value>
      <Value lang="en_IL">Adobe Linguistics CS6</Value>
      <Value lang="fr_MA">Adobe Linguistics CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0", "ChannelID", "AdobeLinguisticsCS6-6.0.0")
INSERT INTO PayloadData VALUES("{DC00A3E1-9C61-4B11-8070-B592E68D2B3C}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeLinguisticsCS6-6.0.0">
    <DisplayName>Adobe Linguistics CS6</DisplayName>
  </Channel>')
