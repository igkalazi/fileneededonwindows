CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{4F450A77-FD06-4710-9A08-FF6C635B9D94}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{C94CF16C-6D4E-495c-9607-CDF2D9373DF9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{38E241CB-822D-4084-9ADA-115519DD9643}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{00C93DAD-D08E-4C6D-891C-D15BB6FC0C21}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{92676595-49CC-402F-9EBE-A48B1B6B574E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{A9AF2471-F407-4349-9B7F-3DBC6E4D5314}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{A1E46ACE-2BE2-4C77-B1D1-5E77EBD32F55}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{C11AB14F-1D9A-4FF5-8A93-D689FA6C8294}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{3D4BAC1A-5F39-4FAD-9913-7689981E8DC4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{A588BC9D-FFC0-47E5-B5D5-6CD95A5A563B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{3B468353-ADFF-4147-AE2E-A444719A5E8E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{6183156F-50C4-42EC-96B3-5BE64FD6B2A0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{7B8306D9-9B34-48F7-8152-EED8EDCFF1AB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{9EDDE5CB-079B-415C-AC6B-E141E141E510}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "{253F16AD-8104-4437-B74F-EB8AFF727742}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{7CA3FAD4-7B82-473C-8207-5A283E90742A}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\CNSWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\CSWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\MORPHLibs.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\NFTWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\PACWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\WRLiloPlugin.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0" , "ValidationSig", "YUh2sXLn0R710cuD/N3GH0Sov16sAIhTN01y1HD06DZ8sPA22JYujz8dY6du1D8NrxyouzsFciMNcFnDIzrNfejQkpL5rHs78A+1h5nz6x5+uqJwPxTqPdizDl2Tt3PkZEWwW7RrG29hIzs9EpGSMBl37ZisqFkn3JEtwE6xHY2ct659nH6Q6i9nhW7q7IWxFSe9YKC5aweYN1OLLEsg8nxfy0dXsnKJHXA+dCmsw6Rcr0hUEC+Te7rKGQjejy5ZbRYBz7YNvs36mW2ygNdHx9owCD3kZQ6hTfsL3SoN3xX4E3HRL2PYhlUAEibYTCTo/uBV296dwpGAdDwvtprbk+eXLT4IvslcxxwreOeJ4MHfaehnkjWAVAs0reIKeoFQAY0pqykagwO2R//OY+vQJwNnuwJmDClWbKNesg504l8mtEAg5IeP14/bbEldtrPLSfVQHNeEnz/miUn9SBpmEJNl99hGVzny5ujMR9sKcg+YeYdVtUAfPSfI34ErZNmq")
INSERT INTO Branding VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5XaW5zb2Z0
TGluZ3Vpc3RpY3MtSW5zdGFsbGVyLUFkb2JlQ29kZTwvRGF0YT4NCiAgICA8L1BheWxvYWQ+DQo8
L0NvbmZpZ3VyYXRpb24+')
INSERT INTO Payloads VALUES	("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "CoreTech", "Adobe WinSoft Linguistics Plugin CS6 x64", "1.3", "normal")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-23 18:26:31.965000</Property>
    <Property name="TargetName">AdobeWinSoftLinguisticsPluginAll_x64</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{7CA3FAD4-7B82-473C-8207-5A283E90742A}</Property>
    <Property name="ProductName">Adobe WinSoft Linguistics Plugin CS6 x64</Property>
    <Property name="ProductVersion">1.3</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3</Platform>
    <Platform isFixed="1" name="OSX" folderName="">[AdobeCommon]/Linguistics/6.0/Providers/Plugins2/WRLiloPlugin1.3.bundle</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe WinSoft Linguistics Plugin CS6 x64</ProductName>
	<ProductVersion>1.3</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{4F450A77-FD06-4710-9A08-FF6C635B9D94}</AdobeCode>
    <AdobeCode>{C94CF16C-6D4E-495c-9607-CDF2D9373DF9}</AdobeCode>
    <AdobeCode>{38E241CB-822D-4084-9ADA-115519DD9643}</AdobeCode>
    <AdobeCode>{00C93DAD-D08E-4C6D-891C-D15BB6FC0C21}</AdobeCode>
    <AdobeCode>{92676595-49CC-402F-9EBE-A48B1B6B574E}</AdobeCode>
    <AdobeCode>{A9AF2471-F407-4349-9B7F-3DBC6E4D5314}</AdobeCode>
    <AdobeCode>{A1E46ACE-2BE2-4C77-B1D1-5E77EBD32F55}</AdobeCode>
    <AdobeCode>{C11AB14F-1D9A-4FF5-8A93-D689FA6C8294}</AdobeCode>
    <AdobeCode>{3D4BAC1A-5F39-4FAD-9913-7689981E8DC4}</AdobeCode>
    <AdobeCode>{A588BC9D-FFC0-47E5-B5D5-6CD95A5A563B}</AdobeCode>
    <AdobeCode>{3B468353-ADFF-4147-AE2E-A444719A5E8E}</AdobeCode>
    <AdobeCode>{6183156F-50C4-42EC-96B3-5BE64FD6B2A0}</AdobeCode>
    <AdobeCode>{7B8306D9-9B34-48F7-8152-EED8EDCFF1AB}</AdobeCode>
    <AdobeCode>{9EDDE5CB-079B-415C-AC6B-E141E141E510}</AdobeCode>
    <AdobeCode>{253F16AD-8104-4437-B74F-EB8AFF727742}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeWinSoftLinguisticsPluginCS6x64-1.3">
    <DisplayName>Adobe WinSoft Linguistics Plugin CS6 x64</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="8066772"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>8066772</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Plugins2/WRLiloPlugin1.3/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="8066772"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">1.3</Value>
      <Value lang="be_BY">1.3</Value>
      <Value lang="bg_BG">1.3</Value>
      <Value lang="ca_ES">1.3</Value>
      <Value lang="cs_CZ">1.3</Value>
      <Value lang="da_DK">1.3</Value>
      <Value lang="de_DE">1.3</Value>
      <Value lang="el_GR">1.3</Value>
      <Value lang="en_GB">1.3</Value>
      <Value lang="en_MX">1.3</Value>
      <Value lang="en_US">1.3</Value>
      <Value lang="en_XC">1.3</Value>
      <Value lang="en_XM">1.3</Value>
      <Value lang="es_ES">1.3</Value>
      <Value lang="es_MX">1.3</Value>
      <Value lang="es_QM">1.3</Value>
      <Value lang="et_EE">1.3</Value>
      <Value lang="fi_FI">1.3</Value>
      <Value lang="fr_CA">1.3</Value>
      <Value lang="fr_FR">1.3</Value>
      <Value lang="fr_MX">1.3</Value>
      <Value lang="fr_XM">1.3</Value>
      <Value lang="he_IL">1.3</Value>
      <Value lang="hi_IN">1.3</Value>
      <Value lang="hr_HR">1.3</Value>
      <Value lang="hu_HU">1.3</Value>
      <Value lang="is_IS">1.3</Value>
      <Value lang="it_IT">1.3</Value>
      <Value lang="ja_JP">1.3</Value>
      <Value lang="ko_KR">1.3</Value>
      <Value lang="lt_LT">1.3</Value>
      <Value lang="lv_LV">1.3</Value>
      <Value lang="mk_MK">1.3</Value>
      <Value lang="nb_NO">1.3</Value>
      <Value lang="nl_NL">1.3</Value>
      <Value lang="nn_NO">1.3</Value>
      <Value lang="no_NO">1.3</Value>
      <Value lang="pl_PL">1.3</Value>
      <Value lang="pt_BR">1.3</Value>
      <Value lang="ro_RO">1.3</Value>
      <Value lang="ru_RU">1.3</Value>
      <Value lang="sh_YU">1.3</Value>
      <Value lang="sk_SK">1.3</Value>
      <Value lang="sl_SI">1.3</Value>
      <Value lang="sq_AL">1.3</Value>
      <Value lang="sv_SE">1.3</Value>
      <Value lang="th_TH">1.3</Value>
      <Value lang="tr_TR">1.3</Value>
      <Value lang="uk_UA">1.3</Value>
      <Value lang="vi_VN">1.3</Value>
      <Value lang="zh_CN">1.3</Value>
      <Value lang="zh_TW">1.3</Value>
      <Value lang="en_AE">1.3</Value>
      <Value lang="en_IL">1.3</Value>
      <Value lang="fr_MA">1.3</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="be_BY">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="bg_BG">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="ca_ES">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="cs_CZ">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="da_DK">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="de_DE">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="el_GR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_GB">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_MX">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_US">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_XC">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_XM">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="es_ES">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="es_MX">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="es_QM">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="et_EE">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fi_FI">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_CA">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_FR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_MX">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_XM">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="he_IL">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="hi_IN">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="hr_HR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="hu_HU">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="is_IS">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="it_IT">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="ja_JP">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="ko_KR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="lt_LT">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="lv_LV">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="mk_MK">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="nb_NO">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="nl_NL">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="nn_NO">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="no_NO">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="pl_PL">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="pt_BR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="ro_RO">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="ru_RU">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="sh_YU">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="sk_SK">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="sl_SI">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="sq_AL">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="sv_SE">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="th_TH">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="tr_TR">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="uk_UA">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="vi_VN">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="zh_CN">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="zh_TW">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_AE">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="en_IL">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_MA">Adobe WinSoft Linguistics Plugin CS6 x64</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0", "AMTConfigPath", "[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\AMT\component.xml")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0", "ChannelID", "AdobeWinSoftLinguisticsPluginCS6x64-1.3")
INSERT INTO PayloadData VALUES("{7CA3FAD4-7B82-473C-8207-5A283E90742A}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeWinSoftLinguisticsPluginCS6x64-1.3">
    <DisplayName>Adobe WinSoft Linguistics Plugin CS6 x64</DisplayName>
  </Channel>')
