CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{9289A207-BF68-4ca8-B0DF-370FA12DC575}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{DE0E1F6C-09C8-4CC9-B57F-97EE367067AC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{32C716BF-5DC9-46AF-ACD2-8C9B6F3CF50D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{A262B72E-CAAD-41F6-B81F-56E78093E4EE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{71D0D885-03CA-4E26-9E64-BC9B97372103}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{25A619BD-3B2C-4CCA-A6DE-02D79423977F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{5517BFBA-D73A-4A06-869C-467563911608}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{F8A1BE32-3821-46C6-B793-8EF6EA2A648B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{C8015D9E-D33E-4596-9C89-EAC41AB16E64}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{14EDC309-4A1A-4765-92EB-8B2D6D8EB932}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{F8F5CF5D-1758-4A34-BC7D-1A4035834053}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{1EB57AC7-43FC-45C3-9925-203F5D927D0E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{DACE04C9-733D-4431-B122-D3D8FF5F953E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{5DDDC40E-0529-4E16-9809-4DC38264AD93}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{E4315970-7A52-4BD5-8C1A-BC9D09538F46}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{E733B1BF-9F0C-44EC-BF9E-54B61E4079F2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{19F3D2A8-18E6-406F-B5AD-F9957CE78090}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{9CF6624E-A931-48E0-931C-51858245F2B0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{52174182-F695-4AF4-B021-EB45318460F2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{D9B0663F-DA9D-43EF-8ACC-524B3EB635B1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{E266ABCF-F312-4C98-8B2B-3BCE9007501C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{97F3CEE6-639F-436E-ACCB-7D8031AAC8E9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{87EF47AC-9B12-4E6A-8894-99E847C34AA2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{E1EE3346-7DA5-4866-8450-33087A42F638}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{5590BC20-3732-4DCB-B907-971E7E939B37}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{25E9ED26-5EC6-4461-B4C5-EFA82E820E8A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{D53220D9-DF06-4C3F-8343-B94E7B5D6D8D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{448CF391-25D3-4C26-9C1C-D9163255B5B6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{C271A338-B7F2-4E8F-8DB1-A300527E9569}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{7F24D403-DA4E-42FA-AD6B-2EA534AC430F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{54875E5A-D371-4466-9A18-7E08A97F4B49}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{A022F30E-F34C-48FD-A888-DF49CD5B381F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{DA44BDD6-5144-4650-8700-B3FAEA5C702B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{7AA75B77-F5E7-4AD0-A26C-1E91B031EA4D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{373B3E73-B00F-450A-B328-7EED6DA4B4DE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{37EE03C1-DA46-4BFD-8A5C-FF2928E845C6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{5CE88ABE-1786-4AC6-92D7-918DA62D6C09}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{3B8D1B4C-7D0D-49CC-A0B8-184F4E40D89C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{A759B975-A5FE-4702-AEE4-35BAE5BDAD75}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{F633C76C-46E0-416B-BC99-3D3736579766}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{054918F3-F6E6-46F7-9763-F8C32AEF37C2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{9DC4D8CD-A337-450F-954C-6F16A01E5F65}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{683D1321-1B82-4ACA-8B49-A4ACD303E564}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "{DEED62AD-9B86-41B9-B89C-A4128CC7815A}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\\AdobeHunspellPlugin.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0" , "ValidationSig", "eqedotkqC0Qr7BX/g547MTZ8aWpap3KXtobZAe+M8XczZaEC5oqA955AGyTkzNIE4OwSGwtzp2iKh4dsinPOSgq5fq3ezZOuOepNXuse4aEvpaGE+YFKKuNh1aDVtC+00i3DNcR6HGsMIuj8BOtdfqO7VpZ3wE6lKsm0k2ElKAWiCnUpLXG05dxeOsJdnXuaiQn0d9cLWVCyAfEJQy6Lee/4lDupDj+MkpbPybtW0Kg63fDR1OLrrLD6MWtem/Rj5qSER1G8Tj+EGPP2LDZ8P71HOh8F1sjGiiNyjQpTsczwthv9qmkoupBSXEo4qbjI7odTPSnwf6ETOqp/Xg9L6VVQuYJAq6AjLwfUH2J3iFK19qi+vcj+9E9cEAuw4d/HZFWLzniQ2DcwHF08IZf3R2xXZHxqsDBfbOE90Xk26zcq4TIAktKDRaHFCbq9iZ187/MYqpO9YgiEfccY3qr85THzhrydIUY2Eab89dakOjzExLoHVCdy3QJ2cmo+nl9N")
INSERT INTO Branding VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5BZG9iZUh1
bnNwZWxsLUluc3RhbGxlci1BZG9iZUNvZGU8L0RhdGE+DQogICAgPC9QYXlsb2FkPg0KPC9Db25m
aWd1cmF0aW9uPg==')
INSERT INTO Payloads VALUES	("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "CoreTech", "Adobe Hunspell Linguistics Plugin CS6", "1.0", "normal")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-03-03 17:05:57.057000</Property>
    <Property name="TargetName">AdobeHunspellPlugin_4_0_All</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}</Property>
    <Property name="ProductName">Adobe Hunspell Linguistics Plugin CS6</Property>
    <Property name="ProductVersion">1.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin</Platform>
    <Platform isFixed="1" name="OSX" folderName="">[AdobeCommon]/Linguistics/6.0/Providers/Plugins2/AdobeHunspellPlugin.bundle</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Hunspell Linguistics Plugin CS6</ProductName>
	<ProductVersion>1.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{9289A207-BF68-4ca8-B0DF-370FA12DC575}</AdobeCode>
    <AdobeCode>{DE0E1F6C-09C8-4CC9-B57F-97EE367067AC}</AdobeCode>
    <AdobeCode>{32C716BF-5DC9-46AF-ACD2-8C9B6F3CF50D}</AdobeCode>
    <AdobeCode>{A262B72E-CAAD-41F6-B81F-56E78093E4EE}</AdobeCode>
    <AdobeCode>{71D0D885-03CA-4E26-9E64-BC9B97372103}</AdobeCode>
    <AdobeCode>{25A619BD-3B2C-4CCA-A6DE-02D79423977F}</AdobeCode>
    <AdobeCode>{5517BFBA-D73A-4A06-869C-467563911608}</AdobeCode>
    <AdobeCode>{F8A1BE32-3821-46C6-B793-8EF6EA2A648B}</AdobeCode>
    <AdobeCode>{C8015D9E-D33E-4596-9C89-EAC41AB16E64}</AdobeCode>
    <AdobeCode>{14EDC309-4A1A-4765-92EB-8B2D6D8EB932}</AdobeCode>
    <AdobeCode>{F8F5CF5D-1758-4A34-BC7D-1A4035834053}</AdobeCode>
    <AdobeCode>{1EB57AC7-43FC-45C3-9925-203F5D927D0E}</AdobeCode>
    <AdobeCode>{DACE04C9-733D-4431-B122-D3D8FF5F953E}</AdobeCode>
    <AdobeCode>{5DDDC40E-0529-4E16-9809-4DC38264AD93}</AdobeCode>
    <AdobeCode>{E4315970-7A52-4BD5-8C1A-BC9D09538F46}</AdobeCode>
    <AdobeCode>{E733B1BF-9F0C-44EC-BF9E-54B61E4079F2}</AdobeCode>
    <AdobeCode>{19F3D2A8-18E6-406F-B5AD-F9957CE78090}</AdobeCode>
    <AdobeCode>{9CF6624E-A931-48E0-931C-51858245F2B0}</AdobeCode>
    <AdobeCode>{52174182-F695-4AF4-B021-EB45318460F2}</AdobeCode>
    <AdobeCode>{D9B0663F-DA9D-43EF-8ACC-524B3EB635B1}</AdobeCode>
    <AdobeCode>{E266ABCF-F312-4C98-8B2B-3BCE9007501C}</AdobeCode>
    <AdobeCode>{97F3CEE6-639F-436E-ACCB-7D8031AAC8E9}</AdobeCode>
    <AdobeCode>{87EF47AC-9B12-4E6A-8894-99E847C34AA2}</AdobeCode>
    <AdobeCode>{E1EE3346-7DA5-4866-8450-33087A42F638}</AdobeCode>
    <AdobeCode>{5590BC20-3732-4DCB-B907-971E7E939B37}</AdobeCode>
    <AdobeCode>{25E9ED26-5EC6-4461-B4C5-EFA82E820E8A}</AdobeCode>
    <AdobeCode>{D53220D9-DF06-4C3F-8343-B94E7B5D6D8D}</AdobeCode>
    <AdobeCode>{448CF391-25D3-4C26-9C1C-D9163255B5B6}</AdobeCode>
    <AdobeCode>{C271A338-B7F2-4E8F-8DB1-A300527E9569}</AdobeCode>
    <AdobeCode>{7F24D403-DA4E-42FA-AD6B-2EA534AC430F}</AdobeCode>
    <AdobeCode>{54875E5A-D371-4466-9A18-7E08A97F4B49}</AdobeCode>
    <AdobeCode>{A022F30E-F34C-48FD-A888-DF49CD5B381F}</AdobeCode>
    <AdobeCode>{DA44BDD6-5144-4650-8700-B3FAEA5C702B}</AdobeCode>
    <AdobeCode>{7AA75B77-F5E7-4AD0-A26C-1E91B031EA4D}</AdobeCode>
    <AdobeCode>{373B3E73-B00F-450A-B328-7EED6DA4B4DE}</AdobeCode>
    <AdobeCode>{37EE03C1-DA46-4BFD-8A5C-FF2928E845C6}</AdobeCode>
    <AdobeCode>{5CE88ABE-1786-4AC6-92D7-918DA62D6C09}</AdobeCode>
    <AdobeCode>{3B8D1B4C-7D0D-49CC-A0B8-184F4E40D89C}</AdobeCode>
    <AdobeCode>{A759B975-A5FE-4702-AEE4-35BAE5BDAD75}</AdobeCode>
    <AdobeCode>{F633C76C-46E0-416B-BC99-3D3736579766}</AdobeCode>
    <AdobeCode>{054918F3-F6E6-46F7-9763-F8C32AEF37C2}</AdobeCode>
    <AdobeCode>{9DC4D8CD-A337-450F-954C-6F16A01E5F65}</AdobeCode>
    <AdobeCode>{683D1321-1B82-4ACA-8B49-A4ACD303E564}</AdobeCode>
    <AdobeCode>{DEED62AD-9B86-41B9-B89C-A4128CC7815A}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeHunspellLinguisticsPluginCS6-1.0">
    <DisplayName>Adobe Hunspell Linguistics Plugin CS6</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="172290897"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>172290897</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Plugins2/AdobeHunspellPlugin/Dictionaries/en_US/Medical\README_en_US-OpenMedSpel.txt</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="172290897"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">1.0</Value>
      <Value lang="be_BY">1.0</Value>
      <Value lang="bg_BG">1.0</Value>
      <Value lang="ca_ES">1.0</Value>
      <Value lang="cs_CZ">1.0</Value>
      <Value lang="da_DK">1.0</Value>
      <Value lang="de_DE">1.0</Value>
      <Value lang="el_GR">1.0</Value>
      <Value lang="en_GB">1.0</Value>
      <Value lang="en_MX">1.0</Value>
      <Value lang="en_US">1.0</Value>
      <Value lang="en_XC">1.0</Value>
      <Value lang="en_XM">1.0</Value>
      <Value lang="es_ES">1.0</Value>
      <Value lang="es_MX">1.0</Value>
      <Value lang="es_QM">1.0</Value>
      <Value lang="et_EE">1.0</Value>
      <Value lang="fi_FI">1.0</Value>
      <Value lang="fr_CA">1.0</Value>
      <Value lang="fr_FR">1.0</Value>
      <Value lang="fr_MX">1.0</Value>
      <Value lang="fr_XM">1.0</Value>
      <Value lang="he_IL">1.0</Value>
      <Value lang="hi_IN">1.0</Value>
      <Value lang="hr_HR">1.0</Value>
      <Value lang="hu_HU">1.0</Value>
      <Value lang="is_IS">1.0</Value>
      <Value lang="it_IT">1.0</Value>
      <Value lang="ja_JP">1.0</Value>
      <Value lang="ko_KR">1.0</Value>
      <Value lang="lt_LT">1.0</Value>
      <Value lang="lv_LV">1.0</Value>
      <Value lang="mk_MK">1.0</Value>
      <Value lang="nb_NO">1.0</Value>
      <Value lang="nl_NL">1.0</Value>
      <Value lang="nn_NO">1.0</Value>
      <Value lang="no_NO">1.0</Value>
      <Value lang="pl_PL">1.0</Value>
      <Value lang="pt_BR">1.0</Value>
      <Value lang="ro_RO">1.0</Value>
      <Value lang="ru_RU">1.0</Value>
      <Value lang="sh_YU">1.0</Value>
      <Value lang="sk_SK">1.0</Value>
      <Value lang="sl_SI">1.0</Value>
      <Value lang="sq_AL">1.0</Value>
      <Value lang="sv_SE">1.0</Value>
      <Value lang="th_TH">1.0</Value>
      <Value lang="tr_TR">1.0</Value>
      <Value lang="uk_UA">1.0</Value>
      <Value lang="vi_VN">1.0</Value>
      <Value lang="zh_CN">1.0</Value>
      <Value lang="zh_TW">1.0</Value>
      <Value lang="en_AE">1.0</Value>
      <Value lang="en_IL">1.0</Value>
      <Value lang="fr_MA">1.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="be_BY">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="bg_BG">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="ca_ES">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="cs_CZ">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="da_DK">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="de_DE">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="el_GR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_GB">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_MX">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_US">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_XC">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_XM">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="es_ES">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="es_MX">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="es_QM">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="et_EE">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fi_FI">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fr_CA">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fr_FR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fr_MX">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fr_XM">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="he_IL">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="hi_IN">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="hr_HR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="hu_HU">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="is_IS">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="it_IT">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="ja_JP">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="ko_KR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="lt_LT">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="lv_LV">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="mk_MK">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="nb_NO">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="nl_NL">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="nn_NO">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="no_NO">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="pl_PL">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="pt_BR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="ro_RO">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="ru_RU">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="sh_YU">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="sk_SK">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="sl_SI">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="sq_AL">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="sv_SE">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="th_TH">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="tr_TR">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="uk_UA">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="vi_VN">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="zh_CN">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="zh_TW">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_AE">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="en_IL">Adobe Hunspell Linguistics Plugin CS6</Value>
      <Value lang="fr_MA">Adobe Hunspell Linguistics Plugin CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0", "AMTConfigPath", "[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\AMT\component.xml")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0", "ChannelID", "AdobeHunspellLinguisticsPluginCS6-1.0")
INSERT INTO PayloadData VALUES("{784B5277-7B8A-4058-8F5D-A146F8BA5F7B}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeHunspellLinguisticsPluginCS6-1.0">
    <DisplayName>Adobe Hunspell Linguistics Plugin CS6</DisplayName>
  </Channel>')
