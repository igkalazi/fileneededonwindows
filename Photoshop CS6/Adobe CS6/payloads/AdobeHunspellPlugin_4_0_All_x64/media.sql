CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{E4AEDFF8-21C4-4ea6-A31A-D671EA2D7098}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{C6EC880B-4AEB-40ce-89B7-76C0D6246B93}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{3E15F1B9-693C-486A-A52B-88DB10B3411F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{11A689FD-C160-46C9-86D5-79D4BFEA5B23}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{27AFED0C-670F-493A-9E84-66D29329F57E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{8063E2D2-C4C9-4633-BF53-136FAEC2C867}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{DC218C4A-42AC-4205-AB81-3D34B8BA5F48}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{EA07565E-F21F-41F2-B30C-B2FF4A991FFA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{800CF13D-EFAF-44FC-915B-2F90DACF36B3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{C0696F92-600D-46C8-956E-CC5AB2279045}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{D453F075-C0EB-422C-A045-15F56FB564A7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{E4C486CA-FB12-43D9-BAA2-C85742E1AD7D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{46BA04D5-819D-4842-90AB-BE968B38179C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{B0B65CC9-E5B9-4D0A-92B8-0FA4A30B8C38}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{9DD6CBFB-13A7-464F-95F5-9C6095A3BFC0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{9F161ED3-04E2-4A10-BB3E-E67E0EFC34BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{30C58F22-D9CA-4D39-ADFF-144FCFEEDAC4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{8D5C59F5-B8D6-4B3E-939D-2D930918993C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{FAAB7A64-5582-4142-8747-39778680E5DA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{28C2D682-8652-4747-8FC3-1052157B6E23}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{F8EA68F4-F758-407B-8B51-D99B55472857}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{3CFC46D4-CDD1-4D28-BA95-4A446E875C97}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{65A7C63C-C6DE-4A2A-B4F5-3AA8C52B3C16}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{A84362E5-37E3-4775-AA14-0502D54F6ABB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{231C4057-6C66-4AFA-B711-17BB7C7EBB17}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{252289C8-11EB-4059-91DA-46E36E4262BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{3490E79E-51DB-41FA-934D-F3D488B7D008}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{6337AC64-DA9D-41CF-99D9-E275499461DA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{49B6ABE1-B289-421A-8E66-467821C4396E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{9B3BDD24-EE76-4961-A8D4-4093D62DC9F1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{18073EA2-BB81-4788-94CD-ED0CA8615F5D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "{2977B860-8183-49FE-AD78-755357258E5F}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\\AdobeHunspellPlugin.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0" , "ValidationSig", "I0eyFx7ZI/B9eoJcM044JDDIVYXe8lN7vrNRzOciGZY+l6eM5CUKb4CzyoyV+oyh1aTuPllJnJv/0NbHvtPLcG+S70jXIrIrkR96Cy2JE/v4h4SVpTjKc+fBvAxfSJ6RFMwtOjB2so7uoJfWs5j13zSLZI4OpDeCOIvR+3vGNFz7IDERW89wm+ZoulGpIGia1TT1DwNK4IovF46hx8zfD9i+R5YANoOiLXt19u3TfKsFplTyKP+gUIqs5tfzTLesAUzaOg+zpP2fkWGbAytcTt/Nw+nKrgTmqlOKqGoFFpNMC92tpQHYW2X8zwlnyhgoywtgEYnKRkdK+avojAfptAeqCqHOJTR/BUlEeM/Jx6FF53Yn7HDiNXYwMSJqandsdhRL2gn/+NBwtKJHaMH7e+yK2ZNZ+q+aUcEibPc/YQ1thAmNt2Gsxa3+KHKLIItOtxwWaURkJvfkp1z7ZFqKkM4PDfVNsfHjCvhtfB4Xaumla1vl5X7Yi69wu+OlOEzG")
INSERT INTO Branding VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5BZG9iZUh1
bnNwZWxsLUluc3RhbGxlci1BZG9iZUNvZGU8L0RhdGE+DQogICAgPC9QYXlsb2FkPg0KPC9Db25m
aWd1cmF0aW9uPg==')
INSERT INTO Payloads VALUES	("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "CoreTech", "Adobe Hunspell Linguistics Plugin CS6 x64", "1.0", "normal")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-03-03 17:12:09.589000</Property>
    <Property name="TargetName">AdobeHunspellPlugin_4_0_All_x64</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}</Property>
    <Property name="ProductName">Adobe Hunspell Linguistics Plugin CS6 x64</Property>
    <Property name="ProductVersion">1.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin</Platform>
    <Platform isFixed="1" name="OSX" folderName="">[AdobeCommon]/Linguistics/6.0/Providers/Plugins2/AdobeHunspellPlugin.bundle</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Hunspell Linguistics Plugin CS6 x64</ProductName>
	<ProductVersion>1.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{E4AEDFF8-21C4-4ea6-A31A-D671EA2D7098}</AdobeCode>
    <AdobeCode>{C6EC880B-4AEB-40ce-89B7-76C0D6246B93}</AdobeCode>
    <AdobeCode>{3E15F1B9-693C-486A-A52B-88DB10B3411F}</AdobeCode>
    <AdobeCode>{11A689FD-C160-46C9-86D5-79D4BFEA5B23}</AdobeCode>
    <AdobeCode>{27AFED0C-670F-493A-9E84-66D29329F57E}</AdobeCode>
    <AdobeCode>{8063E2D2-C4C9-4633-BF53-136FAEC2C867}</AdobeCode>
    <AdobeCode>{DC218C4A-42AC-4205-AB81-3D34B8BA5F48}</AdobeCode>
    <AdobeCode>{EA07565E-F21F-41F2-B30C-B2FF4A991FFA}</AdobeCode>
    <AdobeCode>{800CF13D-EFAF-44FC-915B-2F90DACF36B3}</AdobeCode>
    <AdobeCode>{C0696F92-600D-46C8-956E-CC5AB2279045}</AdobeCode>
    <AdobeCode>{D453F075-C0EB-422C-A045-15F56FB564A7}</AdobeCode>
    <AdobeCode>{E4C486CA-FB12-43D9-BAA2-C85742E1AD7D}</AdobeCode>
    <AdobeCode>{46BA04D5-819D-4842-90AB-BE968B38179C}</AdobeCode>
    <AdobeCode>{B0B65CC9-E5B9-4D0A-92B8-0FA4A30B8C38}</AdobeCode>
    <AdobeCode>{9DD6CBFB-13A7-464F-95F5-9C6095A3BFC0}</AdobeCode>
    <AdobeCode>{9F161ED3-04E2-4A10-BB3E-E67E0EFC34BB}</AdobeCode>
    <AdobeCode>{30C58F22-D9CA-4D39-ADFF-144FCFEEDAC4}</AdobeCode>
    <AdobeCode>{8D5C59F5-B8D6-4B3E-939D-2D930918993C}</AdobeCode>
    <AdobeCode>{FAAB7A64-5582-4142-8747-39778680E5DA}</AdobeCode>
    <AdobeCode>{28C2D682-8652-4747-8FC3-1052157B6E23}</AdobeCode>
    <AdobeCode>{F8EA68F4-F758-407B-8B51-D99B55472857}</AdobeCode>
    <AdobeCode>{3CFC46D4-CDD1-4D28-BA95-4A446E875C97}</AdobeCode>
    <AdobeCode>{65A7C63C-C6DE-4A2A-B4F5-3AA8C52B3C16}</AdobeCode>
    <AdobeCode>{A84362E5-37E3-4775-AA14-0502D54F6ABB}</AdobeCode>
    <AdobeCode>{231C4057-6C66-4AFA-B711-17BB7C7EBB17}</AdobeCode>
    <AdobeCode>{252289C8-11EB-4059-91DA-46E36E4262BB}</AdobeCode>
    <AdobeCode>{3490E79E-51DB-41FA-934D-F3D488B7D008}</AdobeCode>
    <AdobeCode>{6337AC64-DA9D-41CF-99D9-E275499461DA}</AdobeCode>
    <AdobeCode>{49B6ABE1-B289-421A-8E66-467821C4396E}</AdobeCode>
    <AdobeCode>{9B3BDD24-EE76-4961-A8D4-4093D62DC9F1}</AdobeCode>
    <AdobeCode>{18073EA2-BB81-4788-94CD-ED0CA8615F5D}</AdobeCode>
    <AdobeCode>{2977B860-8183-49FE-AD78-755357258E5F}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeHunspellLinguisticsPluginCS6x64-1.0">
    <DisplayName>Adobe Hunspell Linguistics Plugin CS6 x64</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="172428625"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>172428625</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Plugins2/AdobeHunspellPlugin/Dictionaries/en_US/Medical\README_en_US-OpenMedSpel.txt</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="172428625"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">1.0</Value>
      <Value lang="be_BY">1.0</Value>
      <Value lang="bg_BG">1.0</Value>
      <Value lang="ca_ES">1.0</Value>
      <Value lang="cs_CZ">1.0</Value>
      <Value lang="da_DK">1.0</Value>
      <Value lang="de_DE">1.0</Value>
      <Value lang="el_GR">1.0</Value>
      <Value lang="en_GB">1.0</Value>
      <Value lang="en_MX">1.0</Value>
      <Value lang="en_US">1.0</Value>
      <Value lang="en_XC">1.0</Value>
      <Value lang="en_XM">1.0</Value>
      <Value lang="es_ES">1.0</Value>
      <Value lang="es_MX">1.0</Value>
      <Value lang="es_QM">1.0</Value>
      <Value lang="et_EE">1.0</Value>
      <Value lang="fi_FI">1.0</Value>
      <Value lang="fr_CA">1.0</Value>
      <Value lang="fr_FR">1.0</Value>
      <Value lang="fr_MX">1.0</Value>
      <Value lang="fr_XM">1.0</Value>
      <Value lang="he_IL">1.0</Value>
      <Value lang="hi_IN">1.0</Value>
      <Value lang="hr_HR">1.0</Value>
      <Value lang="hu_HU">1.0</Value>
      <Value lang="is_IS">1.0</Value>
      <Value lang="it_IT">1.0</Value>
      <Value lang="ja_JP">1.0</Value>
      <Value lang="ko_KR">1.0</Value>
      <Value lang="lt_LT">1.0</Value>
      <Value lang="lv_LV">1.0</Value>
      <Value lang="mk_MK">1.0</Value>
      <Value lang="nb_NO">1.0</Value>
      <Value lang="nl_NL">1.0</Value>
      <Value lang="nn_NO">1.0</Value>
      <Value lang="no_NO">1.0</Value>
      <Value lang="pl_PL">1.0</Value>
      <Value lang="pt_BR">1.0</Value>
      <Value lang="ro_RO">1.0</Value>
      <Value lang="ru_RU">1.0</Value>
      <Value lang="sh_YU">1.0</Value>
      <Value lang="sk_SK">1.0</Value>
      <Value lang="sl_SI">1.0</Value>
      <Value lang="sq_AL">1.0</Value>
      <Value lang="sv_SE">1.0</Value>
      <Value lang="th_TH">1.0</Value>
      <Value lang="tr_TR">1.0</Value>
      <Value lang="uk_UA">1.0</Value>
      <Value lang="vi_VN">1.0</Value>
      <Value lang="zh_CN">1.0</Value>
      <Value lang="zh_TW">1.0</Value>
      <Value lang="en_AE">1.0</Value>
      <Value lang="en_IL">1.0</Value>
      <Value lang="fr_MA">1.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="be_BY">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="bg_BG">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="ca_ES">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="cs_CZ">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="da_DK">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="de_DE">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="el_GR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_GB">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_MX">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_US">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_XC">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_XM">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="es_ES">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="es_MX">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="es_QM">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="et_EE">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fi_FI">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_CA">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_FR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_MX">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_XM">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="he_IL">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="hi_IN">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="hr_HR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="hu_HU">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="is_IS">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="it_IT">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="ja_JP">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="ko_KR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="lt_LT">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="lv_LV">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="mk_MK">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="nb_NO">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="nl_NL">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="nn_NO">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="no_NO">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="pl_PL">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="pt_BR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="ro_RO">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="ru_RU">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="sh_YU">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="sk_SK">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="sl_SI">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="sq_AL">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="sv_SE">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="th_TH">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="tr_TR">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="uk_UA">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="vi_VN">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="zh_CN">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="zh_TW">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_AE">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="en_IL">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
      <Value lang="fr_MA">Adobe Hunspell Linguistics Plugin CS6 x64</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0", "AMTConfigPath", "[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\AdobeHunspellPlugin\AMT\component.xml")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0", "ChannelID", "AdobeHunspellLinguisticsPluginCS6x64-1.0")
INSERT INTO PayloadData VALUES("{29DB26AB-81CE-41D6-B0C9-BE15E36C87C6}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeHunspellLinguisticsPluginCS6x64-1.0">
    <DisplayName>Adobe Hunspell Linguistics Plugin CS6 x64</DisplayName>
  </Channel>')
