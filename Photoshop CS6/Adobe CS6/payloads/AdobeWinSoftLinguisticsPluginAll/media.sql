CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{5146436D-B705-42ae-B01F-FB45F86BA264}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{DF394AE7-027E-447f-A0C6-4772FB5F420B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{FD6BC53F-8EE7-4C6C-8A97-1BFE95D6FBAD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{9D4D6E22-FF53-4BD1-9E39-0E77B608770C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{9EDA132D-551E-4018-951A-3A34CD4D567E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{3A9D8D16-8635-4021-8337-5D1828F93C2C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{C8D488EE-222A-4018-A3A2-BE430E899F40}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{BD8C9F93-6915-4194-B047-639BE66D244E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{78243CE8-7457-4901-AE1B-EFC17363F5F4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{F096F7D4-AA32-42FF-86CB-770206BCFF77}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{BD4A5B43-9EBD-44D9-9E4C-E7A153BA57CB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{5452C54A-E538-4E29-8505-F87898961CA6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{F129E162-5482-40A5-80B0-B231FAC8E7DB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{A0232D75-C72D-4718-B15B-4F6915BE8CF4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "{0D803D98-7EFC-4159-B1F9-9282AF0787CD}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{94FEA41F-7345-429F-AA31-5C615F24CE29}"><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\CNSWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\CSWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\MORPHLibs.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\NFTWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\PACWin_MacEnc.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File><File><InstallPath>[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\\WRLiloPlugin.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated.{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0" , "ValidationSig", "VPWtlCr1TgYElpEpIxluP1aqOFa560Do2ziWWsOe/5+HI8A7rmoex/W8BpuHDnoY8Y5OuqB9REsoKQhmfpyG7fli95zDNU2xj/G00v+fADCalVbWrYtUb+ABMhIWCOcCLOuZ3bfVBoMaDqh0BILyAMl3a1Pw/c4rDOMGQpsn8/RbeFaI9ljZvUtIawKkOBClxhXj+s1hG9gB6RK18sHH531u2e8QdURgCEDHmvjUKMCi+malIMADmy6lxevpBBwFNwPOIDtOG6Gt4jKEvoIMN9lFC5MnYDJxx/pgGbQYb+TJLQMkmbxwJ8oWvhrNZVDnJu5uybAvs7Ceo55dPjCJyrZ3iQJn40xDS67M9QSVda/deRVI//RaNsp6PrhWat5+U2xtPcvvf038T/DfpxHtTckU69aPh0j15dwnmVXQb+ddbgTaNFudEvsBCBHlXWXBf6HKtaFyT7LLZwRum9kXbzBLKpzrM4K3yfnAtyWDjPSkHpitA02aBNdKgD4l98Ly")
INSERT INTO Branding VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
ICAgIDxQYXlsb2FkPg0KICAgICAgICA8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3Bt
ZW50IHVzZS4gLS0+DQogICAgICAgPERhdGEga2V5PSJkZWZhdWx0QWRvYmVDb2RlIj5XaW5zb2Z0
TGluZ3Vpc3RpY3MtSW5zdGFsbGVyLUFkb2JlQ29kZTwvRGF0YT4NCiAgICA8L1BheWxvYWQ+DQo8
L0NvbmZpZ3VyYXRpb24+')
INSERT INTO Payloads VALUES	("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "CoreTech", "Adobe WinSoft Linguistics Plugin CS6", "1.3", "normal")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-23 18:24:30.969000</Property>
    <Property name="TargetName">AdobeWinSoftLinguisticsPluginAll</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{94FEA41F-7345-429F-AA31-5C615F24CE29}</Property>
    <Property name="ProductName">Adobe WinSoft Linguistics Plugin CS6</Property>
    <Property name="ProductVersion">1.3</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3</Platform>
    <Platform isFixed="1" name="OSX" folderName="">[AdobeCommon]/Linguistics/6.0/Providers/Plugins2/WRLiloPlugin1.3.bundle</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe WinSoft Linguistics Plugin CS6</ProductName>
	<ProductVersion>1.3</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{5146436D-B705-42ae-B01F-FB45F86BA264}</AdobeCode>
    <AdobeCode>{DF394AE7-027E-447f-A0C6-4772FB5F420B}</AdobeCode>
    <AdobeCode>{FD6BC53F-8EE7-4C6C-8A97-1BFE95D6FBAD}</AdobeCode>
    <AdobeCode>{9D4D6E22-FF53-4BD1-9E39-0E77B608770C}</AdobeCode>
    <AdobeCode>{9EDA132D-551E-4018-951A-3A34CD4D567E}</AdobeCode>
    <AdobeCode>{3A9D8D16-8635-4021-8337-5D1828F93C2C}</AdobeCode>
    <AdobeCode>{C8D488EE-222A-4018-A3A2-BE430E899F40}</AdobeCode>
    <AdobeCode>{BD8C9F93-6915-4194-B047-639BE66D244E}</AdobeCode>
    <AdobeCode>{78243CE8-7457-4901-AE1B-EFC17363F5F4}</AdobeCode>
    <AdobeCode>{F096F7D4-AA32-42FF-86CB-770206BCFF77}</AdobeCode>
    <AdobeCode>{BD4A5B43-9EBD-44D9-9E4C-E7A153BA57CB}</AdobeCode>
    <AdobeCode>{5452C54A-E538-4E29-8505-F87898961CA6}</AdobeCode>
    <AdobeCode>{F129E162-5482-40A5-80B0-B231FAC8E7DB}</AdobeCode>
    <AdobeCode>{A0232D75-C72D-4718-B15B-4F6915BE8CF4}</AdobeCode>
    <AdobeCode>{0D803D98-7EFC-4159-B1F9-9282AF0787CD}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeWinSoftLinguisticsPluginCS6-1.3">
    <DisplayName>Adobe WinSoft Linguistics Plugin CS6</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\AMT\component.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="7853268"><Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\component.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>7853268</TotalSize>
      <MaxPathComponent>/Linguistics/6.0/Providers/Plugins2/WRLiloPlugin1.3/AMT\component.xml</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="7853268"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">1.3</Value>
      <Value lang="be_BY">1.3</Value>
      <Value lang="bg_BG">1.3</Value>
      <Value lang="ca_ES">1.3</Value>
      <Value lang="cs_CZ">1.3</Value>
      <Value lang="da_DK">1.3</Value>
      <Value lang="de_DE">1.3</Value>
      <Value lang="el_GR">1.3</Value>
      <Value lang="en_GB">1.3</Value>
      <Value lang="en_MX">1.3</Value>
      <Value lang="en_US">1.3</Value>
      <Value lang="en_XC">1.3</Value>
      <Value lang="en_XM">1.3</Value>
      <Value lang="es_ES">1.3</Value>
      <Value lang="es_MX">1.3</Value>
      <Value lang="es_QM">1.3</Value>
      <Value lang="et_EE">1.3</Value>
      <Value lang="fi_FI">1.3</Value>
      <Value lang="fr_CA">1.3</Value>
      <Value lang="fr_FR">1.3</Value>
      <Value lang="fr_MX">1.3</Value>
      <Value lang="fr_XM">1.3</Value>
      <Value lang="he_IL">1.3</Value>
      <Value lang="hi_IN">1.3</Value>
      <Value lang="hr_HR">1.3</Value>
      <Value lang="hu_HU">1.3</Value>
      <Value lang="is_IS">1.3</Value>
      <Value lang="it_IT">1.3</Value>
      <Value lang="ja_JP">1.3</Value>
      <Value lang="ko_KR">1.3</Value>
      <Value lang="lt_LT">1.3</Value>
      <Value lang="lv_LV">1.3</Value>
      <Value lang="mk_MK">1.3</Value>
      <Value lang="nb_NO">1.3</Value>
      <Value lang="nl_NL">1.3</Value>
      <Value lang="nn_NO">1.3</Value>
      <Value lang="no_NO">1.3</Value>
      <Value lang="pl_PL">1.3</Value>
      <Value lang="pt_BR">1.3</Value>
      <Value lang="ro_RO">1.3</Value>
      <Value lang="ru_RU">1.3</Value>
      <Value lang="sh_YU">1.3</Value>
      <Value lang="sk_SK">1.3</Value>
      <Value lang="sl_SI">1.3</Value>
      <Value lang="sq_AL">1.3</Value>
      <Value lang="sv_SE">1.3</Value>
      <Value lang="th_TH">1.3</Value>
      <Value lang="tr_TR">1.3</Value>
      <Value lang="uk_UA">1.3</Value>
      <Value lang="vi_VN">1.3</Value>
      <Value lang="zh_CN">1.3</Value>
      <Value lang="zh_TW">1.3</Value>
      <Value lang="en_AE">1.3</Value>
      <Value lang="en_IL">1.3</Value>
      <Value lang="fr_MA">1.3</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="be_BY">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="bg_BG">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="ca_ES">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="cs_CZ">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="da_DK">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="de_DE">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="el_GR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_GB">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_MX">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_US">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_XC">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_XM">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="es_ES">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="es_MX">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="es_QM">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="et_EE">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fi_FI">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fr_CA">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fr_FR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fr_MX">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fr_XM">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="he_IL">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="hi_IN">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="hr_HR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="hu_HU">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="is_IS">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="it_IT">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="ja_JP">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="ko_KR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="lt_LT">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="lv_LV">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="mk_MK">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="nb_NO">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="nl_NL">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="nn_NO">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="no_NO">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="pl_PL">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="pt_BR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="ro_RO">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="ru_RU">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="sh_YU">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="sk_SK">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="sl_SI">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="sq_AL">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="sv_SE">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="th_TH">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="tr_TR">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="uk_UA">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="vi_VN">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="zh_CN">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="zh_TW">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_AE">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="en_IL">Adobe WinSoft Linguistics Plugin CS6</Value>
      <Value lang="fr_MA">Adobe WinSoft Linguistics Plugin CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0", "AMTConfigPath", "[AdobeCommon]\Linguistics\6.0\Providers\Plugins2\WRLiloPlugin1.3\AMT\component.xml")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0", "ChannelID", "AdobeWinSoftLinguisticsPluginCS6-1.3")
INSERT INTO PayloadData VALUES("{94FEA41F-7345-429F-AA31-5C615F24CE29}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeWinSoftLinguisticsPluginCS6-1.3">
    <DisplayName>Adobe WinSoft Linguistics Plugin CS6</DisplayName>
  </Channel>')
