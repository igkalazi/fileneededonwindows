CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "{A2B3ADDB-DB6F-42E7-B487-DFB519D67395}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{B2D792AF-F407-4EFA-9A03-3F2A476146F6}"/>')
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0" , "ValidationSig", "H4oTwEDRA/rgj9QLo2azYEyfk3BGtErfiXDMWmmrUNyM870w4fHIDiet1jtCK7Nnup5dOdqG0hv1mpv6tNMhooq5CIESteMWKviRqYvcXdnTVh/7y43GnYHZJ8S7vH2d7hZbP7ajY90Im49cf9bngHvpZbs9+1eNXA//G+f5CJ7B0d5fWGeVl6XZjKE/AQOvNdaJRtmNuOEcPHcx7T3C23llOLUVIo2kdISWFyUw0aQwmCkJD3ZGy5U5Dk1us5xDZs0A3EGQ3DLVRg6BMc9lW7tKP6XSpYHybpi2xLSNX46LRjBDxoqbBVqp0NC8GR05PbAbTqntgIFcXtyIcDhIh84KnklxR7zzEtvu/YOaASIuQ+lWR4a+j/nLvmgWaHHXuTTgqa2QJLt7DaTQseITVkOJzN2s+81sxw+HcpjcZXABgph6pS7eiNzgA9btQJZv5id2EOCExL1nlcgEULEZiLD8DCUbwmlhmkWu6OnV+Z003bkezHj0y5ssklF9/VjZ")
INSERT INTO Payloads VALUES	("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "CoreTech", "AdobeTypeSupport x64 CS6", "11.0", "normal")
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-11-30 13:40:27.963000</Property>
    <Property name="TargetName">AdobeTypeSupport11_x64-mul</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{B2D792AF-F407-4EFA-9A03-3F2A476146F6}</Property>
    <Property name="ProductName">Adobe Type Support x64 CS6</Property>
    <Property name="ProductVersion">11.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\TypeSupport\CS6</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeTypeSupport x64 CS6</ProductName>
	<ProductVersion>11.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{A2B3ADDB-DB6F-42E7-B487-DFB519D67395}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeTypeSupportx64CS6-11.0">
    <DisplayName>Adobe Type Support x64 CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="5736902"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>5736902</TotalSize>
      <MaxPathComponent>/TypeSupport/CS6/Unicode/Mappings/Adobe\Japanese83pv.txt</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="5736902"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">11.0</Value>
      <Value lang="be_BY">11.0</Value>
      <Value lang="bg_BG">11.0</Value>
      <Value lang="ca_ES">11.0</Value>
      <Value lang="cs_CZ">11.0</Value>
      <Value lang="da_DK">11.0</Value>
      <Value lang="de_DE">11.0</Value>
      <Value lang="el_GR">11.0</Value>
      <Value lang="en_GB">11.0</Value>
      <Value lang="en_MX">11.0</Value>
      <Value lang="en_US">11.0</Value>
      <Value lang="en_XC">11.0</Value>
      <Value lang="en_XM">11.0</Value>
      <Value lang="es_ES">11.0</Value>
      <Value lang="es_MX">11.0</Value>
      <Value lang="es_QM">11.0</Value>
      <Value lang="et_EE">11.0</Value>
      <Value lang="fi_FI">11.0</Value>
      <Value lang="fr_CA">11.0</Value>
      <Value lang="fr_FR">11.0</Value>
      <Value lang="fr_MX">11.0</Value>
      <Value lang="fr_XM">11.0</Value>
      <Value lang="he_IL">11.0</Value>
      <Value lang="hi_IN">11.0</Value>
      <Value lang="hr_HR">11.0</Value>
      <Value lang="hu_HU">11.0</Value>
      <Value lang="is_IS">11.0</Value>
      <Value lang="it_IT">11.0</Value>
      <Value lang="ja_JP">11.0</Value>
      <Value lang="ko_KR">11.0</Value>
      <Value lang="lt_LT">11.0</Value>
      <Value lang="lv_LV">11.0</Value>
      <Value lang="mk_MK">11.0</Value>
      <Value lang="nb_NO">11.0</Value>
      <Value lang="nl_NL">11.0</Value>
      <Value lang="nn_NO">11.0</Value>
      <Value lang="no_NO">11.0</Value>
      <Value lang="pl_PL">11.0</Value>
      <Value lang="pt_BR">11.0</Value>
      <Value lang="ro_RO">11.0</Value>
      <Value lang="ru_RU">11.0</Value>
      <Value lang="sh_YU">11.0</Value>
      <Value lang="sk_SK">11.0</Value>
      <Value lang="sl_SI">11.0</Value>
      <Value lang="sq_AL">11.0</Value>
      <Value lang="sv_SE">11.0</Value>
      <Value lang="th_TH">11.0</Value>
      <Value lang="tr_TR">11.0</Value>
      <Value lang="uk_UA">11.0</Value>
      <Value lang="vi_VN">11.0</Value>
      <Value lang="zh_CN">11.0</Value>
      <Value lang="zh_TW">11.0</Value>
      <Value lang="en_AE">11.0</Value>
      <Value lang="en_IL">11.0</Value>
      <Value lang="fr_MA">11.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Type Support x64 CS6</Value>
      <Value lang="be_BY">Adobe Type Support x64 CS6</Value>
      <Value lang="bg_BG">Adobe Type Support x64 CS6</Value>
      <Value lang="ca_ES">Adobe Type Support x64 CS6</Value>
      <Value lang="cs_CZ">Adobe Type Support x64 CS6</Value>
      <Value lang="da_DK">Adobe Type Support x64 CS6</Value>
      <Value lang="de_DE">Adobe Type Support x64 CS6</Value>
      <Value lang="el_GR">Adobe Type Support x64 CS6</Value>
      <Value lang="en_GB">Adobe Type Support x64 CS6</Value>
      <Value lang="en_MX">Adobe Type Support x64 CS6</Value>
      <Value lang="en_US">Adobe Type Support x64 CS6</Value>
      <Value lang="en_XC">Adobe Type Support x64 CS6</Value>
      <Value lang="en_XM">Adobe Type Support x64 CS6</Value>
      <Value lang="es_ES">Adobe Type Support x64 CS6</Value>
      <Value lang="es_MX">Adobe Type Support x64 CS6</Value>
      <Value lang="es_QM">Adobe Type Support x64 CS6</Value>
      <Value lang="et_EE">Adobe Type Support x64 CS6</Value>
      <Value lang="fi_FI">Adobe Type Support x64 CS6</Value>
      <Value lang="fr_CA">Adobe Type Support x64 CS6</Value>
      <Value lang="fr_FR">Adobe Type Support x64 CS6</Value>
      <Value lang="fr_MX">Adobe Type Support x64 CS6</Value>
      <Value lang="fr_XM">Adobe Type Support x64 CS6</Value>
      <Value lang="he_IL">Adobe Type Support x64 CS6</Value>
      <Value lang="hi_IN">Adobe Type Support x64 CS6</Value>
      <Value lang="hr_HR">Adobe Type Support x64 CS6</Value>
      <Value lang="hu_HU">Adobe Type Support x64 CS6</Value>
      <Value lang="is_IS">Adobe Type Support x64 CS6</Value>
      <Value lang="it_IT">Adobe Type Support x64 CS6</Value>
      <Value lang="ja_JP">Adobe Type Support x64 CS6</Value>
      <Value lang="ko_KR">Adobe Type Support x64 CS6</Value>
      <Value lang="lt_LT">Adobe Type Support x64 CS6</Value>
      <Value lang="lv_LV">Adobe Type Support x64 CS6</Value>
      <Value lang="mk_MK">Adobe Type Support x64 CS6</Value>
      <Value lang="nb_NO">Adobe Type Support x64 CS6</Value>
      <Value lang="nl_NL">Adobe Type Support x64 CS6</Value>
      <Value lang="nn_NO">Adobe Type Support x64 CS6</Value>
      <Value lang="no_NO">Adobe Type Support x64 CS6</Value>
      <Value lang="pl_PL">Adobe Type Support x64 CS6</Value>
      <Value lang="pt_BR">Adobe Type Support x64 CS6</Value>
      <Value lang="ro_RO">Adobe Type Support x64 CS6</Value>
      <Value lang="ru_RU">Adobe Type Support x64 CS6</Value>
      <Value lang="sh_YU">Adobe Type Support x64 CS6</Value>
      <Value lang="sk_SK">Adobe Type Support x64 CS6</Value>
      <Value lang="sl_SI">Adobe Type Support x64 CS6</Value>
      <Value lang="sq_AL">Adobe Type Support x64 CS6</Value>
      <Value lang="sv_SE">Adobe Type Support x64 CS6</Value>
      <Value lang="th_TH">Adobe Type Support x64 CS6</Value>
      <Value lang="tr_TR">Adobe Type Support x64 CS6</Value>
      <Value lang="uk_UA">Adobe Type Support x64 CS6</Value>
      <Value lang="vi_VN">Adobe Type Support x64 CS6</Value>
      <Value lang="zh_CN">Adobe Type Support x64 CS6</Value>
      <Value lang="zh_TW">Adobe Type Support x64 CS6</Value>
      <Value lang="en_AE">Adobe Type Support x64 CS6</Value>
      <Value lang="en_IL">Adobe Type Support x64 CS6</Value>
      <Value lang="fr_MA">Adobe Type Support x64 CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0", "ChannelID", "AdobeTypeSupportx64CS6-11.0")
INSERT INTO PayloadData VALUES("{B2D792AF-F407-4EFA-9A03-3F2A476146F6}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeTypeSupportx64CS6-11.0">
    <DisplayName>Adobe Type Support x64 CS6</DisplayName>
  </Channel>')
