CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{E4848F7A-A783-4F26-B07F-3DDC83E8E6E8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{35DA4F4C-65FA-4D7A-A655-FBAF74FF6F7C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{415C8125-AB7E-4F6D-B13B-63855E07AA36}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{A82B9B4A-8943-43A4-8AB4-41EFB29AACBE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{A8B53CF4-C6A5-4BDF-AF18-869D87091931}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{43AFB612-8FC4-49EE-BA95-26F2149B856C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{E7D5DCA8-59B2-4918-BD6F-EB987B8C7C7A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{7DCE73C5-2B4B-4AF9-9678-A08550F11A40}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "{375DAC96-8410-49DB-A263-CB044313FC11}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{48623899-C152-457D-9B30-1F957332AC2F}"/>')
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0" , "ValidationSig", "gW5pkqYeIbznBpnHIge1eDj5zbV7OlisfoNRH2df+TY4HXOfTBGePyFuIl4l2IkfJVPgACvtioLZ0Xf0as/C/k16vsqWEFJy12mS7viDbYZV9gtMnzVdx3vQFoYq7SUkHz+LwhqSwp1VxXx6lHeBoBnUrmiSLel5vUjUFbfdHuiBiGWLB+AuNBS4NgNkZrXSmi6e4vFw1ixt+aMCkMg8ydjEusAzLZm5wVCQlGfdWpcK1H5jh2VEkob07mMM6EpV73MlAg79vEgY4OGkt/baihbt8v8d2stfmhLO4ZFYrhtyS6x2RNSezA7DDFb/Xmko1uh5bBZnn8DtooPFsSxvqIrBs2CH3fi30qV6lTTtWZ5Fg1+QVS0onZKa6y7SeXlfhaW8d43tUKLr0lGK827q0GaIcDBrCqj6DYLfE/ugLQNPEhYDzR3v9qLKlCFRYWIpCO5HX6bEjSB56w+xFoqiz+5npSd9QEWwu4dWWoxInvekXbd1SPVTm5creX49lAMK")
INSERT INTO Payloads VALUES	("{48623899-C152-457D-9B30-1F957332AC2F}", "CoreTech", "AdobeColorPhotoshop CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-09 00:47:14.824000</Property>
    <Property name="TargetName">AdobeColorPhotoshop4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{48623899-C152-457D-9B30-1F957332AC2F}</Property>
    <Property name="ProductName">Adobe Color - Photoshop Specific CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorPhotoshop CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{E4848F7A-A783-4F26-B07F-3DDC83E8E6E8}</AdobeCode>
    <AdobeCode>{35DA4F4C-65FA-4D7A-A655-FBAF74FF6F7C}</AdobeCode>
    <AdobeCode>{415C8125-AB7E-4F6D-B13B-63855E07AA36}</AdobeCode>
    <AdobeCode>{A82B9B4A-8943-43A4-8AB4-41EFB29AACBE}</AdobeCode>
    <AdobeCode>{A8B53CF4-C6A5-4BDF-AF18-869D87091931}</AdobeCode>
    <AdobeCode>{43AFB612-8FC4-49EE-BA95-26F2149B856C}</AdobeCode>
    <AdobeCode>{E7D5DCA8-59B2-4918-BD6F-EB987B8C7C7A}</AdobeCode>
    <AdobeCode>{7DCE73C5-2B4B-4AF9-9678-A08550F11A40}</AdobeCode>
    <AdobeCode>{375DAC96-8410-49DB-A263-CB044313FC11}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColor-PhotoshopSpecificCS6-4.0">
    <DisplayName>Adobe Color - Photoshop Specific CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="1445176"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>1445176</TotalSize>
      <MaxPathComponent>/Color/Profiles\Photoshop4DefaultCMYK.icc</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="1445176"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="be_BY">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="bg_BG">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="ca_ES">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="cs_CZ">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="da_DK">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="de_DE">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="el_GR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_GB">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_MX">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_US">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_XC">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_XM">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="es_ES">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="es_MX">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="es_QM">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="et_EE">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fi_FI">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fr_CA">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fr_FR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fr_MX">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fr_XM">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="he_IL">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="hi_IN">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="hr_HR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="hu_HU">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="is_IS">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="it_IT">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="ja_JP">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="ko_KR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="lt_LT">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="lv_LV">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="mk_MK">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="nb_NO">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="nl_NL">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="nn_NO">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="no_NO">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="pl_PL">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="pt_BR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="ro_RO">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="ru_RU">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="sh_YU">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="sk_SK">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="sl_SI">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="sq_AL">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="sv_SE">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="th_TH">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="tr_TR">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="uk_UA">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="vi_VN">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="zh_CN">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="zh_TW">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_AE">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="en_IL">Adobe Color - Photoshop Specific CS6</Value>
      <Value lang="fr_MA">Adobe Color - Photoshop Specific CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0", "ChannelID", "AdobeColor-PhotoshopSpecificCS6-4.0")
INSERT INTO PayloadData VALUES("{48623899-C152-457D-9B30-1F957332AC2F}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColor-PhotoshopSpecificCS6-4.0">
    <DisplayName>Adobe Color - Photoshop Specific CS6</DisplayName>
  </Channel>')
