CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{6D9366BD-C546-4335-A39C-D7B44825E69B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{BDF35E5A-4083-482F-8D04-9FE92EE36E25}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{C1E02C34-1133-4D28-8707-5D5825B8F993}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{B3208190-D430-4829-9CD1-44B767777B8C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{24DA818F-1B2A-4C80-912C-61E806908D50}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{D94663E4-1604-41F9-846F-9AEC06978FA4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{6ED79F6F-937D-42EC-B463-F67B5FB4A2F2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{7DC84DA5-AEA8-4D4A-8456-1E9B94563B5A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{463E7820-BDC8-45C3-B2F9-A4127E0BF53D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "{C3FABE39-BF7B-42A7-A7F2-2990B54DEE9F}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{AD8A9ABD-0567-4489-8843-15A45760231B}"/>')
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0" , "ValidationSig", "nUqzwEgOSeraAdbhB2DExv29rO3FrgSdVuOdXXLkmB7jyaxP04dw05Mvcn7g3O58h2n1lwKqI5yPDyi56JkY5+7yCd3MQBg0O+3PYomSSH+Yblnb9N5ei51AxYayWZ2QWR0mTezgYF97bV0XFGwd47NbiHS8ViYvawsbF97rvmbMhsehwE//lKywJ7YXwH/k+RzdSlIN7JiO8n0inL7lJSZ5Qccz6XRwOVl8CWmFk73I/h8sWkNcGVs4jPLmBdw5GM7XRP6U9C0xLkOIrRfmlDRt5YNAgfZQQWS7cVyl8oftHuVH1LaI5fmbD/rnxK+Cu0d3FBlQaTrPf1Row3Cndh/oPTumd05RZVINl9CbqcTvJ6ux0YmTCDBLQHXfZ7Oqcd31GuBQBsupvCOeAQIPJHSs8gk4vzcmDEY30U7Xk+x+guBp2SQ/gUzbJ2Luw9R9mPRgMG8yHVJ+VZDBN4vlUUEHTEol4hWUemCOJUr/vPfgRG3ZBrdy+emXDYOGOsTz")
INSERT INTO Payloads VALUES	("{AD8A9ABD-0567-4489-8843-15A45760231B}", "CoreTech", "AdobeColorJA CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-09 00:32:37.281000</Property>
    <Property name="TargetName">AdobeColorJA_Recommended4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{AD8A9ABD-0567-4489-8843-15A45760231B}</Property>
    <Property name="ProductName">Adobe Color JA Recommended Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>ja_JP</Language>
    <Language>ko_KR</Language>
    <Language>zh_CN</Language>
    <Language>zh_TW</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorJA CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{6D9366BD-C546-4335-A39C-D7B44825E69B}</AdobeCode>
    <AdobeCode>{BDF35E5A-4083-482F-8D04-9FE92EE36E25}</AdobeCode>
    <AdobeCode>{C1E02C34-1133-4D28-8707-5D5825B8F993}</AdobeCode>
    <AdobeCode>{B3208190-D430-4829-9CD1-44B767777B8C}</AdobeCode>
    <AdobeCode>{24DA818F-1B2A-4C80-912C-61E806908D50}</AdobeCode>
    <AdobeCode>{D94663E4-1604-41F9-846F-9AEC06978FA4}</AdobeCode>
    <AdobeCode>{6ED79F6F-937D-42EC-B463-F67B5FB4A2F2}</AdobeCode>
    <AdobeCode>{7DC84DA5-AEA8-4D4A-8456-1E9B94563B5A}</AdobeCode>
    <AdobeCode>{463E7820-BDC8-45C3-B2F9-A4127E0BF53D}</AdobeCode>
    <AdobeCode>{C3FABE39-BF7B-42A7-A7F2-2990B54DEE9F}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorJARecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color JA Recommended Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2867004"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2867004</TotalSize>
      <MaxPathComponent>/Color/Settings/Recommended\Japan General Purpose.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2867004"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ja_JP">Adobe Color JA Recommended Settings CS6</Value>
      <Value lang="ko_KR">Adobe Color JA Recommended Settings CS6</Value>
      <Value lang="zh_CN">Adobe Color JA Recommended Settings CS6</Value>
      <Value lang="zh_TW">Adobe Color JA Recommended Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0", "ChannelID", "AdobeColorJARecommendedSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{AD8A9ABD-0567-4489-8843-15A45760231B}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorJARecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color JA Recommended Settings CS6</DisplayName>
  </Channel>')
