CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{754FFB71-1450-41F5-9AFC-14B5A54C663A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DD41A83F-DE0A-4C3F-984B-A8E8D375E6E7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{FEE457F0-4A44-426D-8E28-F4820148B3BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{800417F5-29FC-4A46-8590-4DA4E78E3B6E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{E7C80473-D9DA-46F4-B027-198120AC350F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{489286E9-8E3F-4554-A4EA-0C5060038500}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{D9E22D46-7A51-4F85-9BCA-799B542F1102}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{80DC6F64-13EC-4EB0-AADB-EC948B7818F5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{75EE740B-6308-4557-9009-9A7450F366E5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{72F841FE-80CC-4335-9B99-8FFC09BAE2EB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DF9E481F-F130-407E-BFA9-3BEAE5EFA9CC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{B1FC0C81-A242-48A6-B80E-503D4EE9DF45}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{80722766-31F1-4A8D-AB86-FF25E98C0CFF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{20206DEF-4DBA-4B0C-A1DE-417F91DED6E0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{1791A91A-152F-4E12-80F0-32BAEF448FD0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{B797B3A7-8665-4940-87D3-0C174FABC1B3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{8AECED86-B966-47DA-9FD4-64CB504BFCD8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{B777CB0D-85E5-46E9-86F3-9C88C6B9176A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A97C3193-D4A5-463D-B243-29AB84540F64}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{62EB1427-9602-427F-B830-9C30DB295C15}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{11B66949-E4F2-43B1-9337-0DCF2CE06DFA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{2D0C50FB-4460-4237-B8D2-9E9D135C73A1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3CEA2672-260E-4D5E-89D4-BD28B68EC78C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{6070C44C-95A3-4897-AC78-8B1F8A28174E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{0A68C804-82D5-4967-B6DC-710DD5E479B8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{93CE6973-368B-4A5A-96D9-B22AB2E656BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C719FF3D-54A5-46F1-A096-5EEF4E4B29C1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{E7B754ED-692C-426F-84D9-5D258D62896C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{20DE258D-5A70-4643-8175-7AB51A264D94}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{EB1F304D-973E-4B4D-8101-D10BA3F4DABC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C9F751D6-9ED0-4183-AAC0-50C764813DA9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{6C84B3D1-B564-4A5E-9DEC-3C26598AC779}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{AEFDF339-BC8D-4092-94A7-1472A53E4C61}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{69F450F2-E445-4356-8840-AB320FCCB2BF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DEC01C9C-E74C-47FE-B69E-426BAF681C80}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{D8B3E5A7-9F73-4A11-A686-6D232F52D6C4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{2EA9D930-A5CC-47A0-A63A-B9D453F28C3D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{411C4A01-5DDF-4638-9DA0-92042C3D7A49}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{9C099CC7-E269-4F31-834C-377BFF0F8DEB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{1644032C-7F1A-42DB-AF1C-79C63E9708E5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{FC444B38-546D-41AB-9755-7CC6950C15D2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{8891C3F4-3E80-43FD-8FA6-3D59D3DAB3A8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{4A2D7EA1-445E-47FF-8190-32EFE3476DF6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{E8E80436-8BB6-422A-A7B8-A6C28CB62A83}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{B08B4EFE-5E4E-462E-A608-067040F842B3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{11D0CE09-FBCF-4A60-A652-DBCA510D5E37}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7B6A9DDF-0CC4-4FAC-842D-9FAE0AA6193D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{90139004-5AF7-4277-BFBB-B7BA9C8E61A7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{9AEAD7FD-6637-4CF8-9810-BD61F87B859F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7FBCDAC2-2825-453D-B989-ED41224C47B4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{1B32F181-AA0A-4D70-AF63-2AB4F525CBB7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{5EB23F37-65AF-4D82-90CB-E68EF7EAE705}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{BDEBFA79-51A8-421F-9876-2DEADBFDA84B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{355DEC0E-39B3-496D-B82D-75E878F95EF0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{D2A35F53-1B4E-4CA9-8538-F28EB63F756E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{0A9BAFC8-ECFE-4386-B43F-51C90843CD38}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{2ED94C02-D497-4369-A4A8-A47502689643}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3D54913B-146D-473D-9BE6-9FADD759BE12}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{91AC124C-BAB7-4262-AC77-EA47110BB92F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{57BD4325-FADB-4310-9CE5-BF7D989F9407}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DCE49CA0-D258-41D1-B350-133275494393}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A14C684F-B740-4027-9CB7-623637BD717E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{F8DA693B-3AF4-4FCB-8B67-447D3BE27BBE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{4F377EF1-F5AD-4175-8889-64CFF82086A9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{27CC74B3-FF39-45A4-A29D-0CFB0D071777}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{AF11B986-DC6A-4CF2-BB5D-337717745491}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{6DE20FC7-0081-44FC-A5A8-E5501D32E9FF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{88EF5B8A-9E1E-4021-8562-96DE0657FAF9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7033E037-E50D-46B1-9EB2-E80D3BBCB70C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3E2DF5FE-1A9C-499C-9B41-043FF2EFBC1C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{9AE8F4E9-FED3-4087-A7ED-B9A9B614EAD3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{4EAB13B1-F3EE-4675-80D6-663AE97488E8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{240BD714-61CD-4DCB-895B-861098E74FE9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{1EBBDBB1-920B-460A-9F97-0E2F18795B90}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{15B4D369-DA16-44F9-A68F-8DE6D31C24E1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C5837D51-061D-4310-80B6-A127DB4AA15E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{50F5E8C1-0F01-4DAB-8C80-0B0F12615058}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{4C027BD6-7514-4869-A442-40D6B3F64ADB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3675ACC1-A6BB-4B16-9BC7-517E0922B263}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A53F005B-4E84-4920-A11E-AC6EF7960EDD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{5DDCDF9B-AEED-4841-8A98-4740F2DC8589}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{86F603A4-BB13-4054-BD80-331FB38D8A55}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{E6324C86-7101-414C-A6E7-75ABB158378C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7545C78F-7D7E-432E-BD41-850DA65947DB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{052D4328-05CC-4C9C-9A28-39746B4A5B7C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{FB086DF1-9BD7-44C7-BC21-E76BCD89160F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{0A0ECA0A-89E7-4C41-AB48-F55995972208}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A6F4CBF1-F984-417B-93DC-33019BCC94FF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{CEFCDE6D-331C-4004-A35B-B6FBD1756927}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{F182F38B-1F1C-4959-B7F0-C23E1C6EE990}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{732D6052-819B-4916-AF1E-590BE7B0615B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{2061A6F0-97AE-441E-BCDA-E5FD9712A77D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C968268A-73F5-4A63-9C88-48005596E836}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{49B0CC96-56AE-4FB6-9233-7D5D4099010B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DB6D4C9B-137E-472E-BA56-CDBB1AA6B3FD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{5B390136-E96B-428E-BA97-956B24C8E438}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{06E43EB7-269C-4663-94F9-5F925C8AE75C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C03239A3-431B-49F3-BF59-16F6B127DE15}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DBD7989F-E0F7-41E3-AB27-6A4F470DF443}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{DF3EFA38-081E-40A7-8627-D13D7E4835EA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{378EFF93-B526-48FB-B582-153231B0D3E8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C34B888D-E5D2-4582-A180-BB00E5621927}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A5368C5A-E5F3-4C15-BB4D-2DD4FAEBD577}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7B277B5B-4E9F-4C62-9743-96BF735504EE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7F58D236-A1B9-4482-B79C-18C5DC961217}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{048580F7-CFB1-42D8-9AA4-83C4FFD481B3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3FA436C2-231C-47AC-ABC5-DA990DF98BD6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{69F9BD71-0590-48EE-947D-4F58BAF5DD4E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{553C906F-3CAE-44DA-872D-F7B3BD483929}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{0742BB92-6EEF-4E34-B3A1-45D1C58762F6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{83445E0C-8715-41EC-BA80-D2EBEAA69B35}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C2137CA6-BC0F-4159-8E02-EB99DA3832A5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{BCA95BDE-9B9C-4AB2-8B5A-CB7631484AF8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{A190BFB4-B5E6-47E4-8D54-342042999421}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{EBCAF2AF-7C1F-4828-B593-0F33E4683ACA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{035B764A-48FD-4FE5-9478-46CA2589B403}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{7134BBEA-5C74-4D3E-BE4C-C7A0B833202A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{0E464A1F-C45C-4C70-971C-AECC8B1274CE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{2E16AAB9-754F-417C-ABD8-61E8AD565281}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{D6B1AF6D-7821-4A2B-A672-62CB8645BF91}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{B5CB1D32-957F-45B8-868A-2E4ABD7BD70A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{9BDBD912-47A3-428D-BC71-6B8AAFE296E9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{89E154E1-B674-44F7-B9A3-02EEABF337E8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{3FD19309-2492-44C0-90CF-047732052246}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{F14A3F0E-EEF4-4337-AE39-E94A2B128AB7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{5EC5B1E8-AB33-443E-8C04-FA8D94337338}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{C2A3FDFE-692B-4C8C-854A-FEA09393B1F7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{1608EB80-797C-49C2-AE60-9041407EF3A4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "{134FA147-A102-40A8-B30D-99D0CF7CD754}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{9254D539-549A-41DD-A7DA-251766F2B76F}"><File><InstallPath>[INSTALLDIR]\adbeapecore.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\adbeapeengine.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\NPSWF32.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\WebKit.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0" , "ValidationSig", "EbWlFD1AyQe3ENqAfMM/HnLx1+40xNjZwVdUeZUtFQG1zsRt31w0wZuOMzjyzbBUBxQncVxZ5VTHazFbDVxu/Qyai+nInMLgLFnUndITZvtum6za8gt+5Sddsz19w6Z66RqAsFw1Rrt9l7DwcUtLOUL7QpO2FZ4XQ8wQyxyjuPmAmYPpgnPe9h5oSbjmxC0VlxPZ8Ix8vVFEzMpfF1EFCNdcx2usijnETXFYeE9IbLeBmCiXz2j718ulMDHEtsCKQjGRzHLuDYKlIsWaaY1gyhVb3jWuQhBzjp2RY5PDNMTAEDLfpHZaUQUDp2WbodaIC0YwVtbUHuCj7rQCtVjPmKZfgyYW4N1qUZ3Pbi+0FHOSRpuXBDviu+WP3Ls45nHezN9IFdiKxbgXIapp6P9s7xZWllgS2ugBQrk/0ueH1hvDVEPsPnbIoqy9NLs6q6tfuq5PzWPjPTxaExx94ArfN18/P8PLvp7EvipbcFg0XhufF9O2v5eZ4ZpCKgKLHXwG")
INSERT INTO Payloads VALUES	("{9254D539-549A-41DD-A7DA-251766F2B76F}", "CoreTech", "Adobe Player for Embedding x64 3.3", "3.3", "normal")
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-25 12:53:40.756000</Property>
    <Property name="TargetName">AdobeAPE3.3_x64-mul</Property>
    <Property name="ProcessorFamily">x64</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{9254D539-549A-41DD-A7DA-251766F2B76F}</Property>
    <Property name="ProductName">Adobe Player for Embedding x64 3.3</Property>
    <Property name="ProductVersion">3.3</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\APE\3.3</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Player for Embedding x64 3.3</ProductName>
	<ProductVersion>3.3</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{754FFB71-1450-41F5-9AFC-14B5A54C663A}</AdobeCode>
    <AdobeCode>{DD41A83F-DE0A-4C3F-984B-A8E8D375E6E7}</AdobeCode>
    <AdobeCode>{FEE457F0-4A44-426D-8E28-F4820148B3BB}</AdobeCode>
    <AdobeCode>{800417F5-29FC-4A46-8590-4DA4E78E3B6E}</AdobeCode>
    <AdobeCode>{E7C80473-D9DA-46F4-B027-198120AC350F}</AdobeCode>
    <AdobeCode>{489286E9-8E3F-4554-A4EA-0C5060038500}</AdobeCode>
    <AdobeCode>{D9E22D46-7A51-4F85-9BCA-799B542F1102}</AdobeCode>
    <AdobeCode>{80DC6F64-13EC-4EB0-AADB-EC948B7818F5}</AdobeCode>
    <AdobeCode>{75EE740B-6308-4557-9009-9A7450F366E5}</AdobeCode>
    <AdobeCode>{72F841FE-80CC-4335-9B99-8FFC09BAE2EB}</AdobeCode>
    <AdobeCode>{DF9E481F-F130-407E-BFA9-3BEAE5EFA9CC}</AdobeCode>
    <AdobeCode>{B1FC0C81-A242-48A6-B80E-503D4EE9DF45}</AdobeCode>
    <AdobeCode>{80722766-31F1-4A8D-AB86-FF25E98C0CFF}</AdobeCode>
    <AdobeCode>{20206DEF-4DBA-4B0C-A1DE-417F91DED6E0}</AdobeCode>
    <AdobeCode>{1791A91A-152F-4E12-80F0-32BAEF448FD0}</AdobeCode>
    <AdobeCode>{B797B3A7-8665-4940-87D3-0C174FABC1B3}</AdobeCode>
    <AdobeCode>{8AECED86-B966-47DA-9FD4-64CB504BFCD8}</AdobeCode>
    <AdobeCode>{B777CB0D-85E5-46E9-86F3-9C88C6B9176A}</AdobeCode>
    <AdobeCode>{A97C3193-D4A5-463D-B243-29AB84540F64}</AdobeCode>
    <AdobeCode>{62EB1427-9602-427F-B830-9C30DB295C15}</AdobeCode>
    <AdobeCode>{11B66949-E4F2-43B1-9337-0DCF2CE06DFA}</AdobeCode>
    <AdobeCode>{2D0C50FB-4460-4237-B8D2-9E9D135C73A1}</AdobeCode>
    <AdobeCode>{3CEA2672-260E-4D5E-89D4-BD28B68EC78C}</AdobeCode>
    <AdobeCode>{6070C44C-95A3-4897-AC78-8B1F8A28174E}</AdobeCode>
    <AdobeCode>{0A68C804-82D5-4967-B6DC-710DD5E479B8}</AdobeCode>
    <AdobeCode>{93CE6973-368B-4A5A-96D9-B22AB2E656BB}</AdobeCode>
    <AdobeCode>{C719FF3D-54A5-46F1-A096-5EEF4E4B29C1}</AdobeCode>
    <AdobeCode>{E7B754ED-692C-426F-84D9-5D258D62896C}</AdobeCode>
    <AdobeCode>{20DE258D-5A70-4643-8175-7AB51A264D94}</AdobeCode>
    <AdobeCode>{EB1F304D-973E-4B4D-8101-D10BA3F4DABC}</AdobeCode>
    <AdobeCode>{C9F751D6-9ED0-4183-AAC0-50C764813DA9}</AdobeCode>
    <AdobeCode>{6C84B3D1-B564-4A5E-9DEC-3C26598AC779}</AdobeCode>
    <AdobeCode>{AEFDF339-BC8D-4092-94A7-1472A53E4C61}</AdobeCode>
    <AdobeCode>{69F450F2-E445-4356-8840-AB320FCCB2BF}</AdobeCode>
    <AdobeCode>{DEC01C9C-E74C-47FE-B69E-426BAF681C80}</AdobeCode>
    <AdobeCode>{D8B3E5A7-9F73-4A11-A686-6D232F52D6C4}</AdobeCode>
    <AdobeCode>{2EA9D930-A5CC-47A0-A63A-B9D453F28C3D}</AdobeCode>
    <AdobeCode>{411C4A01-5DDF-4638-9DA0-92042C3D7A49}</AdobeCode>
    <AdobeCode>{9C099CC7-E269-4F31-834C-377BFF0F8DEB}</AdobeCode>
    <AdobeCode>{1644032C-7F1A-42DB-AF1C-79C63E9708E5}</AdobeCode>
    <AdobeCode>{FC444B38-546D-41AB-9755-7CC6950C15D2}</AdobeCode>
    <AdobeCode>{8891C3F4-3E80-43FD-8FA6-3D59D3DAB3A8}</AdobeCode>
    <AdobeCode>{4A2D7EA1-445E-47FF-8190-32EFE3476DF6}</AdobeCode>
    <AdobeCode>{E8E80436-8BB6-422A-A7B8-A6C28CB62A83}</AdobeCode>
    <AdobeCode>{B08B4EFE-5E4E-462E-A608-067040F842B3}</AdobeCode>
    <AdobeCode>{11D0CE09-FBCF-4A60-A652-DBCA510D5E37}</AdobeCode>
    <AdobeCode>{7B6A9DDF-0CC4-4FAC-842D-9FAE0AA6193D}</AdobeCode>
    <AdobeCode>{90139004-5AF7-4277-BFBB-B7BA9C8E61A7}</AdobeCode>
    <AdobeCode>{9AEAD7FD-6637-4CF8-9810-BD61F87B859F}</AdobeCode>
    <AdobeCode>{7FBCDAC2-2825-453D-B989-ED41224C47B4}</AdobeCode>
    <AdobeCode>{1B32F181-AA0A-4D70-AF63-2AB4F525CBB7}</AdobeCode>
    <AdobeCode>{5EB23F37-65AF-4D82-90CB-E68EF7EAE705}</AdobeCode>
    <AdobeCode>{BDEBFA79-51A8-421F-9876-2DEADBFDA84B}</AdobeCode>
    <AdobeCode>{355DEC0E-39B3-496D-B82D-75E878F95EF0}</AdobeCode>
    <AdobeCode>{D2A35F53-1B4E-4CA9-8538-F28EB63F756E}</AdobeCode>
    <AdobeCode>{0A9BAFC8-ECFE-4386-B43F-51C90843CD38}</AdobeCode>
    <AdobeCode>{2ED94C02-D497-4369-A4A8-A47502689643}</AdobeCode>
    <AdobeCode>{3D54913B-146D-473D-9BE6-9FADD759BE12}</AdobeCode>
    <AdobeCode>{91AC124C-BAB7-4262-AC77-EA47110BB92F}</AdobeCode>
    <AdobeCode>{57BD4325-FADB-4310-9CE5-BF7D989F9407}</AdobeCode>
    <AdobeCode>{DCE49CA0-D258-41D1-B350-133275494393}</AdobeCode>
    <AdobeCode>{A14C684F-B740-4027-9CB7-623637BD717E}</AdobeCode>
    <AdobeCode>{F8DA693B-3AF4-4FCB-8B67-447D3BE27BBE}</AdobeCode>
    <AdobeCode>{4F377EF1-F5AD-4175-8889-64CFF82086A9}</AdobeCode>
    <AdobeCode>{27CC74B3-FF39-45A4-A29D-0CFB0D071777}</AdobeCode>
    <AdobeCode>{AF11B986-DC6A-4CF2-BB5D-337717745491}</AdobeCode>
    <AdobeCode>{6DE20FC7-0081-44FC-A5A8-E5501D32E9FF}</AdobeCode>
    <AdobeCode>{88EF5B8A-9E1E-4021-8562-96DE0657FAF9}</AdobeCode>
    <AdobeCode>{7033E037-E50D-46B1-9EB2-E80D3BBCB70C}</AdobeCode>
    <AdobeCode>{3E2DF5FE-1A9C-499C-9B41-043FF2EFBC1C}</AdobeCode>
    <AdobeCode>{9AE8F4E9-FED3-4087-A7ED-B9A9B614EAD3}</AdobeCode>
    <AdobeCode>{4EAB13B1-F3EE-4675-80D6-663AE97488E8}</AdobeCode>
    <AdobeCode>{240BD714-61CD-4DCB-895B-861098E74FE9}</AdobeCode>
    <AdobeCode>{1EBBDBB1-920B-460A-9F97-0E2F18795B90}</AdobeCode>
    <AdobeCode>{15B4D369-DA16-44F9-A68F-8DE6D31C24E1}</AdobeCode>
    <AdobeCode>{C5837D51-061D-4310-80B6-A127DB4AA15E}</AdobeCode>
    <AdobeCode>{50F5E8C1-0F01-4DAB-8C80-0B0F12615058}</AdobeCode>
    <AdobeCode>{4C027BD6-7514-4869-A442-40D6B3F64ADB}</AdobeCode>
    <AdobeCode>{3675ACC1-A6BB-4B16-9BC7-517E0922B263}</AdobeCode>
    <AdobeCode>{A53F005B-4E84-4920-A11E-AC6EF7960EDD}</AdobeCode>
    <AdobeCode>{5DDCDF9B-AEED-4841-8A98-4740F2DC8589}</AdobeCode>
    <AdobeCode>{86F603A4-BB13-4054-BD80-331FB38D8A55}</AdobeCode>
    <AdobeCode>{E6324C86-7101-414C-A6E7-75ABB158378C}</AdobeCode>
    <AdobeCode>{7545C78F-7D7E-432E-BD41-850DA65947DB}</AdobeCode>
    <AdobeCode>{052D4328-05CC-4C9C-9A28-39746B4A5B7C}</AdobeCode>
    <AdobeCode>{FB086DF1-9BD7-44C7-BC21-E76BCD89160F}</AdobeCode>
    <AdobeCode>{0A0ECA0A-89E7-4C41-AB48-F55995972208}</AdobeCode>
    <AdobeCode>{A6F4CBF1-F984-417B-93DC-33019BCC94FF}</AdobeCode>
    <AdobeCode>{CEFCDE6D-331C-4004-A35B-B6FBD1756927}</AdobeCode>
    <AdobeCode>{F182F38B-1F1C-4959-B7F0-C23E1C6EE990}</AdobeCode>
    <AdobeCode>{732D6052-819B-4916-AF1E-590BE7B0615B}</AdobeCode>
    <AdobeCode>{2061A6F0-97AE-441E-BCDA-E5FD9712A77D}</AdobeCode>
    <AdobeCode>{C968268A-73F5-4A63-9C88-48005596E836}</AdobeCode>
    <AdobeCode>{49B0CC96-56AE-4FB6-9233-7D5D4099010B}</AdobeCode>
    <AdobeCode>{DB6D4C9B-137E-472E-BA56-CDBB1AA6B3FD}</AdobeCode>
    <AdobeCode>{5B390136-E96B-428E-BA97-956B24C8E438}</AdobeCode>
    <AdobeCode>{06E43EB7-269C-4663-94F9-5F925C8AE75C}</AdobeCode>
    <AdobeCode>{C03239A3-431B-49F3-BF59-16F6B127DE15}</AdobeCode>
    <AdobeCode>{DBD7989F-E0F7-41E3-AB27-6A4F470DF443}</AdobeCode>
    <AdobeCode>{DF3EFA38-081E-40A7-8627-D13D7E4835EA}</AdobeCode>
    <AdobeCode>{378EFF93-B526-48FB-B582-153231B0D3E8}</AdobeCode>
    <AdobeCode>{C34B888D-E5D2-4582-A180-BB00E5621927}</AdobeCode>
    <AdobeCode>{A5368C5A-E5F3-4C15-BB4D-2DD4FAEBD577}</AdobeCode>
    <AdobeCode>{7B277B5B-4E9F-4C62-9743-96BF735504EE}</AdobeCode>
    <AdobeCode>{7F58D236-A1B9-4482-B79C-18C5DC961217}</AdobeCode>
    <AdobeCode>{048580F7-CFB1-42D8-9AA4-83C4FFD481B3}</AdobeCode>
    <AdobeCode>{3FA436C2-231C-47AC-ABC5-DA990DF98BD6}</AdobeCode>
    <AdobeCode>{69F9BD71-0590-48EE-947D-4F58BAF5DD4E}</AdobeCode>
    <AdobeCode>{553C906F-3CAE-44DA-872D-F7B3BD483929}</AdobeCode>
    <AdobeCode>{0742BB92-6EEF-4E34-B3A1-45D1C58762F6}</AdobeCode>
    <AdobeCode>{83445E0C-8715-41EC-BA80-D2EBEAA69B35}</AdobeCode>
    <AdobeCode>{C2137CA6-BC0F-4159-8E02-EB99DA3832A5}</AdobeCode>
    <AdobeCode>{BCA95BDE-9B9C-4AB2-8B5A-CB7631484AF8}</AdobeCode>
    <AdobeCode>{A190BFB4-B5E6-47E4-8D54-342042999421}</AdobeCode>
    <AdobeCode>{EBCAF2AF-7C1F-4828-B593-0F33E4683ACA}</AdobeCode>
    <AdobeCode>{035B764A-48FD-4FE5-9478-46CA2589B403}</AdobeCode>
    <AdobeCode>{7134BBEA-5C74-4D3E-BE4C-C7A0B833202A}</AdobeCode>
    <AdobeCode>{0E464A1F-C45C-4C70-971C-AECC8B1274CE}</AdobeCode>
    <AdobeCode>{2E16AAB9-754F-417C-ABD8-61E8AD565281}</AdobeCode>
    <AdobeCode>{D6B1AF6D-7821-4A2B-A672-62CB8645BF91}</AdobeCode>
    <AdobeCode>{B5CB1D32-957F-45B8-868A-2E4ABD7BD70A}</AdobeCode>
    <AdobeCode>{9BDBD912-47A3-428D-BC71-6B8AAFE296E9}</AdobeCode>
    <AdobeCode>{89E154E1-B674-44F7-B9A3-02EEABF337E8}</AdobeCode>
    <AdobeCode>{3FD19309-2492-44C0-90CF-047732052246}</AdobeCode>
    <AdobeCode>{F14A3F0E-EEF4-4337-AE39-E94A2B128AB7}</AdobeCode>
    <AdobeCode>{5EC5B1E8-AB33-443E-8C04-FA8D94337338}</AdobeCode>
    <AdobeCode>{C2A3FDFE-692B-4C8C-854A-FEA09393B1F7}</AdobeCode>
    <AdobeCode>{1608EB80-797C-49C2-AE60-9041407EF3A4}</AdobeCode>
    <AdobeCode>{134FA147-A102-40A8-B30D-99D0CF7CD754}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeAPE3.3_Win64_NoLocale">
    <DisplayName>Adobe Player for Embedding x64 3.3</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="24926896" sysDriveSize="0"><Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>24926896</TotalSize>
      <MaxPathComponent>adbeapeengine.dll</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="1" name="Assets1_1" size="24926896"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">3.3</Value>
      <Value lang="be_BY">3.3</Value>
      <Value lang="bg_BG">3.3</Value>
      <Value lang="ca_ES">3.3</Value>
      <Value lang="cs_CZ">3.3</Value>
      <Value lang="da_DK">3.3</Value>
      <Value lang="de_DE">3.3</Value>
      <Value lang="el_GR">3.3</Value>
      <Value lang="en_GB">3.3</Value>
      <Value lang="en_MX">3.3</Value>
      <Value lang="en_US">3.3</Value>
      <Value lang="en_XC">3.3</Value>
      <Value lang="en_XM">3.3</Value>
      <Value lang="es_ES">3.3</Value>
      <Value lang="es_MX">3.3</Value>
      <Value lang="es_QM">3.3</Value>
      <Value lang="et_EE">3.3</Value>
      <Value lang="fi_FI">3.3</Value>
      <Value lang="fr_CA">3.3</Value>
      <Value lang="fr_FR">3.3</Value>
      <Value lang="fr_MX">3.3</Value>
      <Value lang="fr_XM">3.3</Value>
      <Value lang="he_IL">3.3</Value>
      <Value lang="hi_IN">3.3</Value>
      <Value lang="hr_HR">3.3</Value>
      <Value lang="hu_HU">3.3</Value>
      <Value lang="is_IS">3.3</Value>
      <Value lang="it_IT">3.3</Value>
      <Value lang="ja_JP">3.3</Value>
      <Value lang="ko_KR">3.3</Value>
      <Value lang="lt_LT">3.3</Value>
      <Value lang="lv_LV">3.3</Value>
      <Value lang="mk_MK">3.3</Value>
      <Value lang="nb_NO">3.3</Value>
      <Value lang="nl_NL">3.3</Value>
      <Value lang="nn_NO">3.3</Value>
      <Value lang="no_NO">3.3</Value>
      <Value lang="pl_PL">3.3</Value>
      <Value lang="pt_BR">3.3</Value>
      <Value lang="ro_RO">3.3</Value>
      <Value lang="ru_RU">3.3</Value>
      <Value lang="sh_YU">3.3</Value>
      <Value lang="sk_SK">3.3</Value>
      <Value lang="sl_SI">3.3</Value>
      <Value lang="sq_AL">3.3</Value>
      <Value lang="sv_SE">3.3</Value>
      <Value lang="th_TH">3.3</Value>
      <Value lang="tr_TR">3.3</Value>
      <Value lang="uk_UA">3.3</Value>
      <Value lang="vi_VN">3.3</Value>
      <Value lang="zh_CN">3.3</Value>
      <Value lang="zh_TW">3.3</Value>
      <Value lang="en_AE">3.3</Value>
      <Value lang="en_IL">3.3</Value>
      <Value lang="fr_MA">3.3</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="be_BY">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="bg_BG">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="ca_ES">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="cs_CZ">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="da_DK">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="de_DE">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="el_GR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_GB">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_MX">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_US">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_XC">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_XM">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="es_ES">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="es_MX">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="es_QM">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="et_EE">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fi_FI">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fr_CA">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fr_FR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fr_MX">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fr_XM">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="he_IL">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="hi_IN">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="hr_HR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="hu_HU">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="is_IS">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="it_IT">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="ja_JP">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="ko_KR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="lt_LT">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="lv_LV">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="mk_MK">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="nb_NO">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="nl_NL">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="nn_NO">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="no_NO">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="pl_PL">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="pt_BR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="ro_RO">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="ru_RU">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="sh_YU">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="sk_SK">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="sl_SI">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="sq_AL">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="sv_SE">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="th_TH">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="tr_TR">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="uk_UA">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="vi_VN">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="zh_CN">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="zh_TW">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_AE">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="en_IL">Adobe Player for Embedding x64 3.3</Value>
      <Value lang="fr_MA">Adobe Player for Embedding x64 3.3</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0", "ChannelID", "AdobeAPE3.3_Win64_NoLocale")
INSERT INTO PayloadData VALUES("{9254D539-549A-41DD-A7DA-251766F2B76F}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeAPE3.3_Win64_NoLocale">
    <DisplayName>Adobe Player for Embedding x64 3.3</DisplayName>
  </Channel>')
