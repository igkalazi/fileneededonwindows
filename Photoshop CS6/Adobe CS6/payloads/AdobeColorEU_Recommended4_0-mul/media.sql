CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{F4D004BD-7296-4F46-B59B-B08D742F2EAB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{FB1816A6-4095-4C3B-9319-4CAA16B323FA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{439A3084-00D2-40A3-B5D5-D46BE6D9B3AB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{AE38323D-48A9-4AD0-AF38-A7E98329953C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{B0575A9E-EB0B-4B98-A913-AF87786A9A72}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{D831C261-B937-45FA-9390-5BF475E7C9FF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{34BC0EF8-39D1-4A4C-8AC2-FD4918AF88F9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{00B4F46A-8CC1-4F08-93B9-03D7DC581DAA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{2DEE7F2E-12E9-41F3-BB90-74BC43F92411}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "{DF791A87-C25D-4FB3-BF37-D116D7EDA5A9}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}"/>')
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0" , "ValidationSig", "ggTlpuaxGzPGwDosccIhmAKgJ3RVWkkLPfUM7S3YwiWSeToFWVOv5b617UMmFRB58I9s4aytD7vpE20fMg0wZc1eCnMjoxgWCYQlaornFRoqkO/MAvBr9Oy0KbwYR9kBvqOtKu+QOaqR+uhN4w5UztlPvGWh3cBbbyxcJWLEZuvH4WcXxw7eiVAFjlN3ApuyhlR5s6IuAqgIBNIgIl0GtMHXFRxK9yH83MMmSXfs4Ss5uNJvVIRqN52iK21g1XHAn6GuOL1uVZolzPmqflN8VEXFWPy7Hn6i8pbSI0Dcq8bJTDLf0eaAllwQCnIKConC1DD8JMCcSlHcJHH1xPP23dUHl5kjNjFvFTgdk7OYbM5Ahd0r1pMT0ft1DMkCxzBFq3eLpV7PUy3ocb+S3BKT15HpoNSdVeL4EEgnn5VDsYQ4YXuNmzk+iOH4cVJ82oUhR9o8yda3WBDAMkZDnW0anElpOebvKWKwljhX0eXn69/CWwp5esDekINf9ZyB58P7")
INSERT INTO Payloads VALUES	("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "CoreTech", "AdobeColorEU CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-15 16:26:44.897000</Property>
    <Property name="TargetName">AdobeColorEU_Recommended4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}</Property>
    <Property name="ProductName">Adobe Color EU Recommended Settings CS6</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages>
    <Language>ar_AE</Language>
    <Language>cs_CZ</Language>
    <Language>da_DK</Language>
    <Language>de_DE</Language>
    <Language>el_GR</Language>
    <Language>en_GB</Language>
    <Language>en_AE</Language>
    <Language>en_IL</Language>
    <Language>es_ES</Language>
    <Language>fi_FI</Language>
    <Language>fr_FR</Language>
    <Language>fr_MA</Language>
    <Language>he_IL</Language>
    <Language>hu_HU</Language>
    <Language>it_IT</Language>
    <Language>nb_NO</Language>
    <Language>nl_NL</Language>
    <Language>pl_PL</Language>
    <Language>pt_BR</Language>
    <Language>ro_RO</Language>
    <Language>ru_RU</Language>
    <Language>sv_SE</Language>
    <Language>tr_TR</Language>
    <Language>uk_UA</Language>
    <Language>es_MX</Language>
  </Languages><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorEU CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{F4D004BD-7296-4F46-B59B-B08D742F2EAB}</AdobeCode>
    <AdobeCode>{FB1816A6-4095-4C3B-9319-4CAA16B323FA}</AdobeCode>
    <AdobeCode>{439A3084-00D2-40A3-B5D5-D46BE6D9B3AB}</AdobeCode>
    <AdobeCode>{AE38323D-48A9-4AD0-AF38-A7E98329953C}</AdobeCode>
    <AdobeCode>{B0575A9E-EB0B-4B98-A913-AF87786A9A72}</AdobeCode>
    <AdobeCode>{D831C261-B937-45FA-9390-5BF475E7C9FF}</AdobeCode>
    <AdobeCode>{34BC0EF8-39D1-4A4C-8AC2-FD4918AF88F9}</AdobeCode>
    <AdobeCode>{00B4F46A-8CC1-4F08-93B9-03D7DC581DAA}</AdobeCode>
    <AdobeCode>{2DEE7F2E-12E9-41F3-BB90-74BC43F92411}</AdobeCode>
    <AdobeCode>{DF791A87-C25D-4FB3-BF37-D116D7EDA5A9}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorEURecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color EU Recommended Settings CS6</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="2023544"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>2023544</TotalSize>
      <MaxPathComponent>/Color/Settings/Recommended\Europe General Purpose 3.csf</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="2023544"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="es_MX">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="cs_CZ">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="da_DK">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="de_DE">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="el_GR">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="en_GB">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="en_AE">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="en_IL">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="es_ES">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="fi_FI">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="fr_FR">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="fr_MA">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="he_IL">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="hu_HU">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="it_IT">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="nb_NO">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="nl_NL">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="pl_PL">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="pt_BR">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="ro_RO">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="ru_RU">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="sv_SE">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="tr_TR">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="uk_UA">Adobe Color EU Recommended Settings CS6</Value>
      <Value lang="es_MX">Adobe Color EU Recommended Settings CS6</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0", "ChannelID", "AdobeColorEURecommendedSettingsCS6-4.0")
INSERT INTO PayloadData VALUES("{0E0AA043-65AC-4A20-AAD6-9B4C7667309B}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorEURecommendedSettingsCS6-4.0">
    <DisplayName>Adobe Color EU Recommended Settings CS6</DisplayName>
  </Channel>')
