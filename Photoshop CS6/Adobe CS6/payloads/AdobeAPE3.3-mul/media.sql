CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D160CBDA-589A-490D-82CD-8739EAE387C3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{A4AF775D-63AA-48ED-AB73-28ED48A397A1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7CE32F61-70B1-4A5E-9DA0-5F138CA3BD47}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{DC130F83-DE05-4030-8CCC-A967CB1EB088}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{9DF069F7-C06C-4928-81DB-4437F7AC2D59}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{14DC1CA7-FB19-4F60-88AF-6AEAFD46EC4C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D2FAB121-1F36-4D00-ACC6-81F410D2D5AF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{1B739947-0F0A-45B1-826F-0A70489EE585}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{CDFEE695-26FC-4B37-9B2D-C3D51B72EA90}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F8662495-3225-4F20-BD66-BD9891B70E88}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E8C5A221-7E93-45EC-87B7-F5BC61D9A866}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{C2C81B45-96D6-48B1-8FFE-B4CFA555ECAE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E454D44E-87A8-4D8D-A57E-F09C35051240}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8DE0BA8D-FBC7-4C5D-8F9A-6AE50C4CD70F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F6F25C64-4DEF-4D50-8E4B-AB8F1036CC73}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{35B12B4C-5184-4C15-995D-BC9E140EA155}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{62C175F3-0215-4540-AADE-B4F5D3255462}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{53933B10-B556-4B50-9115-2012FF7EC664}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{960A175C-1FEF-4F80-BD83-A07325AD44CE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{DF17A9FA-EA86-4067-9813-2948BC0ED863}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{3AA56F1F-3400-4B0F-B36F-E92CE5A25D37}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{2A99C098-204B-4F91-BB7F-343B3ABF773E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{A391B212-DBAC-46FA-96E0-E833BF7AECD6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{1D5A6E0D-020C-485B-BD4B-3BC9FFA84C90}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{04BED426-9E48-459B-8F30-B2D2EB73028E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{29AF9A3C-158D-4EB8-9338-DF255F436E4C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7F5AAC1D-155E-4B1D-9CE4-4842472CDC1B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{58ACDB5D-22E6-4637-898E-E63193F95BAA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{5AFF8A03-F808-4CFC-9285-E444A489E554}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{1E560997-E139-4A94-ABC9-7856F1657A6A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{79BDAD77-647B-498C-A924-B8EDB77B0742}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{0193046A-41D7-4C03-928F-429014594D26}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{9BD3B9B9-EF7D-4941-A600-20079EA4142F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{EA6912AF-DC2A-4FF4-A057-4474CB2F1623}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{0D266A61-EA8F-45ED-B6DF-8A29C458BBEB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{30C52622-3587-40C6-A6FD-2CCE3A405E4E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{47DA0699-AEC3-4ED6-B16C-FC5BF356FEB7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{04C6BC6C-6BC5-4078-902F-516E4A8A2373}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7427978E-5097-4727-B03B-DF53E942DEDC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8FAA540B-B5C6-4821-9350-865EBD9BA5BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{90B705A5-47B1-4D6C-9713-A4355BF325BB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{2253BBBE-A966-4DB4-B41C-45BAB153F2D2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{928D13BE-49C5-457F-ACBB-98424CA23C76}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{773F2265-076A-4085-ABF9-DA6E52EBFD7A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{34007122-E4C0-42CF-BF59-C25467FB5BB5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8BC45445-168F-4AF9-9F5E-D68F329FAD9C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{5649AB72-4CFD-4DC1-A8B0-8C87CC311909}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E63C957A-8F6D-45D5-AEA4-0DFCA13B1A57}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{3AAEC501-B093-4AD4-9A43-6DF5B5A2ED8A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{C4DA7E57-7C0D-4C37-B706-EFBC803228DC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{52383D2A-6DD7-4F72-A5D3-2C7190B51234}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{5D811331-CDDD-4F71-B7F2-55EAA50F19E5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{A96BC928-2E65-4993-8F63-3E1B8AD43222}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{C85FD0E0-352A-4E01-B7B0-6CF952DDA959}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{BBE7434F-5641-47DB-ADC9-7C401C71C367}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{301372EA-6C20-48BC-8F11-A9D15DFD7F8D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{FB56D9AB-45E6-489E-AF37-0078307AE70E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{DC1A423F-7958-4376-AACA-609693AF8EE3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{29DDB5B2-A8DA-40AA-BA36-92A3C9BCA665}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{505165A1-7D69-48F7-B6BA-EC6BB9017FCA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{12DA364F-D2FE-4366-849B-A261EFA216F1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{96A867B6-1761-4ECC-A3D7-4484F42E1259}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F4D467C7-ACD5-4FCC-80AD-094168CF54A9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{95363F91-F3E6-4536-8D2F-7578A02D4C94}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{3B850BE1-E4E4-4815-AC20-51C69F7E3E27}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{BC275928-8CAB-46BE-84C6-DCA1206DAD9B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7DE691D2-92A5-4126-AA53-C3EAF17BA755}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{0F5CB387-22DF-4A3D-BED9-0FDC7DE6A8A6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{79D3C83F-B9E9-4638-AD97-7A9C7AA96379}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E385D53E-3ABA-428B-A8A3-56F7701DCE22}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8A5D78E9-77AC-442C-BA3B-1F666B2C1BD6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E5999DD2-56C1-42A5-A60E-14C75A63F828}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7455BA92-B2FF-4086-86A1-54DCDE29CF8B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{AFF5547C-21AB-4AE7-82F8-7AD1B7D8B316}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{61699F44-C548-48B7-A207-3D5A15055918}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{20F9433F-D5F0-4A18-ABCC-C4FAFBC95497}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{EDD45B35-BEC4-4D03-85F7-4D9EDA509844}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E697893E-8762-44AD-B270-9AD6106DC356}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D2F880AD-8F64-413F-B132-5B21FE6EDD76}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F09E0566-B540-44EE-BAC2-D3DA9EB80DC2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{533906F7-3E24-4AB2-A99C-7C7F5C25579F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D135625B-E5D2-4AC8-93C1-CB8BC04F65D1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{70F13FEC-1773-4232-8CAA-AF5BBFCB2779}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F73D47B7-1F52-49CF-87F2-DB3A646D7C5E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{42E4A7DE-F7F7-4E28-8F76-91A3BC690C2E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E9DCF218-0CBC-4DAA-B4AE-3CBFE1D69981}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E122F8F7-ED94-4A1B-848A-55177191003A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{E6FF6EB3-A3FD-4931-9E96-4F2E6C1F4B25}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{79FE754A-3D51-4AAF-AD4F-35CEA3FE7805}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D5197A44-88A8-41C7-8B97-75FAEFF0B3E7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{AF0937C9-0159-47EF-A406-87E2A7F36EFF}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{0DB44A1F-BBCF-4CEF-AEB3-C982BDAD14BC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{26E0A4AD-9F7B-43D4-9824-AD95B0350A91}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8EB8C90E-BDDC-43D7-90F5-B868541AA9EB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{C71A5D88-F6E9-4600-A0F3-9C702A4AEED6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{82891A21-68A7-476F-A1A1-823A9F7FD71A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7EAA850E-FDDC-402B-832B-98A308047AF2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{A10AD292-81CF-4F21-9858-64C6994CF5C3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{D25C5E75-BFD5-4F33-B2BF-A5B7A4D08BC2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{68F1CDBA-0F20-4BDC-A41F-8A0DEAFFD961}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{48DEDAC4-011E-477F-BA90-2FC68A9DCD07}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{022ED072-C366-4D43-B40B-6C7B42F2CB30}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{501B3B5A-8713-4149-B7D1-E5DF7D880F45}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{460D157F-FAEC-4ABE-BE41-F9DA5B592B52}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{1D156F69-6E08-46AF-9442-3F2B9002F44A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7174DE0E-387A-4FA5-999C-F2CD4192CD5A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{AF8EB7A0-2C26-4529-A901-5D9B01202DB2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{046C40CC-F9D6-4EE7-834E-5487575DA308}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{7DE8104C-7D0D-4179-9D76-42D885C40C0F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{94E76DC5-2E31-4DF5-B562-EF0532D30DFA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{CFF04F47-288C-44B9-8AB4-133E5EB71F7A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{9A2EB412-CCB7-4DE9-BDE5-C8B638483CBA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{248A98B4-505A-43D7-B3FB-0E1DD48F3C24}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{3DBF0734-D30C-47C4-AC0F-892ECC287398}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{AEEC097C-E790-4E85-9669-12C2F5CC1CDB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{5C815088-3EE6-4F0B-9270-A944A443D3CD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{917ED490-432E-417B-85A1-C5627BF61B1A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{3890DE52-D2B9-44F8-9366-1469461BEBA0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{DAC43449-7CD3-4C64-A076-53886811D046}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{90ED371D-A8D3-4B3E-B359-1151B2CA53B0}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{F4A31F48-490C-425F-8033-388D4B0B234D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{B086C536-2E80-4436-9264-4BFB91409A1C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{C0488528-4283-4EB0-9E5A-C2695608DBC9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{8EA770B5-A3BA-4616-9AEF-25E3D3C82106}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{397874B0-8474-450E-81CD-D4B530D7496F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{993E1663-4971-4E5D-A775-8308F37DC780}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{556D78B8-031F-446B-90A9-CE4AFD8B2B7E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "{4D6B8632-BB34-400C-A65D-D64F82B73B05}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{354D20E6-A25F-4728-9DA6-C9003D8F2928}"><File><InstallPath>[INSTALLDIR]\adbeapecore.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\adbeapeengine.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\NPSWF32.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File><File><InstallPath>[INSTALLDIR]\WebKit.dll</InstallPath><SignatureType>DigitalSignature</SignatureType><SignatureInfo>Adobe Systems Incorporated{|}VeriSign Class 3 Code Signing 2010 CA{|}VeriSign Class 3 Public Primary Certification Authority - G5{|}Class 3 Public Primary Certification Authority</SignatureInfo></File></ValidationInfo>')
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0" , "ValidationSig", "UUthPKskuCcKGyjmaGLpkrbgIdcu92q0Mh2AWaklX5QsqAhe3mkqz8o2GinhHRyzxS7IpP6uh+C3zuoMacxHV40790p0phHfP5D5FzOZVu8npobcs8t02DuZdqI72B/X8iADp8txmzd1Qdvfn82fluAWBu3kjdEl6nMilvmbVHwkK4AcAlKK9fNPEowEPO68fCyk8zRnCwW6K4ID6mvUUz9GJZl9ozsJraIZwFQ2/F38z6wsDQkLSqzksoOBloDiw0+urS3FobQDwT8VACoJ1eVB4xZIdEz1j3HjI8CJDlglOYXpaKfhe3iJPB0/CDAanNbEQfmb1dUIx03zvXDZHXCZ6eihLwfc35OLueUUFCgH+gi/TWFLK3NI4nARX73pzIgBD2yhifcWXkUnw9SlvcGsx1hvUjlygvPT1BJKQ4lDIEp7wJZOWD6UwUsIBHDp+lixGkswO/pV0V7y1m/uuwjIrgpB2TVzEjURaWV+/KgsDOdBj2t+Xc0n+pBEdUjy")
INSERT INTO Payloads VALUES	("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "CoreTech", "Adobe Player for Embedding 3.3", "3.3", "normal")
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-25 12:43:02.938000</Property>
    <Property name="TargetName">AdobeAPE3.3-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{354D20E6-A25F-4728-9DA6-C9003D8F2928}</Property>
    <Property name="ProductName">Adobe Player for Embedding 3.3</Property>
    <Property name="ProductVersion">3.3</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\APE\3.3</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>Adobe Player for Embedding 3.3</ProductName>
	<ProductVersion>3.3</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{D160CBDA-589A-490D-82CD-8739EAE387C3}</AdobeCode>
    <AdobeCode>{A4AF775D-63AA-48ED-AB73-28ED48A397A1}</AdobeCode>
    <AdobeCode>{7CE32F61-70B1-4A5E-9DA0-5F138CA3BD47}</AdobeCode>
    <AdobeCode>{DC130F83-DE05-4030-8CCC-A967CB1EB088}</AdobeCode>
    <AdobeCode>{9DF069F7-C06C-4928-81DB-4437F7AC2D59}</AdobeCode>
    <AdobeCode>{14DC1CA7-FB19-4F60-88AF-6AEAFD46EC4C}</AdobeCode>
    <AdobeCode>{D2FAB121-1F36-4D00-ACC6-81F410D2D5AF}</AdobeCode>
    <AdobeCode>{1B739947-0F0A-45B1-826F-0A70489EE585}</AdobeCode>
    <AdobeCode>{CDFEE695-26FC-4B37-9B2D-C3D51B72EA90}</AdobeCode>
    <AdobeCode>{F8662495-3225-4F20-BD66-BD9891B70E88}</AdobeCode>
    <AdobeCode>{E8C5A221-7E93-45EC-87B7-F5BC61D9A866}</AdobeCode>
    <AdobeCode>{C2C81B45-96D6-48B1-8FFE-B4CFA555ECAE}</AdobeCode>
    <AdobeCode>{E454D44E-87A8-4D8D-A57E-F09C35051240}</AdobeCode>
    <AdobeCode>{8DE0BA8D-FBC7-4C5D-8F9A-6AE50C4CD70F}</AdobeCode>
    <AdobeCode>{F6F25C64-4DEF-4D50-8E4B-AB8F1036CC73}</AdobeCode>
    <AdobeCode>{35B12B4C-5184-4C15-995D-BC9E140EA155}</AdobeCode>
    <AdobeCode>{62C175F3-0215-4540-AADE-B4F5D3255462}</AdobeCode>
    <AdobeCode>{53933B10-B556-4B50-9115-2012FF7EC664}</AdobeCode>
    <AdobeCode>{960A175C-1FEF-4F80-BD83-A07325AD44CE}</AdobeCode>
    <AdobeCode>{DF17A9FA-EA86-4067-9813-2948BC0ED863}</AdobeCode>
    <AdobeCode>{3AA56F1F-3400-4B0F-B36F-E92CE5A25D37}</AdobeCode>
    <AdobeCode>{2A99C098-204B-4F91-BB7F-343B3ABF773E}</AdobeCode>
    <AdobeCode>{A391B212-DBAC-46FA-96E0-E833BF7AECD6}</AdobeCode>
    <AdobeCode>{1D5A6E0D-020C-485B-BD4B-3BC9FFA84C90}</AdobeCode>
    <AdobeCode>{04BED426-9E48-459B-8F30-B2D2EB73028E}</AdobeCode>
    <AdobeCode>{29AF9A3C-158D-4EB8-9338-DF255F436E4C}</AdobeCode>
    <AdobeCode>{7F5AAC1D-155E-4B1D-9CE4-4842472CDC1B}</AdobeCode>
    <AdobeCode>{58ACDB5D-22E6-4637-898E-E63193F95BAA}</AdobeCode>
    <AdobeCode>{5AFF8A03-F808-4CFC-9285-E444A489E554}</AdobeCode>
    <AdobeCode>{1E560997-E139-4A94-ABC9-7856F1657A6A}</AdobeCode>
    <AdobeCode>{79BDAD77-647B-498C-A924-B8EDB77B0742}</AdobeCode>
    <AdobeCode>{0193046A-41D7-4C03-928F-429014594D26}</AdobeCode>
    <AdobeCode>{9BD3B9B9-EF7D-4941-A600-20079EA4142F}</AdobeCode>
    <AdobeCode>{EA6912AF-DC2A-4FF4-A057-4474CB2F1623}</AdobeCode>
    <AdobeCode>{0D266A61-EA8F-45ED-B6DF-8A29C458BBEB}</AdobeCode>
    <AdobeCode>{30C52622-3587-40C6-A6FD-2CCE3A405E4E}</AdobeCode>
    <AdobeCode>{47DA0699-AEC3-4ED6-B16C-FC5BF356FEB7}</AdobeCode>
    <AdobeCode>{04C6BC6C-6BC5-4078-902F-516E4A8A2373}</AdobeCode>
    <AdobeCode>{7427978E-5097-4727-B03B-DF53E942DEDC}</AdobeCode>
    <AdobeCode>{8FAA540B-B5C6-4821-9350-865EBD9BA5BB}</AdobeCode>
    <AdobeCode>{90B705A5-47B1-4D6C-9713-A4355BF325BB}</AdobeCode>
    <AdobeCode>{2253BBBE-A966-4DB4-B41C-45BAB153F2D2}</AdobeCode>
    <AdobeCode>{928D13BE-49C5-457F-ACBB-98424CA23C76}</AdobeCode>
    <AdobeCode>{773F2265-076A-4085-ABF9-DA6E52EBFD7A}</AdobeCode>
    <AdobeCode>{34007122-E4C0-42CF-BF59-C25467FB5BB5}</AdobeCode>
    <AdobeCode>{8BC45445-168F-4AF9-9F5E-D68F329FAD9C}</AdobeCode>
    <AdobeCode>{5649AB72-4CFD-4DC1-A8B0-8C87CC311909}</AdobeCode>
    <AdobeCode>{E63C957A-8F6D-45D5-AEA4-0DFCA13B1A57}</AdobeCode>
    <AdobeCode>{3AAEC501-B093-4AD4-9A43-6DF5B5A2ED8A}</AdobeCode>
    <AdobeCode>{C4DA7E57-7C0D-4C37-B706-EFBC803228DC}</AdobeCode>
    <AdobeCode>{52383D2A-6DD7-4F72-A5D3-2C7190B51234}</AdobeCode>
    <AdobeCode>{5D811331-CDDD-4F71-B7F2-55EAA50F19E5}</AdobeCode>
    <AdobeCode>{A96BC928-2E65-4993-8F63-3E1B8AD43222}</AdobeCode>
    <AdobeCode>{C85FD0E0-352A-4E01-B7B0-6CF952DDA959}</AdobeCode>
    <AdobeCode>{BBE7434F-5641-47DB-ADC9-7C401C71C367}</AdobeCode>
    <AdobeCode>{301372EA-6C20-48BC-8F11-A9D15DFD7F8D}</AdobeCode>
    <AdobeCode>{FB56D9AB-45E6-489E-AF37-0078307AE70E}</AdobeCode>
    <AdobeCode>{DC1A423F-7958-4376-AACA-609693AF8EE3}</AdobeCode>
    <AdobeCode>{29DDB5B2-A8DA-40AA-BA36-92A3C9BCA665}</AdobeCode>
    <AdobeCode>{505165A1-7D69-48F7-B6BA-EC6BB9017FCA}</AdobeCode>
    <AdobeCode>{12DA364F-D2FE-4366-849B-A261EFA216F1}</AdobeCode>
    <AdobeCode>{96A867B6-1761-4ECC-A3D7-4484F42E1259}</AdobeCode>
    <AdobeCode>{F4D467C7-ACD5-4FCC-80AD-094168CF54A9}</AdobeCode>
    <AdobeCode>{95363F91-F3E6-4536-8D2F-7578A02D4C94}</AdobeCode>
    <AdobeCode>{3B850BE1-E4E4-4815-AC20-51C69F7E3E27}</AdobeCode>
    <AdobeCode>{BC275928-8CAB-46BE-84C6-DCA1206DAD9B}</AdobeCode>
    <AdobeCode>{7DE691D2-92A5-4126-AA53-C3EAF17BA755}</AdobeCode>
    <AdobeCode>{0F5CB387-22DF-4A3D-BED9-0FDC7DE6A8A6}</AdobeCode>
    <AdobeCode>{79D3C83F-B9E9-4638-AD97-7A9C7AA96379}</AdobeCode>
    <AdobeCode>{E385D53E-3ABA-428B-A8A3-56F7701DCE22}</AdobeCode>
    <AdobeCode>{8A5D78E9-77AC-442C-BA3B-1F666B2C1BD6}</AdobeCode>
    <AdobeCode>{E5999DD2-56C1-42A5-A60E-14C75A63F828}</AdobeCode>
    <AdobeCode>{7455BA92-B2FF-4086-86A1-54DCDE29CF8B}</AdobeCode>
    <AdobeCode>{AFF5547C-21AB-4AE7-82F8-7AD1B7D8B316}</AdobeCode>
    <AdobeCode>{61699F44-C548-48B7-A207-3D5A15055918}</AdobeCode>
    <AdobeCode>{20F9433F-D5F0-4A18-ABCC-C4FAFBC95497}</AdobeCode>
    <AdobeCode>{EDD45B35-BEC4-4D03-85F7-4D9EDA509844}</AdobeCode>
    <AdobeCode>{E697893E-8762-44AD-B270-9AD6106DC356}</AdobeCode>
    <AdobeCode>{D2F880AD-8F64-413F-B132-5B21FE6EDD76}</AdobeCode>
    <AdobeCode>{F09E0566-B540-44EE-BAC2-D3DA9EB80DC2}</AdobeCode>
    <AdobeCode>{533906F7-3E24-4AB2-A99C-7C7F5C25579F}</AdobeCode>
    <AdobeCode>{D135625B-E5D2-4AC8-93C1-CB8BC04F65D1}</AdobeCode>
    <AdobeCode>{70F13FEC-1773-4232-8CAA-AF5BBFCB2779}</AdobeCode>
    <AdobeCode>{F73D47B7-1F52-49CF-87F2-DB3A646D7C5E}</AdobeCode>
    <AdobeCode>{42E4A7DE-F7F7-4E28-8F76-91A3BC690C2E}</AdobeCode>
    <AdobeCode>{E9DCF218-0CBC-4DAA-B4AE-3CBFE1D69981}</AdobeCode>
    <AdobeCode>{E122F8F7-ED94-4A1B-848A-55177191003A}</AdobeCode>
    <AdobeCode>{E6FF6EB3-A3FD-4931-9E96-4F2E6C1F4B25}</AdobeCode>
    <AdobeCode>{79FE754A-3D51-4AAF-AD4F-35CEA3FE7805}</AdobeCode>
    <AdobeCode>{D5197A44-88A8-41C7-8B97-75FAEFF0B3E7}</AdobeCode>
    <AdobeCode>{AF0937C9-0159-47EF-A406-87E2A7F36EFF}</AdobeCode>
    <AdobeCode>{0DB44A1F-BBCF-4CEF-AEB3-C982BDAD14BC}</AdobeCode>
    <AdobeCode>{26E0A4AD-9F7B-43D4-9824-AD95B0350A91}</AdobeCode>
    <AdobeCode>{8EB8C90E-BDDC-43D7-90F5-B868541AA9EB}</AdobeCode>
    <AdobeCode>{C71A5D88-F6E9-4600-A0F3-9C702A4AEED6}</AdobeCode>
    <AdobeCode>{82891A21-68A7-476F-A1A1-823A9F7FD71A}</AdobeCode>
    <AdobeCode>{7EAA850E-FDDC-402B-832B-98A308047AF2}</AdobeCode>
    <AdobeCode>{A10AD292-81CF-4F21-9858-64C6994CF5C3}</AdobeCode>
    <AdobeCode>{D25C5E75-BFD5-4F33-B2BF-A5B7A4D08BC2}</AdobeCode>
    <AdobeCode>{68F1CDBA-0F20-4BDC-A41F-8A0DEAFFD961}</AdobeCode>
    <AdobeCode>{48DEDAC4-011E-477F-BA90-2FC68A9DCD07}</AdobeCode>
    <AdobeCode>{022ED072-C366-4D43-B40B-6C7B42F2CB30}</AdobeCode>
    <AdobeCode>{501B3B5A-8713-4149-B7D1-E5DF7D880F45}</AdobeCode>
    <AdobeCode>{460D157F-FAEC-4ABE-BE41-F9DA5B592B52}</AdobeCode>
    <AdobeCode>{1D156F69-6E08-46AF-9442-3F2B9002F44A}</AdobeCode>
    <AdobeCode>{7174DE0E-387A-4FA5-999C-F2CD4192CD5A}</AdobeCode>
    <AdobeCode>{AF8EB7A0-2C26-4529-A901-5D9B01202DB2}</AdobeCode>
    <AdobeCode>{046C40CC-F9D6-4EE7-834E-5487575DA308}</AdobeCode>
    <AdobeCode>{7DE8104C-7D0D-4179-9D76-42D885C40C0F}</AdobeCode>
    <AdobeCode>{94E76DC5-2E31-4DF5-B562-EF0532D30DFA}</AdobeCode>
    <AdobeCode>{CFF04F47-288C-44B9-8AB4-133E5EB71F7A}</AdobeCode>
    <AdobeCode>{9A2EB412-CCB7-4DE9-BDE5-C8B638483CBA}</AdobeCode>
    <AdobeCode>{248A98B4-505A-43D7-B3FB-0E1DD48F3C24}</AdobeCode>
    <AdobeCode>{3DBF0734-D30C-47C4-AC0F-892ECC287398}</AdobeCode>
    <AdobeCode>{AEEC097C-E790-4E85-9669-12C2F5CC1CDB}</AdobeCode>
    <AdobeCode>{5C815088-3EE6-4F0B-9270-A944A443D3CD}</AdobeCode>
    <AdobeCode>{917ED490-432E-417B-85A1-C5627BF61B1A}</AdobeCode>
    <AdobeCode>{3890DE52-D2B9-44F8-9366-1469461BEBA0}</AdobeCode>
    <AdobeCode>{DAC43449-7CD3-4C64-A076-53886811D046}</AdobeCode>
    <AdobeCode>{90ED371D-A8D3-4B3E-B359-1151B2CA53B0}</AdobeCode>
    <AdobeCode>{F4A31F48-490C-425F-8033-388D4B0B234D}</AdobeCode>
    <AdobeCode>{B086C536-2E80-4436-9264-4BFB91409A1C}</AdobeCode>
    <AdobeCode>{C0488528-4283-4EB0-9E5A-C2695608DBC9}</AdobeCode>
    <AdobeCode>{8EA770B5-A3BA-4616-9AEF-25E3D3C82106}</AdobeCode>
    <AdobeCode>{397874B0-8474-450E-81CD-D4B530D7496F}</AdobeCode>
    <AdobeCode>{993E1663-4971-4E5D-A775-8308F37DC780}</AdobeCode>
    <AdobeCode>{556D78B8-031F-446B-90A9-CE4AFD8B2B7E}</AdobeCode>
    <AdobeCode>{4D6B8632-BB34-400C-A65D-D64F82B73B05}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeAPE3.3_Win_NoLocale">
    <DisplayName>Adobe Player for Embedding 3.3</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="19904176" sysDriveSize="0"><Destination>
      <Root>[INSTALLDIR]</Root>
      <TotalSize>19904176</TotalSize>
      <MaxPathComponent>adbeapeengine.dll</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="1" name="Assets1_1" size="19904176"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">3.3</Value>
      <Value lang="be_BY">3.3</Value>
      <Value lang="bg_BG">3.3</Value>
      <Value lang="ca_ES">3.3</Value>
      <Value lang="cs_CZ">3.3</Value>
      <Value lang="da_DK">3.3</Value>
      <Value lang="de_DE">3.3</Value>
      <Value lang="el_GR">3.3</Value>
      <Value lang="en_GB">3.3</Value>
      <Value lang="en_MX">3.3</Value>
      <Value lang="en_US">3.3</Value>
      <Value lang="en_XC">3.3</Value>
      <Value lang="en_XM">3.3</Value>
      <Value lang="es_ES">3.3</Value>
      <Value lang="es_MX">3.3</Value>
      <Value lang="es_QM">3.3</Value>
      <Value lang="et_EE">3.3</Value>
      <Value lang="fi_FI">3.3</Value>
      <Value lang="fr_CA">3.3</Value>
      <Value lang="fr_FR">3.3</Value>
      <Value lang="fr_MX">3.3</Value>
      <Value lang="fr_XM">3.3</Value>
      <Value lang="he_IL">3.3</Value>
      <Value lang="hi_IN">3.3</Value>
      <Value lang="hr_HR">3.3</Value>
      <Value lang="hu_HU">3.3</Value>
      <Value lang="is_IS">3.3</Value>
      <Value lang="it_IT">3.3</Value>
      <Value lang="ja_JP">3.3</Value>
      <Value lang="ko_KR">3.3</Value>
      <Value lang="lt_LT">3.3</Value>
      <Value lang="lv_LV">3.3</Value>
      <Value lang="mk_MK">3.3</Value>
      <Value lang="nb_NO">3.3</Value>
      <Value lang="nl_NL">3.3</Value>
      <Value lang="nn_NO">3.3</Value>
      <Value lang="no_NO">3.3</Value>
      <Value lang="pl_PL">3.3</Value>
      <Value lang="pt_BR">3.3</Value>
      <Value lang="ro_RO">3.3</Value>
      <Value lang="ru_RU">3.3</Value>
      <Value lang="sh_YU">3.3</Value>
      <Value lang="sk_SK">3.3</Value>
      <Value lang="sl_SI">3.3</Value>
      <Value lang="sq_AL">3.3</Value>
      <Value lang="sv_SE">3.3</Value>
      <Value lang="th_TH">3.3</Value>
      <Value lang="tr_TR">3.3</Value>
      <Value lang="uk_UA">3.3</Value>
      <Value lang="vi_VN">3.3</Value>
      <Value lang="zh_CN">3.3</Value>
      <Value lang="zh_TW">3.3</Value>
      <Value lang="en_AE">3.3</Value>
      <Value lang="en_IL">3.3</Value>
      <Value lang="fr_MA">3.3</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe Player for Embedding 3.3</Value>
      <Value lang="be_BY">Adobe Player for Embedding 3.3</Value>
      <Value lang="bg_BG">Adobe Player for Embedding 3.3</Value>
      <Value lang="ca_ES">Adobe Player for Embedding 3.3</Value>
      <Value lang="cs_CZ">Adobe Player for Embedding 3.3</Value>
      <Value lang="da_DK">Adobe Player for Embedding 3.3</Value>
      <Value lang="de_DE">Adobe Player for Embedding 3.3</Value>
      <Value lang="el_GR">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_GB">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_MX">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_US">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_XC">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_XM">Adobe Player for Embedding 3.3</Value>
      <Value lang="es_ES">Adobe Player for Embedding 3.3</Value>
      <Value lang="es_MX">Adobe Player for Embedding 3.3</Value>
      <Value lang="es_QM">Adobe Player for Embedding 3.3</Value>
      <Value lang="et_EE">Adobe Player for Embedding 3.3</Value>
      <Value lang="fi_FI">Adobe Player for Embedding 3.3</Value>
      <Value lang="fr_CA">Adobe Player for Embedding 3.3</Value>
      <Value lang="fr_FR">Adobe Player for Embedding 3.3</Value>
      <Value lang="fr_MX">Adobe Player for Embedding 3.3</Value>
      <Value lang="fr_XM">Adobe Player for Embedding 3.3</Value>
      <Value lang="he_IL">Adobe Player for Embedding 3.3</Value>
      <Value lang="hi_IN">Adobe Player for Embedding 3.3</Value>
      <Value lang="hr_HR">Adobe Player for Embedding 3.3</Value>
      <Value lang="hu_HU">Adobe Player for Embedding 3.3</Value>
      <Value lang="is_IS">Adobe Player for Embedding 3.3</Value>
      <Value lang="it_IT">Adobe Player for Embedding 3.3</Value>
      <Value lang="ja_JP">Adobe Player for Embedding 3.3</Value>
      <Value lang="ko_KR">Adobe Player for Embedding 3.3</Value>
      <Value lang="lt_LT">Adobe Player for Embedding 3.3</Value>
      <Value lang="lv_LV">Adobe Player for Embedding 3.3</Value>
      <Value lang="mk_MK">Adobe Player for Embedding 3.3</Value>
      <Value lang="nb_NO">Adobe Player for Embedding 3.3</Value>
      <Value lang="nl_NL">Adobe Player for Embedding 3.3</Value>
      <Value lang="nn_NO">Adobe Player for Embedding 3.3</Value>
      <Value lang="no_NO">Adobe Player for Embedding 3.3</Value>
      <Value lang="pl_PL">Adobe Player for Embedding 3.3</Value>
      <Value lang="pt_BR">Adobe Player for Embedding 3.3</Value>
      <Value lang="ro_RO">Adobe Player for Embedding 3.3</Value>
      <Value lang="ru_RU">Adobe Player for Embedding 3.3</Value>
      <Value lang="sh_YU">Adobe Player for Embedding 3.3</Value>
      <Value lang="sk_SK">Adobe Player for Embedding 3.3</Value>
      <Value lang="sl_SI">Adobe Player for Embedding 3.3</Value>
      <Value lang="sq_AL">Adobe Player for Embedding 3.3</Value>
      <Value lang="sv_SE">Adobe Player for Embedding 3.3</Value>
      <Value lang="th_TH">Adobe Player for Embedding 3.3</Value>
      <Value lang="tr_TR">Adobe Player for Embedding 3.3</Value>
      <Value lang="uk_UA">Adobe Player for Embedding 3.3</Value>
      <Value lang="vi_VN">Adobe Player for Embedding 3.3</Value>
      <Value lang="zh_CN">Adobe Player for Embedding 3.3</Value>
      <Value lang="zh_TW">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_AE">Adobe Player for Embedding 3.3</Value>
      <Value lang="en_IL">Adobe Player for Embedding 3.3</Value>
      <Value lang="fr_MA">Adobe Player for Embedding 3.3</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0", "ChannelID", "AdobeAPE3.3_Win_NoLocale")
INSERT INTO PayloadData VALUES("{354D20E6-A25F-4728-9DA6-C9003D8F2928}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeAPE3.3_Win_NoLocale">
    <DisplayName>Adobe Player for Embedding 3.3</DisplayName>
  </Channel>')
