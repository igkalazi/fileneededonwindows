CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6C4AC966-96AF-4D21-9BFF-D2A5181F6EEA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{16662E8E-2ABA-4B4C-80D0-FE774E175E31}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{5720C908-4308-4789-918A-0BAFD668F0A1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{3AFE24CC-B9D5-474B-9D56-1B36FBEACBAD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{3BC58836-80A7-4692-B4BB-1A12BDE576B1}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{FF18B579-4AF9-42AA-8892-107F1EF38853}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{62B69E87-ADF9-47EB-96DB-112733B28A54}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E7A76DDD-969D-4019-B177-F30F1076C551}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{5244FCAF-8A7E-47E6-B48C-85D313212AEE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6FD09DBC-2581-44CB-BF1D-F5CA63348F78}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{BA046051-4472-41D1-A81B-BB5D327C4312}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{55641D87-9C87-4B13-ADD8-08DF9031361E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{9F81B117-B7BF-41B9-8C57-BACA261F2812}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{8E825664-5EE6-43F4-B8DB-21F3E3B8431B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{C573FC44-94A8-433B-BA0C-4C92BE577AE5}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{BB430810-877B-4286-83F2-4B8A2FE4691E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{1B388CB1-ECE3-400F-A9E1-0589C5FDC17E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{BBCD45D1-5C32-48BB-B11B-21A32206BEE7}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E0F88BB2-7AA9-4739-B539-BC4D6D652019}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{A5CB522F-DC9E-483E-A3CF-C3451CFAAF8A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D0AC5A67-2B6B-4E21-B188-C0E189F7259F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{5176B560-3989-46B6-B84E-EE93B2488C9B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{C461400A-40D8-474E-AE41-FC70C2932ED3}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E20C04A7-5837-4FDF-8A38-5EE56EAC5D0A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{1EACDD91-B7F2-4903-9C87-9A2743F742DA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{76A6CD2F-E088-4DBF-B327-0BBDF7382A07}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{14037C12-3472-44EF-9E35-4D264B326DCA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{B1273BCA-E4DD-451A-A805-116F9798667B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6FB06EB2-3902-4819-A7F1-C8FDB5394D9D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{DB57E7BF-38C7-4770-8561-3167AAB2A6D9}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{F308F7A9-579B-428A-A534-0AB5115B1515}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{563FC2C3-5117-4378-96BF-252CE59B56FE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{CD27021F-E0FB-4E29-AC7D-217B2C783D47}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D8D7B3FC-EDCC-4FB3-A40E-6E17BC0A7DAA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{62DF69EA-B703-4EEB-9694-40A569AC2F40}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{B531B521-25CC-4594-A5D2-E7A9C44E2078}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{45A0848E-05CB-4282-B830-B1C8D75DF00D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{F5BFC583-EB7D-4EF5-BD28-EA02092616FE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{A21EBC00-106D-4055-890B-E0144C79F268}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{406D6D34-74A1-4B3C-83C3-4B640E357E9D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{B4B9F946-370D-4D61-BD49-52A731759A3C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{91A9B03F-1CB2-4218-B48F-6EA2A13469E6}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{AD8ABD89-B4A0-41AB-8568-1479132A3176}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{0DE15155-986A-412C-B038-EE7E77CD9BAD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{DE032719-03B6-451E-AFA0-58E50888FABB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{83875C03-A911-4777-9E81-B2CBB6A3ADAA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{C813DEF2-11A2-412E-AD3B-8E788E77DA8B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6A3636C7-3117-42B3-913B-71A29442FA77}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{63788243-E39C-44FE-9884-817FB8EC5C2B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E30CECE2-241C-4A12-823D-8C443E0C73E8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{75B4A6CB-4B9D-427A-B683-E937C7871743}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{7D6473BF-97E7-458B-B2F7-C82F0B6800F8}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{F1C2F828-40F6-4B1C-8C42-4F184FDD909B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{7A4FC369-80DF-49DA-9AAF-890DA61A40CB}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{422DFC26-BCAD-4B9B-AAA0-94223EBD6251}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6E0EFE8C-DE70-40CF-9615-2627059DDB45}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{A0145B8A-778E-4737-BEBD-3BF4DD5A6146}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E86F4B25-2FFF-41A3-B4CB-9537C02D32EE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{0C32E8D0-37B6-4D06-A31F-FFEB4CD619FC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{E29F23D4-9C36-4DD5-8902-ADA2E5DFDD8E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D141BC9B-5FDF-41F9-AA6E-DF72B728DE85}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{79765FBF-7F59-4B0E-9C7A-C5F18724A64C}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{63312F54-21B1-493D-8086-D529BF918433}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{CB2649D2-4351-40AA-8844-57547F51CA8A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{26F46332-EA41-4EE2-96F6-1EFA4382A383}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{0CB8CEC6-5D91-42A8-B121-79D824DAA964}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{C2AC68B4-C2D5-4505-8385-D937DE467B5B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6C299A65-94ED-4C6B-B0FE-5CBF4ADAE85E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{64AC41BE-8248-4317-9E4A-DE912A94CB1E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{41181380-301E-4354-BD7D-23013DF3B9B4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{53E97A78-B8D8-46B8-A8D1-08160E83403E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{8AB04514-9747-4059-A58D-C12550545D3B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{32F07292-18AE-45DC-A35E-3337C912DBE2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{535A5E10-53B2-4B62-BC81-AFEAF9FEE9B2}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{B751B897-B475-41EB-9AFF-DC79EDD9EA91}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{3D529DE3-78F3-41EC-A319-4E2A86AE6EEC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{376A8D96-AFA0-4636-B019-422DA0F30445}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{4FD2BAA7-E483-4C9B-BE66-A19FDBBF4033}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{15E8EF85-20A4-4694-A9F4-B31F598E7445}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{DDBA6F9D-8AEB-464E-A492-D520777C0325}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{360DE482-E62F-4FDA-9A14-333B016BC81F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D8F56307-565B-4A5E-A05E-B7CD1FE9F9D4}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{0552DD63-B40D-4619-AE24-9E5F9F41564A}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{6AD8397F-A129-4623-976E-FD16EE34DE22}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{DCC01774-52A4-44BB-87DE-9F4381733EDD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{CA572985-D94E-46F8-9313-4ADEA97C4172}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{DE95A9F3-9F4C-4A0E-BA6A-F69F7A49DA8B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{380C2ABD-A99D-4BEA-A0D6-440C0737A880}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{CE552215-D6A1-4A4A-87C0-CF3E2E93CA58}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{94DC8ED0-0D67-4722-98B1-403694112DBC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D3283F63-8FAB-446D-8647-AA49A299C57B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{D9A65D2A-B944-4C3E-9353-7E4A2AFCA72D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{EB1BB675-A51B-4F0D-86D9-812C09B420ED}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{B2741DAA-E75D-4403-A57B-883A8D3DBE67}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{F2EC5799-FEE7-476D-AC1B-E49DDEB39FAD}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{F432BDB2-FDB9-480F-A9C7-A0018D4F0B30}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{A650060D-0808-46C2-9D36-818C45085F5E}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{3C50232C-1970-43B4-8E57-BC314A84AE1D}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "{922FE330-1EA2-4867-9CF0-6158091BA801}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{093DEFC4-542D-4D0A-8162-0592055515F4}"/>')
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0" , "ValidationSig", "bszxVJikzaTblb1IANMaKyx1tHZLJoMqO+QEoTpQoMIBh2baHUYp3O0fQTeDu6ZKHj9U+YKoBRKEwFMp6WjDZ9vqPhn+3NtTw8St59FbsQmypcaPO3DwN1C0BSY4uitX94aTel0c/MRUJ4lLPdZS4FrUwN5Q1v0/FtU36QFdjRq42toHhRo9HiPHRQX2AugPIMOm1vdxgiwuzQ3jz7nC0T6DmpHFfDyL7gV+Oo8G2PTY0WpnzCB82hhjv1ZX/1SfiRh7KhC0/Y/U0kq7c3V/OgYo00AM0SNMk/zEbDzhWSZwc5P8wWORMFguemqokAzvtWZnTVcJ6JEknUs21bJjawWoVorcamLmiA70kkaj0pFEngb534K1pxGz5NH9RxIKlQzwh6HktKYJJ4DwLwErSiopMd/mQgj5Pzf+StLLW2Bi6WccEfWDD6v+XUF8fb8pNcBfH2vWwE0LALb7zEM9/aFvum4J9VfSNn0Yf2ZLGlChUOcwAjJNrObXfpYi2yJm")
INSERT INTO Branding VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "AMTConfig", 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxDb25maWd1cmF0aW9uPg0K
CTxQYXlsb2FkPg0KCQk8IS0tIGRlZmF1bHRBZG9iZUNvZGUgZm9yIGRldmVsb3BtZW50IHVzZS4g
LS0+DQoJCTxEYXRhIGtleT0iZGVmYXVsdEFkb2JlQ29kZSI+QWRvYmVYTVBQYW5lbHMtSW5zdGFs
bGVyLUFkb2JlQ29kZTwvRGF0YT4NCgk8L1BheWxvYWQ+DQo8L0NvbmZpZ3VyYXRpb24+DQo=')
INSERT INTO Payloads VALUES	("{093DEFC4-542D-4D0A-8162-0592055515F4}", "Creative Suites", "Adobe XMP Panels", "4.0", "normal")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2012-01-17 16:50:36</Property>
    <Property name="TargetName">AdobeXMPPanelsAll</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{093DEFC4-542D-4D0A-8162-0592055515F4}</Property>
    <Property name="ProductName">Adobe XMP Panels</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\XMP\Custom File Info Panels\4.0</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>Creative Suites</Family>
		<ProductName>Adobe XMP Panels</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{6C4AC966-96AF-4D21-9BFF-D2A5181F6EEA}</AdobeCode>
    <AdobeCode>{16662E8E-2ABA-4B4C-80D0-FE774E175E31}</AdobeCode>
    <AdobeCode>{5720C908-4308-4789-918A-0BAFD668F0A1}</AdobeCode>
    <AdobeCode>{3AFE24CC-B9D5-474B-9D56-1B36FBEACBAD}</AdobeCode>
    <AdobeCode>{3BC58836-80A7-4692-B4BB-1A12BDE576B1}</AdobeCode>
    <AdobeCode>{FF18B579-4AF9-42AA-8892-107F1EF38853}</AdobeCode>
    <AdobeCode>{62B69E87-ADF9-47EB-96DB-112733B28A54}</AdobeCode>
    <AdobeCode>{E7A76DDD-969D-4019-B177-F30F1076C551}</AdobeCode>
    <AdobeCode>{5244FCAF-8A7E-47E6-B48C-85D313212AEE}</AdobeCode>
    <AdobeCode>{6FD09DBC-2581-44CB-BF1D-F5CA63348F78}</AdobeCode>
    <AdobeCode>{BA046051-4472-41D1-A81B-BB5D327C4312}</AdobeCode>
    <AdobeCode>{55641D87-9C87-4B13-ADD8-08DF9031361E}</AdobeCode>
    <AdobeCode>{9F81B117-B7BF-41B9-8C57-BACA261F2812}</AdobeCode>
    <AdobeCode>{8E825664-5EE6-43F4-B8DB-21F3E3B8431B}</AdobeCode>
    <AdobeCode>{C573FC44-94A8-433B-BA0C-4C92BE577AE5}</AdobeCode>
    <AdobeCode>{BB430810-877B-4286-83F2-4B8A2FE4691E}</AdobeCode>
    <AdobeCode>{1B388CB1-ECE3-400F-A9E1-0589C5FDC17E}</AdobeCode>
    <AdobeCode>{BBCD45D1-5C32-48BB-B11B-21A32206BEE7}</AdobeCode>
    <AdobeCode>{E0F88BB2-7AA9-4739-B539-BC4D6D652019}</AdobeCode>
    <AdobeCode>{A5CB522F-DC9E-483E-A3CF-C3451CFAAF8A}</AdobeCode>
    <AdobeCode>{D0AC5A67-2B6B-4E21-B188-C0E189F7259F}</AdobeCode>
    <AdobeCode>{5176B560-3989-46B6-B84E-EE93B2488C9B}</AdobeCode>
    <AdobeCode>{C461400A-40D8-474E-AE41-FC70C2932ED3}</AdobeCode>
    <AdobeCode>{E20C04A7-5837-4FDF-8A38-5EE56EAC5D0A}</AdobeCode>
    <AdobeCode>{1EACDD91-B7F2-4903-9C87-9A2743F742DA}</AdobeCode>
    <AdobeCode>{76A6CD2F-E088-4DBF-B327-0BBDF7382A07}</AdobeCode>
    <AdobeCode>{14037C12-3472-44EF-9E35-4D264B326DCA}</AdobeCode>
    <AdobeCode>{B1273BCA-E4DD-451A-A805-116F9798667B}</AdobeCode>
    <AdobeCode>{6FB06EB2-3902-4819-A7F1-C8FDB5394D9D}</AdobeCode>
    <AdobeCode>{DB57E7BF-38C7-4770-8561-3167AAB2A6D9}</AdobeCode>
    <AdobeCode>{F308F7A9-579B-428A-A534-0AB5115B1515}</AdobeCode>
    <AdobeCode>{563FC2C3-5117-4378-96BF-252CE59B56FE}</AdobeCode>
    <AdobeCode>{CD27021F-E0FB-4E29-AC7D-217B2C783D47}</AdobeCode>
    <AdobeCode>{D8D7B3FC-EDCC-4FB3-A40E-6E17BC0A7DAA}</AdobeCode>
    <AdobeCode>{62DF69EA-B703-4EEB-9694-40A569AC2F40}</AdobeCode>
    <AdobeCode>{B531B521-25CC-4594-A5D2-E7A9C44E2078}</AdobeCode>
    <AdobeCode>{45A0848E-05CB-4282-B830-B1C8D75DF00D}</AdobeCode>
    <AdobeCode>{F5BFC583-EB7D-4EF5-BD28-EA02092616FE}</AdobeCode>
    <AdobeCode>{A21EBC00-106D-4055-890B-E0144C79F268}</AdobeCode>
    <AdobeCode>{406D6D34-74A1-4B3C-83C3-4B640E357E9D}</AdobeCode>
    <AdobeCode>{B4B9F946-370D-4D61-BD49-52A731759A3C}</AdobeCode>
    <AdobeCode>{91A9B03F-1CB2-4218-B48F-6EA2A13469E6}</AdobeCode>
    <AdobeCode>{AD8ABD89-B4A0-41AB-8568-1479132A3176}</AdobeCode>
    <AdobeCode>{0DE15155-986A-412C-B038-EE7E77CD9BAD}</AdobeCode>
    <AdobeCode>{DE032719-03B6-451E-AFA0-58E50888FABB}</AdobeCode>
    <AdobeCode>{83875C03-A911-4777-9E81-B2CBB6A3ADAA}</AdobeCode>
    <AdobeCode>{C813DEF2-11A2-412E-AD3B-8E788E77DA8B}</AdobeCode>
    <AdobeCode>{6A3636C7-3117-42B3-913B-71A29442FA77}</AdobeCode>
    <AdobeCode>{63788243-E39C-44FE-9884-817FB8EC5C2B}</AdobeCode>
    <AdobeCode>{E30CECE2-241C-4A12-823D-8C443E0C73E8}</AdobeCode>
    <AdobeCode>{75B4A6CB-4B9D-427A-B683-E937C7871743}</AdobeCode>
    <AdobeCode>{7D6473BF-97E7-458B-B2F7-C82F0B6800F8}</AdobeCode>
    <AdobeCode>{F1C2F828-40F6-4B1C-8C42-4F184FDD909B}</AdobeCode>
    <AdobeCode>{7A4FC369-80DF-49DA-9AAF-890DA61A40CB}</AdobeCode>
    <AdobeCode>{422DFC26-BCAD-4B9B-AAA0-94223EBD6251}</AdobeCode>
    <AdobeCode>{6E0EFE8C-DE70-40CF-9615-2627059DDB45}</AdobeCode>
    <AdobeCode>{A0145B8A-778E-4737-BEBD-3BF4DD5A6146}</AdobeCode>
    <AdobeCode>{E86F4B25-2FFF-41A3-B4CB-9537C02D32EE}</AdobeCode>
    <AdobeCode>{0C32E8D0-37B6-4D06-A31F-FFEB4CD619FC}</AdobeCode>
    <AdobeCode>{E29F23D4-9C36-4DD5-8902-ADA2E5DFDD8E}</AdobeCode>
    <AdobeCode>{D141BC9B-5FDF-41F9-AA6E-DF72B728DE85}</AdobeCode>
    <AdobeCode>{79765FBF-7F59-4B0E-9C7A-C5F18724A64C}</AdobeCode>
    <AdobeCode>{63312F54-21B1-493D-8086-D529BF918433}</AdobeCode>
    <AdobeCode>{CB2649D2-4351-40AA-8844-57547F51CA8A}</AdobeCode>
    <AdobeCode>{26F46332-EA41-4EE2-96F6-1EFA4382A383}</AdobeCode>
    <AdobeCode>{0CB8CEC6-5D91-42A8-B121-79D824DAA964}</AdobeCode>
    <AdobeCode>{C2AC68B4-C2D5-4505-8385-D937DE467B5B}</AdobeCode>
    <AdobeCode>{6C299A65-94ED-4C6B-B0FE-5CBF4ADAE85E}</AdobeCode>
    <AdobeCode>{64AC41BE-8248-4317-9E4A-DE912A94CB1E}</AdobeCode>
    <AdobeCode>{41181380-301E-4354-BD7D-23013DF3B9B4}</AdobeCode>
    <AdobeCode>{53E97A78-B8D8-46B8-A8D1-08160E83403E}</AdobeCode>
    <AdobeCode>{8AB04514-9747-4059-A58D-C12550545D3B}</AdobeCode>
    <AdobeCode>{32F07292-18AE-45DC-A35E-3337C912DBE2}</AdobeCode>
    <AdobeCode>{535A5E10-53B2-4B62-BC81-AFEAF9FEE9B2}</AdobeCode>
    <AdobeCode>{B751B897-B475-41EB-9AFF-DC79EDD9EA91}</AdobeCode>
    <AdobeCode>{3D529DE3-78F3-41EC-A319-4E2A86AE6EEC}</AdobeCode>
    <AdobeCode>{376A8D96-AFA0-4636-B019-422DA0F30445}</AdobeCode>
    <AdobeCode>{4FD2BAA7-E483-4C9B-BE66-A19FDBBF4033}</AdobeCode>
    <AdobeCode>{15E8EF85-20A4-4694-A9F4-B31F598E7445}</AdobeCode>
    <AdobeCode>{DDBA6F9D-8AEB-464E-A492-D520777C0325}</AdobeCode>
    <AdobeCode>{360DE482-E62F-4FDA-9A14-333B016BC81F}</AdobeCode>
    <AdobeCode>{D8F56307-565B-4A5E-A05E-B7CD1FE9F9D4}</AdobeCode>
    <AdobeCode>{0552DD63-B40D-4619-AE24-9E5F9F41564A}</AdobeCode>
    <AdobeCode>{6AD8397F-A129-4623-976E-FD16EE34DE22}</AdobeCode>
    <AdobeCode>{DCC01774-52A4-44BB-87DE-9F4381733EDD}</AdobeCode>
    <AdobeCode>{CA572985-D94E-46F8-9313-4ADEA97C4172}</AdobeCode>
    <AdobeCode>{DE95A9F3-9F4C-4A0E-BA6A-F69F7A49DA8B}</AdobeCode>
    <AdobeCode>{380C2ABD-A99D-4BEA-A0D6-440C0737A880}</AdobeCode>
    <AdobeCode>{CE552215-D6A1-4A4A-87C0-CF3E2E93CA58}</AdobeCode>
    <AdobeCode>{94DC8ED0-0D67-4722-98B1-403694112DBC}</AdobeCode>
    <AdobeCode>{D3283F63-8FAB-446D-8647-AA49A299C57B}</AdobeCode>
    <AdobeCode>{D9A65D2A-B944-4C3E-9353-7E4A2AFCA72D}</AdobeCode>
    <AdobeCode>{EB1BB675-A51B-4F0D-86D9-812C09B420ED}</AdobeCode>
    <AdobeCode>{B2741DAA-E75D-4403-A57B-883A8D3DBE67}</AdobeCode>
    <AdobeCode>{F2EC5799-FEE7-476D-AC1B-E49DDEB39FAD}</AdobeCode>
    <AdobeCode>{F432BDB2-FDB9-480F-A9C7-A0018D4F0B30}</AdobeCode>
    <AdobeCode>{A650060D-0808-46C2-9D36-818C45085F5E}</AdobeCode>
    <AdobeCode>{3C50232C-1970-43B4-8E57-BC314A84AE1D}</AdobeCode>
    <AdobeCode>{922FE330-1EA2-4867-9CF0-6158091BA801}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeXMPPanels-4.0">
    <DisplayName>Adobe XMP Panels</DisplayName>
  </Channel><AMTConfig path="[AdobeCommon]\XMP\Custom File Info Panels\4.0\AMT\application.xml" LEID=""/><InstallDestinationMetadata relocatableSize="0" sysDriveSize="4661070"><Destination>
      <Root>[System32Folder]</Root>
      <TotalSize>3</TotalSize>
      <MaxPathComponent>/Macromed/Flash/FlashPlayerTrust\AdobeXMPFileInfo4.cfg</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[_OOBEHome]</Root>
      <TotalSize>0</TotalSize>
      <MaxPathComponent>/PCF\application.xml</MaxPathComponent>
    </Destination>
    <Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>4661067</TotalSize>
      <MaxPathComponent>/XMP/Custom File Info Panels/4.0/panels/description/loc\description_ar_AE.dat</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="4661070"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	</Win32>
</ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">Adobe XMP Panels</Value>
      <Value lang="be_BY">Adobe XMP Panels</Value>
      <Value lang="bg_BG">Adobe XMP Panels</Value>
      <Value lang="ca_ES">Adobe XMP Panels</Value>
      <Value lang="cs_CZ">Adobe XMP Panels</Value>
      <Value lang="da_DK">Adobe XMP Panels</Value>
      <Value lang="de_DE">Adobe XMP Panels</Value>
      <Value lang="el_GR">Adobe XMP Panels</Value>
      <Value lang="en_GB">Adobe XMP Panels</Value>
      <Value lang="en_MX">Adobe XMP Panels</Value>
      <Value lang="en_US">Adobe XMP Panels</Value>
      <Value lang="en_XC">Adobe XMP Panels</Value>
      <Value lang="en_XM">Adobe XMP Panels</Value>
      <Value lang="es_ES">Adobe XMP Panels</Value>
      <Value lang="es_MX">Adobe XMP Panels</Value>
      <Value lang="es_QM">Adobe XMP Panels</Value>
      <Value lang="et_EE">Adobe XMP Panels</Value>
      <Value lang="fi_FI">Adobe XMP Panels</Value>
      <Value lang="fr_CA">Adobe XMP Panels</Value>
      <Value lang="fr_FR">Adobe XMP Panels</Value>
      <Value lang="fr_MX">Adobe XMP Panels</Value>
      <Value lang="fr_XM">Adobe XMP Panels</Value>
      <Value lang="he_IL">Adobe XMP Panels</Value>
      <Value lang="hi_IN">Adobe XMP Panels</Value>
      <Value lang="hr_HR">Adobe XMP Panels</Value>
      <Value lang="hu_HU">Adobe XMP Panels</Value>
      <Value lang="is_IS">Adobe XMP Panels</Value>
      <Value lang="it_IT">Adobe XMP Panels</Value>
      <Value lang="ja_JP">Adobe XMP Panels</Value>
      <Value lang="ko_KR">Adobe XMP Panels</Value>
      <Value lang="lt_LT">Adobe XMP Panels</Value>
      <Value lang="lv_LV">Adobe XMP Panels</Value>
      <Value lang="mk_MK">Adobe XMP Panels</Value>
      <Value lang="nb_NO">Adobe XMP Panels</Value>
      <Value lang="nl_NL">Adobe XMP Panels</Value>
      <Value lang="nn_NO">Adobe XMP Panels</Value>
      <Value lang="no_NO">Adobe XMP Panels</Value>
      <Value lang="pl_PL">Adobe XMP Panels</Value>
      <Value lang="pt_BR">Adobe XMP Panels</Value>
      <Value lang="ro_RO">Adobe XMP Panels</Value>
      <Value lang="ru_RU">Adobe XMP Panels</Value>
      <Value lang="sh_YU">Adobe XMP Panels</Value>
      <Value lang="sk_SK">Adobe XMP Panels</Value>
      <Value lang="sl_SI">Adobe XMP Panels</Value>
      <Value lang="sq_AL">Adobe XMP Panels</Value>
      <Value lang="sv_SE">Adobe XMP Panels</Value>
      <Value lang="th_TH">Adobe XMP Panels</Value>
      <Value lang="tr_TR">Adobe XMP Panels</Value>
      <Value lang="uk_UA">Adobe XMP Panels</Value>
      <Value lang="vi_VN">Adobe XMP Panels</Value>
      <Value lang="zh_CN">Adobe XMP Panels</Value>
      <Value lang="zh_TW">Adobe XMP Panels</Value>
      <Value lang="en_AE">Adobe XMP Panels</Value>
      <Value lang="en_IL">Adobe XMP Panels</Value>
      <Value lang="fr_MA">Adobe XMP Panels</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0", "AMTConfigPath", "[AdobeCommon]\XMP\Custom File Info Panels\4.0\AMT\application.xml")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0", "LEID", "")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0", "ChannelID", "AdobeXMPPanels-4.0")
INSERT INTO PayloadData VALUES("{093DEFC4-542D-4D0A-8162-0592055515F4}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeXMPPanels-4.0">
    <DisplayName>Adobe XMP Panels</DisplayName>
  </Channel>')
