CREATE TABLE Branding ( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),resource_type TEXT NOT NULL,resource_data TEXT NOT NULL,PRIMARY KEY (ProductID, resource_type) )
CREATE TABLE DependencyData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PayloadIDb TEXT ,type TEXT NOT NULL ,product_family TEXT, product_name TEXT, version TEXT, PRIMARY KEY (PayloadID,PayloadIDb,type,product_family,product_name,version))
CREATE TABLE EULA_Files( productID TEXT NOT NULL, langCode TEXT NOT NULL,eula TEXT NOT NULL,PRIMARY KEY (productID, langCode) )
CREATE TABLE PayloadData( PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),domain TEXT NOT NULL,key TEXT NOT NULL,value TEXT NOT NULL,PRIMARY KEY (PayloadID, domain, key) )
CREATE TABLE Payloads( PayloadID TEXT NOT NULL, payload_family TEXT NOT NULL,payload_name TEXT NOT NULL, payload_version TEXT NOT NULL,payload_type TEXT NOT NULL,PRIMARY KEY (PayloadID) )
CREATE TABLE SuitePayloads( ProductID TEXT NOT NULL REFERENCES Suites (ProductID),PayloadID TEXT NOT NULL REFERENCES Payloads (PayloadID),PRIMARY KEY (ProductID, PayloadID) )
CREATE TABLE Suites( ProductID TEXT NOT NULL, group_name TEXT NOT NULL, group_family TEXT NOT NULL, display_name TEXT NOT NULL, PRIMARY KEY (ProductID) )
CREATE TABLE EULA_Ref( productID TEXT NOT NULL, langCode TEXT NOT NULL, eula_hash TEXT NOT NULL, PRIMARY KEY (productID, langCode) )
CREATE TABLE EULA_Content(eula_hash TEXT NOT NULL, Content TEXT NOT NULL, PRIMARY KEY (eula_hash))
CREATE TABLE IF NOT EXISTS pcd_meta ( key TEXT NOT NULL, value TEXT NOT NULL, PRIMARY KEY (key) )
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_version', 2)
INSERT OR REPLACE INTO pcd_meta (key, value) VALUES ('schema_compatibility_version', 1)
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{5C8414EB-CA7E-4C34-9F49-ED52A13388BE}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{6148264D-0CB6-473A-86EE-A82812B69A1F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{8C2802A3-CF13-44B3-A549-7AE6155E42AA}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{863B2CF9-D1CA-4226-83E2-2C6402CB9316}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{C591C49F-362D-46B9-81A7-639D6ED23528}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{19507546-03BF-4C6D-8B31-7C61D4D9A84F}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{DA1E6A94-0F8F-4FE0-81D6-B99D5C14B2DC}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{74C0DB31-A71F-477D-877C-BE7FBD653729}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{4A9DE96B-49E4-4A6E-A63A-5E7510AE3C5B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{FFF17629-331F-4511-8FA6-D58E1053429B}", "upgrade", "", "", "")
INSERT INTO DependencyData VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "{658C6ED1-AE33-4D93-8532-65BB7AD79252}", "upgrade", "", "", "")
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0" , "ValidationInfo", '<ValidationInfo payloadID="{0C4E7429-E920-4125-980E-029A87AE0A4D}"/>')
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0" , "ValidationSig", "0ppzDiwcObCZlqSCHaBqD/tjHTE2THTnCBqu+7a4tIf+Qu7cH0s9ppaY7TqLrdF67m5/uSHk16b3Pyhq6Tab2RQ02lUMESUF0GITuiemeq0ZPQ+4S5xJV3Xat3Iukd4/5vFml9AzLDWHxGKBQMUP5QdeTedWVXvJhA5JzbkVEx20NvjijH28vabmDFKMPADP+aAHbjkIdlKzTFLMHrIIqwkOaKAHk5t6Qa9gaZsLi6QdkBbclJ3tCCFYN9wJFSBKsDObOs8xs/7pF8XP/W2LrEPQO+9CxAbFdwCcf68sVNaOYI6lIskaJij2fADxXMmhkSeD5Am+Jxxw+6N5vMEewVVhw1D1Ny2cY3pE4ToUPKZwph/+8eHzhBU/XALKDr0rEsrqpaWVZWxmU3y8J8wnxIHURNbdCxPQ+ZlwQ5H9Z7FQyWFYiGhactvv3NSVSBfI+RwMycTCYpCwCR/UG07rUTkO7LbfSPvfvprg0AKj4aV5d0n0Y8LprtNKBNYeonvv")
INSERT INTO Payloads VALUES	("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "CoreTech", "AdobeColorCommonSetCMYK CS6", "4.0", "normal")
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0" , "PayloadInfo", '<PayloadInfo version="6.0.29.0"><BuildInfo>
    <Property name="Created">2011-09-26 11:21:03.607000</Property>
    <Property name="TargetName">AdobeColorCommonSetCMYK4_0-mul</Property>
    <Property name="ProcessorFamily">All</Property>
  </BuildInfo><InstallerProperties>
    <Property name="payloadType">SQLite</Property>
    <Property name="AdobeCode">{0C4E7429-E920-4125-980E-029A87AE0A4D}</Property>
    <Property name="ProductName">AdobeColorCommonSetCMYK</Property>
    <Property name="ProductVersion">4.0</Property>
  </InstallerProperties><InstallDir>
    <Platform isFixed="1" name="Default" folderName="">[AdobeCommon]\Color</Platform>
  </InstallDir><Languages languageIndependent="1"/><Satisfies>
    <ProductInfo>
		<Family>CoreTech</Family>
		<ProductName>AdobeColorCommonSetCMYK CS6</ProductName>
	<ProductVersion>4.0</ProductVersion>
    </ProductInfo>
  </Satisfies><Upgrades>
    <AdobeCode>{5C8414EB-CA7E-4C34-9F49-ED52A13388BE}</AdobeCode>
    <AdobeCode>{6148264D-0CB6-473A-86EE-A82812B69A1F}</AdobeCode>
    <AdobeCode>{8C2802A3-CF13-44B3-A549-7AE6155E42AA}</AdobeCode>
    <AdobeCode>{863B2CF9-D1CA-4226-83E2-2C6402CB9316}</AdobeCode>
    <AdobeCode>{C591C49F-362D-46B9-81A7-639D6ED23528}</AdobeCode>
    <AdobeCode>{19507546-03BF-4C6D-8B31-7C61D4D9A84F}</AdobeCode>
    <AdobeCode>{DA1E6A94-0F8F-4FE0-81D6-B99D5C14B2DC}</AdobeCode>
    <AdobeCode>{74C0DB31-A71F-477D-877C-BE7FBD653729}</AdobeCode>
    <AdobeCode>{4A9DE96B-49E4-4A6E-A63A-5E7510AE3C5B}</AdobeCode>
    <AdobeCode>{FFF17629-331F-4511-8FA6-D58E1053429B}</AdobeCode>
    <AdobeCode>{658C6ED1-AE33-4D93-8532-65BB7AD79252}</AdobeCode>
  </Upgrades><Channel enable="1" id="AdobeColorCommonSetCMYK-4.0">
    <DisplayName>AdobeColorCommonSetCMYK</DisplayName>
  </Channel><InstallDestinationMetadata relocatableSize="0" sysDriveSize="11937444"><Destination>
      <Root>[AdobeCommon]</Root>
      <TotalSize>11937444</TotalSize>
      <MaxPathComponent>/Color/Profiles/Recommended\JapanColor2002Newspaper.icc</MaxPathComponent>
    </Destination>
    <Assets>
      <Asset flag="0" name="Assets2_1" size="11937444"/>
    </Assets>
  </InstallDestinationMetadata><ConflictingProcesses>
	<Win32>
	<Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Ii][Nn][Gg] [Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Uu][Dd][Ii][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Gg][Oo][Ll][Ii][Vv][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Ff][Tt][Ee][Rr][Ff][Xx]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ss][Hh][Ee][Ll][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Dd][Nn][Gg] [Cc][Oo][Nn][Vv][Ee][Rr][Tt][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee][Pp][Rr][Oo][Xx][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ee][Vv][Ii][Cc][Ee][Cc][Ee][Nn][Tt][Rr][Aa][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Rr][Ee][Aa][Mm][Ww][Ee][Aa][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Nn][Cc][Oo][Rr][Ee]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Dd][Ss][Cc][Rr][Ii][Pp][Tt] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Rr][Aa][Ss][Hh][Rr][Ee][Pp][Oo][Rr][Tt][Ee][Rr]App\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh][Pp][Ll][Aa][Yy][Ee][Rr]*\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 7 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] 8 [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Cc][Oo][Pp][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Pp][Rr][Oo]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][[Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Tt][Oo][Cc][Kk] [Pp][Hh][Oo][Tt][Oo][Ss] [Cc][Ss]3\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 4\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] 5\.0\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Oo][Rr][Gg][Aa][Nn][Ii][Zz][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp][Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss][Ee][Dd][Ii][Tt][Oo][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Ii][Mm][Aa][Gg][Ee][Rr][Ee][Aa][Dd][Yy]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee] [Dd][Vv][Dd]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Oo][Nn][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee] [Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]4\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]10\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]\.[Cc][Oo][Mm]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Tt][Mm][Ll]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Oo][Bb][Oo][Hh][Ee][Ll][Pp]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Rr][Aa][Mm][Ee][Mm][Aa][Kk][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">[Aa][Dd][Oo][Bb][Ee][Cc][Aa][Pp][Tt][Ii][Vv][Aa][Tt][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Mm][Ii][Dd][Dd][Ll][Ee][Tt][Ii][Ee][Rr][Ss][Ee][Rr][Vv][Ii][Cc][Ee]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Rr][Ss][Oo]3[Ss][Ee][Rr][Vv][Ee][Rr]\.[Ee][Xx][Ee]</Process>
      <Process processType="Adobe" blocking="1">^3[Dd] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]\.[Ee][Xx][Ee]</Process>
    </Win32>
<OSX>
      <Process processType="Adobe" blocking="1">^[Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Ii][Nn][Gg] [Gg][Ll][Yy][Pp][Hh][Ll][Ee][Tt] [Mm][Aa][Nn][Aa][Gg][Ee][Rr] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Bb][Ee][Tt][Aa]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ss][Oo][Uu][Nn][Dd][Bb][Oo][Oo][Tt][Hh] [Cc][Ss]4</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Aa][Ff][Tt][Ee][Rr] [Ee][Ff][Ff][Ee][Cc][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Ff][Tt][Ee][Rr] [Ee][Ff][Ff][Ee][Cc][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Hh][Ee][Ll][Pp] [Vv][Ii][Ee][Ww][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Mm][Aa][Gg][Ee][Rr][Ee][Aa][Dd][Yy]*</Process>
      <Process processType="Adobe" blocking="1">^[Pp][Hh][Oo][Tt][Oo][Ss][Hh][Oo][Pp] [Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Gg][Oo][Ll][Ii][Vv][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee] [Cc][Ss][23]</Process>
      <Process processType="Adobe" blocking="1">^[Bb][Rr][Ii][Dd][Gg][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Cc][Rr][Aa][Ss][Hh][Cc][Rr][Aa][Ss][Hh] [Rr][Ee][Pp][Oo][Rr][Tt][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Ss][Hh][Ee][Ll][Ll]</Process>
      <Process processType="Adobe" blocking="1">^[Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ff][Ee][Xx][Tt][Nn][Ff][Oo][Rr][Cc][Oo][Nn][Tt][Rr][Ii][Bb][Uu][Tt][Ee][Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ee][Vv][Ii][Cc][Ee] [Cc][Ee][Nn][Tt][Rr][Aa][Ll]</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Rr][Ee][Aa][Mm][Ww][Ee][Aa][Vv][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ee][Nn][Cc][Oo][Rr][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Nn][Cc][Oo][Rr][Ee][Dd][Vv][Dd]</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Dd][Ss][Cc][Rr][Ii][Pp][Tt] [Tt][Oo][Oo][Ll][Kk][Ii][Tt]*</Process>
      <Process processType="Adobe" blocking="1">^[Ee][Xx][Tt][Ee][Nn][Ss][Ii][Oo][Nn] [Mm][Aa][Nn][Aa][Gg][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ff][Ii][Rr][Ee][Ww][Oo][Rr][Kk][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ff][Ll][Aa][Ss][Hh] [Cc][Ss]3</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Pp][Ll][Aa][Yy][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Ss][Tt][Aa][Ll][Ll] [Ff][Ll][Aa][Ss][Hh] [Pp][Ll][Aa][Yy][Ee][Rr] 9 [Uu][Bb]</Process>
      <Process processType="Adobe" blocking="1">^[Ff][Ll][Aa][Ss][Hh] [Vv][Ii][Dd][Ee][Oo] [Ee][Nn][Cc][Oo][Dd][Ee][Rr]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Ll][Ll][Uu][Ss][Tt][Rr][Aa][Tt][Oo][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Nn][Cc][Oo][Pp][Yy] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Cc][Oo][Pp][Yy] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn] [Cc][Ss]*</Process>
      <Process processType="Adobe" blocking="1">^[Ii][Nn][Dd][Ee][Ss][Ii][Gg][Nn][Ss][Ee][Rr][Vv][Ee][Rr]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Uu][Dd][Ii][Tt][Ii][Oo][Nn]</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Pp][Rr][Ee][Mm][Ii][Ee][Rr][Ee]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Cc][Rr][Oo][Bb][Aa][Tt]*</Process>
      <Process processType="Adobe" blocking="1">^[Dd][Ii][Ss][Tt][Ii][Ll][Ll][Ee][Rr]*</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Oo][Nn][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Bb][Rr][Ii][Dd][Gg][Ee] [Cc][Ss]4$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Mm][Ee][Dd][Ii][Aa] [Pp][Ll][Aa][Yy][Ee][Rr]$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Dd][Rr][Ii][Vv][Ee] [Cc][Ss]4$</Process>
      <Process processType="Adobe" blocking="1">^[Aa][Dd][Oo][Bb][Ee] [Mm][Ee][Dd][Ii][Aa] [Pp][Ll][Aa][Yy][Ee][Rr]$</Process>
    </OSX>
  </ConflictingProcesses><AddRemoveInfo>
    <DisplayVersion>
      <Value lang="ar_AE">4.0</Value>
      <Value lang="be_BY">4.0</Value>
      <Value lang="bg_BG">4.0</Value>
      <Value lang="ca_ES">4.0</Value>
      <Value lang="cs_CZ">4.0</Value>
      <Value lang="da_DK">4.0</Value>
      <Value lang="de_DE">4.0</Value>
      <Value lang="el_GR">4.0</Value>
      <Value lang="en_GB">4.0</Value>
      <Value lang="en_MX">4.0</Value>
      <Value lang="en_US">4.0</Value>
      <Value lang="en_XC">4.0</Value>
      <Value lang="en_XM">4.0</Value>
      <Value lang="es_ES">4.0</Value>
      <Value lang="es_MX">4.0</Value>
      <Value lang="es_QM">4.0</Value>
      <Value lang="et_EE">4.0</Value>
      <Value lang="fi_FI">4.0</Value>
      <Value lang="fr_CA">4.0</Value>
      <Value lang="fr_FR">4.0</Value>
      <Value lang="fr_MX">4.0</Value>
      <Value lang="fr_XM">4.0</Value>
      <Value lang="he_IL">4.0</Value>
      <Value lang="hi_IN">4.0</Value>
      <Value lang="hr_HR">4.0</Value>
      <Value lang="hu_HU">4.0</Value>
      <Value lang="is_IS">4.0</Value>
      <Value lang="it_IT">4.0</Value>
      <Value lang="ja_JP">4.0</Value>
      <Value lang="ko_KR">4.0</Value>
      <Value lang="lt_LT">4.0</Value>
      <Value lang="lv_LV">4.0</Value>
      <Value lang="mk_MK">4.0</Value>
      <Value lang="nb_NO">4.0</Value>
      <Value lang="nl_NL">4.0</Value>
      <Value lang="nn_NO">4.0</Value>
      <Value lang="no_NO">4.0</Value>
      <Value lang="pl_PL">4.0</Value>
      <Value lang="pt_BR">4.0</Value>
      <Value lang="ro_RO">4.0</Value>
      <Value lang="ru_RU">4.0</Value>
      <Value lang="sh_YU">4.0</Value>
      <Value lang="sk_SK">4.0</Value>
      <Value lang="sl_SI">4.0</Value>
      <Value lang="sq_AL">4.0</Value>
      <Value lang="sv_SE">4.0</Value>
      <Value lang="th_TH">4.0</Value>
      <Value lang="tr_TR">4.0</Value>
      <Value lang="uk_UA">4.0</Value>
      <Value lang="vi_VN">4.0</Value>
      <Value lang="zh_CN">4.0</Value>
      <Value lang="zh_TW">4.0</Value>
      <Value lang="en_AE">4.0</Value>
      <Value lang="en_IL">4.0</Value>
      <Value lang="fr_MA">4.0</Value>
    </DisplayVersion>
    <DisplayName>
      <Value lang="ar_AE">AdobeColorCommonSetCMYK</Value>
      <Value lang="be_BY">AdobeColorCommonSetCMYK</Value>
      <Value lang="bg_BG">AdobeColorCommonSetCMYK</Value>
      <Value lang="ca_ES">AdobeColorCommonSetCMYK</Value>
      <Value lang="cs_CZ">AdobeColorCommonSetCMYK</Value>
      <Value lang="da_DK">AdobeColorCommonSetCMYK</Value>
      <Value lang="de_DE">AdobeColorCommonSetCMYK</Value>
      <Value lang="el_GR">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_GB">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_MX">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_US">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_XC">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_XM">AdobeColorCommonSetCMYK</Value>
      <Value lang="es_ES">AdobeColorCommonSetCMYK</Value>
      <Value lang="es_MX">AdobeColorCommonSetCMYK</Value>
      <Value lang="es_QM">AdobeColorCommonSetCMYK</Value>
      <Value lang="et_EE">AdobeColorCommonSetCMYK</Value>
      <Value lang="fi_FI">AdobeColorCommonSetCMYK</Value>
      <Value lang="fr_CA">AdobeColorCommonSetCMYK</Value>
      <Value lang="fr_FR">AdobeColorCommonSetCMYK</Value>
      <Value lang="fr_MX">AdobeColorCommonSetCMYK</Value>
      <Value lang="fr_XM">AdobeColorCommonSetCMYK</Value>
      <Value lang="he_IL">AdobeColorCommonSetCMYK</Value>
      <Value lang="hi_IN">AdobeColorCommonSetCMYK</Value>
      <Value lang="hr_HR">AdobeColorCommonSetCMYK</Value>
      <Value lang="hu_HU">AdobeColorCommonSetCMYK</Value>
      <Value lang="is_IS">AdobeColorCommonSetCMYK</Value>
      <Value lang="it_IT">AdobeColorCommonSetCMYK</Value>
      <Value lang="ja_JP">AdobeColorCommonSetCMYK</Value>
      <Value lang="ko_KR">AdobeColorCommonSetCMYK</Value>
      <Value lang="lt_LT">AdobeColorCommonSetCMYK</Value>
      <Value lang="lv_LV">AdobeColorCommonSetCMYK</Value>
      <Value lang="mk_MK">AdobeColorCommonSetCMYK</Value>
      <Value lang="nb_NO">AdobeColorCommonSetCMYK</Value>
      <Value lang="nl_NL">AdobeColorCommonSetCMYK</Value>
      <Value lang="nn_NO">AdobeColorCommonSetCMYK</Value>
      <Value lang="no_NO">AdobeColorCommonSetCMYK</Value>
      <Value lang="pl_PL">AdobeColorCommonSetCMYK</Value>
      <Value lang="pt_BR">AdobeColorCommonSetCMYK</Value>
      <Value lang="ro_RO">AdobeColorCommonSetCMYK</Value>
      <Value lang="ru_RU">AdobeColorCommonSetCMYK</Value>
      <Value lang="sh_YU">AdobeColorCommonSetCMYK</Value>
      <Value lang="sk_SK">AdobeColorCommonSetCMYK</Value>
      <Value lang="sl_SI">AdobeColorCommonSetCMYK</Value>
      <Value lang="sq_AL">AdobeColorCommonSetCMYK</Value>
      <Value lang="sv_SE">AdobeColorCommonSetCMYK</Value>
      <Value lang="th_TH">AdobeColorCommonSetCMYK</Value>
      <Value lang="tr_TR">AdobeColorCommonSetCMYK</Value>
      <Value lang="uk_UA">AdobeColorCommonSetCMYK</Value>
      <Value lang="vi_VN">AdobeColorCommonSetCMYK</Value>
      <Value lang="zh_CN">AdobeColorCommonSetCMYK</Value>
      <Value lang="zh_TW">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_AE">AdobeColorCommonSetCMYK</Value>
      <Value lang="en_IL">AdobeColorCommonSetCMYK</Value>
      <Value lang="fr_MA">AdobeColorCommonSetCMYK</Value>
    </DisplayName>
  </AddRemoveInfo><UserPreferences>0</UserPreferences></PayloadInfo>')
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0" , "WorkflowVersion", "CS6")
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0" , "DEVersion", "6.0")
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0", "ChannelID", "AdobeColorCommonSetCMYK-4.0")
INSERT INTO PayloadData VALUES("{0C4E7429-E920-4125-980E-029A87AE0A4D}", "0", "ChannelInfo", '<Channel enable="1" id="AdobeColorCommonSetCMYK-4.0">
    <DisplayName>AdobeColorCommonSetCMYK</DisplayName>
  </Channel>')
