WEBVTT

00:00:10.500 --> 00:00:15.230
Hey guys. In this lecture I'm going to show you how to set

00:00:15.230 --> 00:00:20.210
up your development environment so that you can follow along in these courses.

00:00:20.210 --> 00:00:24.260
First let's talk about operating systems.

00:00:24.260 --> 00:00:28.085
The three big ones are Windows, Linux and Mac.

00:00:28.085 --> 00:00:32.350
Most, but not all of my courses are part of the Deep Learning Series.

00:00:32.350 --> 00:00:37.060
Eventually the deep learning models we're going to build will become so complex

00:00:37.060 --> 00:00:42.579
that we will need specialized libraries like Theano and Tensor Flow to implement them.

00:00:42.579 --> 00:00:48.509
Now as of today Theano and TensorFlow are not officially supported on Windows.

00:00:48.509 --> 00:00:51.039
So if you are interested in deep learning,

00:00:51.039 --> 00:00:56.329
you will discover pretty soon that there is not much you can do without a ton of work.

00:00:56.329 --> 00:00:58.750
Even working with non-GPU enabled

00:00:58.750 --> 00:01:02.380
libraries like Numpy and Matplotlib it can be a challenge,

00:01:02.380 --> 00:01:05.769
but these are at least possible on Windows.

00:01:05.769 --> 00:01:08.814
If you really must use Windows and have your Python environment

00:01:08.814 --> 00:01:12.459
inside Windows and you don't want to try the virtual machine method,

00:01:12.459 --> 00:01:14.436
which I'm going to discuss next,

00:01:14.436 --> 00:01:18.599
then one good library I know of is called Anaconda.

00:01:18.599 --> 00:01:24.319
You can get Anaconda at continuum.io/downloads.

00:01:24.319 --> 00:01:28.810
I can't vouch for this 100 percent because I haven't used it myself,

00:01:28.810 --> 00:01:32.650
but I've known others who have found it at least usable.

00:01:32.650 --> 00:01:34.819
Now I'm telling you this from experience.

00:01:34.819 --> 00:01:36.159
I've worked with a ton of clients,

00:01:36.159 --> 00:01:38.890
one on one, and Python development,

00:01:38.890 --> 00:01:44.849
especially when it involves the NumPy SciPy stack is not easy when it's on Windows.

00:01:44.849 --> 00:01:46.870
The method I'm going to describe using

00:01:46.870 --> 00:01:50.920
virtual machines will work on most modern computers and as of right

00:01:50.920 --> 00:01:54.640
now if you want to do any deep learning stuff whether that

00:01:54.640 --> 00:01:58.805
be with Theano or TensorFlow you cannot do this on Windows.

00:01:58.805 --> 00:02:04.194
With that out of the way I'm going to quickly go over which courses I've released so far,

00:02:04.194 --> 00:02:09.439
or plan to release in the near future that do not require Theano and TensorFlow,

00:02:09.439 --> 00:02:11.870
so it's possible to do on Windows.

00:02:11.870 --> 00:02:16.925
With these in mind you can decide if you want to install a virtual machine or not.

00:02:16.925 --> 00:02:19.270
But I would highly recommend it because it's free

00:02:19.270 --> 00:02:22.590
and everyone can follow the same instructions.

00:02:22.590 --> 00:02:25.919
So we've got linear regression and Python,

00:02:25.919 --> 00:02:28.004
logistic regression in Python,

00:02:28.004 --> 00:02:31.840
which are the prerequisites to deep learning part one.

00:02:31.840 --> 00:02:34.020
Then we have deep learning in Python part one,

00:02:34.020 --> 00:02:37.810
which is mostly in NumPy and a little bit of TensorFlow.

00:02:37.810 --> 00:02:41.189
We've got easy natural language processing in Python,

00:02:41.189 --> 00:02:44.159
data analytics, SQL for newbs,

00:02:44.159 --> 00:02:49.740
beginners and marketers, cluster analysis and unsupervised machine learning in Python,

00:02:49.740 --> 00:02:52.469
and unsupervised machine learning,

00:02:52.469 --> 00:02:55.020
hidden Markov models in Python.

00:02:55.020 --> 00:02:57.454
This course is going to have a little bit of Theano in it.

00:02:57.454 --> 00:03:04.254
Next, here are some courses that depend heavily upon Theano or TensorFlow or both.

00:03:04.254 --> 00:03:07.450
We've got practical deep learning in Theano or TensorFlow,

00:03:07.450 --> 00:03:10.485
convolutional neural networks in Python,

00:03:10.485 --> 00:03:12.895
unsupervised deep learning in Python,

00:03:12.895 --> 00:03:15.585
and recurrent neural networks in Python.

00:03:15.585 --> 00:03:17.009
As you can see, a lot of

00:03:17.009 --> 00:03:21.544
interesting and complex stuff can be done in Theano and TensorFlow.

00:03:21.544 --> 00:03:26.699
All right, so hopefully I've convinced you that using a virtual machine is a good idea.

00:03:26.699 --> 00:03:28.889
Windows is a great operating system,

00:03:28.889 --> 00:03:34.800
but it tends to not be as developer friendly since it tries to be more consumer friendly.

00:03:34.800 --> 00:03:36.210
Note that if you're on a Mac,

00:03:36.210 --> 00:03:38.159
this probably isn't necessary.

00:03:38.159 --> 00:03:40.841
You can just install Numpy, SciPy, Pandas,

00:03:40.841 --> 00:03:46.560
Matplotlib, Theano and TensorFlow using easy installer pip.

00:03:46.560 --> 00:03:49.574
Sometimes, this stuff can fail depending on

00:03:49.574 --> 00:03:53.430
the many combinations of versions of each thing that are possible,

00:03:53.430 --> 00:03:55.109
but usually just googling

00:03:55.109 --> 00:03:59.244
your error message will lead you to the right solution on Stack Overflow.

00:03:59.244 --> 00:04:02.669
If you're on a Mac, you can just do sudo pip install NumPy, SciPy,

00:04:02.669 --> 00:04:07.180
IPython, Pandas, Matplotlib and Theano.

00:04:07.180 --> 00:04:09.719
Alternatively you can use easy install,

00:04:09.719 --> 00:04:12.319
which might have a more recent version.

00:04:12.319 --> 00:04:15.525
You may need to use easy install to install pip itself,

00:04:15.525 --> 00:04:18.420
which is just sudo easy_install pip.

00:04:18.420 --> 00:04:20.310
If you want to install TensorFlow,

00:04:20.310 --> 00:04:22.108
just copy the command at TensorFlow.org.

00:04:22.108 --> 00:04:27.894
It's just a pip install command that points to their custom installation location.

00:04:27.894 --> 00:04:29.970
I won't put it here since it corresponds to

00:04:29.970 --> 00:04:35.069
a specific version and that version may become out of date by the time you watch this.

00:04:35.069 --> 00:04:39.839
That is good news because the people working on TensorFlow are updating it all the time.

00:04:39.839 --> 00:04:42.720
All right, so if you've made it this far that means you want

00:04:42.720 --> 00:04:46.019
to install a virtual machine with Linux on Windows.

00:04:46.019 --> 00:04:48.389
Or that you're already using Linux and you want to

00:04:48.389 --> 00:04:52.009
know what commands to use to install these libraries.

00:04:52.009 --> 00:04:55.649
We are going to need two things to start for this tutorial.

00:04:55.649 --> 00:04:59.399
VirtualBox and a lightweight version of Ubuntu.

00:04:59.399 --> 00:05:02.295
I would recommend Xubuntu or Lubuntu.

00:05:02.295 --> 00:05:07.063
I'm going to use the 64-bit version of Lubuntu for this tutorial.

00:05:07.063 --> 00:05:10.589
So download these first and then return to the tutorial.

00:05:10.589 --> 00:05:13.540
All right so now that you've got VirtualBox installed,

00:05:13.540 --> 00:05:19.129
we are going to create a new machine with 64-bit Lubuntu.

00:05:19.129 --> 00:05:25.430
So you want to hit new, type in a name for your machine,

00:05:25.430 --> 00:05:30.459
and this stuff is already correct.

00:05:30.459 --> 00:05:36.750
I'm going to choose 2 gigs of memory.

00:05:36.750 --> 00:05:42.104
You can use more memory if your computer has more memory.

00:05:42.104 --> 00:05:49.649
I'm going to create a virtual hard disk now.

00:05:49.649 --> 00:05:51.675
I'm going to choose dynamically allocated,

00:05:51.675 --> 00:05:54.579
and 8 gigs is good enough for this example.

00:05:54.579 --> 00:06:03.937
All right, so I'm going to go to settings.

00:06:03.937 --> 00:06:08.959
If we go to storage you can choose

00:06:08.959 --> 00:06:18.110
the ISO file that you downloaded.

00:06:18.110 --> 00:06:26.238
So I'm going to load up the Lubuntu ISO that you downloaded from Lubuntu.net.

00:06:26.238 --> 00:06:30.540
All right, so hit okay, and hit start.

00:06:30.540 --> 00:06:37.170
So it's going to take you through the installation of Lubuntu.

00:06:37.170 --> 00:06:43.750
So you want to hit install.

00:06:43.750 --> 00:06:47.639
All right, so Lubuntu is going to

00:06:47.639 --> 00:06:57.543
take you through some prompts.

00:06:57.543 --> 00:07:01.560
All right, so you want to erase this,

00:07:01.560 --> 00:07:33.009
get install Lubuntu and I'm not going to check any of these. So continue.

00:07:33.009 --> 00:07:35.964
All right, so once that's done it's going to ask you restart.

00:07:35.964 --> 00:07:40.000
So just hit restart now.

00:08:05.970 --> 00:08:09.670
All right. Now that you're in the machine,

00:08:09.670 --> 00:08:11.084
what I always like to do,

00:08:11.084 --> 00:08:17.875
if you notice how this window is too big for my Mac window,

00:08:17.875 --> 00:08:22.970
you can make the inner window resizable by installing

00:08:22.970 --> 00:08:24.790
guest additions and this also lets you

00:08:24.790 --> 00:08:30.540
do useful things like cut and paste between machines.

00:08:32.399 --> 00:08:36.085
So you want to open a terminal,

00:08:36.085 --> 00:08:45.299
you go to System Tools, select LXTerminal.

00:08:45.299 --> 00:09:00.970
Now I'm going to CD into that folder.

00:09:00.970 --> 00:09:04.914
Cool, so the correct command was

00:09:04.914 --> 00:09:14.210
sudo./VBoxLinuxAdditions.run noticed there was something that failed in there.

00:09:14.210 --> 00:09:17.100
So we need to install GCC.

00:09:17.100 --> 00:09:35.129
So you want to run sudo apt-get update and then run sudo apt-get upgrade.

00:09:35.129 --> 00:09:37.714
So the next thing is you want to sudo

00:09:37.714 --> 00:09:46.809
apt-get install build essential.

00:09:46.809 --> 00:09:50.940
All right, once you've done that we can try to run VBox additions

00:09:50.940 --> 00:10:04.565
again so that's sudo./VBoxLinux.

00:10:04.565 --> 00:10:12.139
All right, so everything worked this time so we're going to restart this machine.

00:10:15.669 --> 00:10:22.750
All right, so now the window is only as big as I want to drag it.

00:10:22.750 --> 00:10:29.570
Okay so now we can install the actual data science stuff.

00:10:29.570 --> 00:10:46.220
So you want to open again LXTerminal do a sudo apt-get update again.

00:10:46.220 --> 00:10:49.379
Now you want to install.

00:10:49.379 --> 00:10:54.576
It is a long list so python numpy,

00:10:54.576 --> 00:11:00.800
python scipy, python matplotlib,

00:11:00.800 --> 00:11:07.934
ipython python-pip,

00:11:07.934 --> 00:11:20.360
python-dev and python-setuptools. Just hit yes.

00:11:21.070 --> 00:11:25.690
All right, so now that that's installed you can test it out.

00:11:25.690 --> 00:11:29.065
You can type in ipython.

00:11:29.065 --> 00:11:32.220
So ipython is open.

00:11:32.220 --> 00:11:37.507
Import numpy and Numpy is working,

00:11:37.507 --> 00:11:43.023
import scipy, import pandas.

00:11:43.023 --> 00:11:44.820
All right, we don't have pandas yet.

00:11:44.820 --> 00:11:46.591
We will install that next.

00:11:46.591 --> 00:11:52.309
Import matplotlib.

00:11:52.309 --> 00:11:57.554
So we have everything we've installed so far.

00:11:57.554 --> 00:12:04.480
So now we're going to install pandas and theano.

00:12:04.480 --> 00:12:11.110
So that's pip install--upgrade pandas theano.

00:12:14.490 --> 00:12:17.779
All right, so now we've got theano.

00:12:17.779 --> 00:12:21.649
The last thing we need is TensorFlow.

00:12:21.649 --> 00:12:26.179
To get this you guys want to go to the TensorFlow website and just grab

00:12:26.179 --> 00:12:32.384
the latest command since they're updating the package all the time.

00:12:32.384 --> 00:12:38.299
So let's open a browser.

00:12:38.299 --> 00:12:59.389
Let's search for install TensorFlow.

00:12:59.389 --> 00:13:02.799
You take this code,

00:13:03.200 --> 00:13:11.355
copy it, and you paste it in here.

00:13:11.355 --> 00:13:16.480
All right, now you've got Theano and TensorFlow.

00:13:16.480 --> 00:13:18.289
You can test them out,

00:13:18.289 --> 00:13:51.049
go to GitHub.com/lazyprogrammer/machinelearningexamples.

00:13:51.049 --> 00:13:55.870
So you need Git if you want to check out this code with Git.

00:13:55.870 --> 00:14:07.629
So let's install Git.

00:14:07.629 --> 00:14:16.482
All right. So let's run our clone command again.

00:14:16.482 --> 00:14:28.250
Let's try the https version.

00:14:28.250 --> 00:14:31.899
Okay, so this one works.

00:14:32.240 --> 00:14:41.320
All right, so you CD into machine learning examples go into ann class2,

00:14:41.320 --> 00:14:47.465
so this has some introductory TensorFlow and Theano code.

00:14:47.465 --> 00:14:51.454
So just run python theano1.py,

00:14:51.454 --> 00:14:58.750
since this does not require any data.

00:14:58.750 --> 00:15:01.529
Cool, so it seems everything is working.

00:15:01.529 --> 00:15:03.875
Now let's try the TensorFlow example.

00:15:03.875 --> 00:15:12.259
So it's tensorflow1.

00:15:12.259 --> 00:15:16.894
All right so you want to make sure you install the correct TensorFlow.

00:15:16.894 --> 00:15:48.610
You want the CPU only version.

00:15:48.610 --> 00:15:53.980
Hopefully it just overwrites the other version without any trouble.

00:15:53.980 --> 00:16:01.080
So let's try the example again.

00:16:01.080 --> 00:16:04.340
Super, so now everything is working.

00:16:04.340 --> 00:16:09.850
Now if you want there is a text editor that I highly recommend.

00:16:09.850 --> 00:16:16.580
It's called Sublime Text.

00:16:16.580 --> 00:16:23.560
So you download, you go to Ubuntu 64-bit,

00:16:23.560 --> 00:16:35.129
you save this and

00:16:35.129 --> 00:16:39.299
it will automatically be installed.

00:17:02.750 --> 00:17:08.519
All right, so now you have the exact same text editor that I

00:17:08.519 --> 00:17:15.549
use in my lectures.

00:17:15.549 --> 00:17:22.950
So you open machine learning examples and this is all of our code.
